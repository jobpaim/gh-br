
INSERT INTO ERRO VALUES(1, 0, 0, 'Campo Nulo', 'O campo <$1> n�o pode ser nulo.');
INSERT INTO ERRO VALUES(2, 0, 0, 'Objeto Duplicado', 'J� existe um Objeto <$1> com  <$2> <$3>.');
INSERT INTO ERRO VALUES(3, 0, 0, 'Objeto Engatado', 'O objeto <$1> est� vinculado (em uso) ao objeto  <$2> <$3>.');


INSERT INTO CURSO VALUES(1,0,0,'Curso 1','cur1',NULL);
INSERT INTO CURSO VALUES(2,0,0,'Curso 2','cur2',NULL);
INSERT INTO CURSO VALUES(3,0,0,'Curso 3','cur3',NULL);

INSERT INTO DIASEMANA VALUES(1,0,0,'Segunda-Feira','Seg',NULL,1);
INSERT INTO DIASEMANA VALUES(2,0,0,'Ter�a-Feria','Ter',NULL,2);
INSERT INTO DIASEMANA VALUES(3,0,0,'Quarta-Feira','Qua',NULL,3);
INSERT INTO DIASEMANA VALUES(4,0,0,'Quinta-Feira','Qui',NULL,4);
INSERT INTO DIASEMANA VALUES(5,0,0,'Sexta-Feira','Sex',NULL,5);

INSERT INTO DOSCENTE VALUES(1,0,0,'Doscente 1','dos1',NULL,20);
INSERT INTO DOSCENTE VALUES(2,0,0,'Doscente 2','dos2',NULL,20);
INSERT INTO DOSCENTE VALUES(3,0,0,'Doscente 3','dos3',NULL,30);
INSERT INTO DOSCENTE VALUES(4,0,0,'Doscente 4','dos4',NULL,40);

INSERT INTO SALA VALUES(1,0,0,'Sala 1','sal1',NULL);
INSERT INTO SALA VALUES(2,0,0,'Sala 2','sal2',NULL);
INSERT INTO SALA VALUES(3,0,0,'Sala 3','sal3',NULL);
INSERT INTO SALA VALUES(4,0,0,'Sala 4','sal4',NULL);

INSERT INTO SEMESTRE VALUES(1,0,0,'1� Semestre','1� Sem.',NULL,1);
INSERT INTO SEMESTRE VALUES(2,0,0,'2� Semestre','2� Sem.',NULL,2);
INSERT INTO SEMESTRE VALUES(3,0,0,'3� Semestre','3� Sem.',NULL,3);
INSERT INTO SEMESTRE VALUES(4,0,0,'4� Semestre','4� Sem.',NULL,4);
INSERT INTO SEMESTRE VALUES(5,0,0,'5� Semestre','5� Sem.',NULL,5);
INSERT INTO SEMESTRE VALUES(6,0,0,'6� Semestre','6� Sem.',NULL,6);
INSERT INTO SEMESTRE VALUES(7,0,0,'7� Semestre','7� Sem.',NULL,7);

INSERT INTO TURNO VALUES(1,0,0,'Manh�','M',NULL,0,'0002-12-31 07:00:00.000000000','0002-12-31 12:00:00.000000000');
INSERT INTO TURNO VALUES(2,0,0,'Tarde','T',NULL,1,'0002-12-31 13:00:00.000000000','0002-12-31 18:00:00.000000000');
INSERT INTO TURNO VALUES(3,0,0,'Noite','N',NULL,2,'0002-12-31 19:00:00.000000000','0002-12-31 23:00:00.000000000');

INSERT INTO HORARIO VALUES(1,0,0,'07:00 - 08:00','07:00 - 08:00',NULL,'0002-12-31 07:00:00.000000000','0002-12-31 08:00:00.000000000',1);
INSERT INTO HORARIO VALUES(2,0,0,'08:00 - 09:00','08:00 - 09:00',NULL,'0002-12-31 08:00:00.000000000','0002-12-31 09:00:00.000000000',1);
INSERT INTO HORARIO VALUES(3,0,0,'09:00 - 10:00','09:00 - 10:00',NULL,'0002-12-31 09:00:00.000000000','0002-12-31 10:00:00.000000000',1);
INSERT INTO HORARIO VALUES(4,0,0,'10:00 - 12:00','10:00 - 12:00',NULL,'0002-12-31 10:00:00.000000000','0002-12-31 12:00:00.000000000',1);
INSERT INTO HORARIO VALUES(5,0,0,'13:00 - 15:00','13:00 - 15:00',NULL,'0002-12-31 13:00:00.000000000','0002-12-31 15:00:00.000000000',2);
INSERT INTO HORARIO VALUES(6,0,0,'15:00 - 17:00','15:00 - 17:00',NULL,'0002-12-31 15:00:00.000000000','0002-12-31 17:00:00.000000000',2);
INSERT INTO HORARIO VALUES(7,0,0,'19:00 - 21:00','19:00 - 21:00',NULL,'0002-12-31 19:00:00.000000000','0002-12-31 21:00:00.000000000',3);
INSERT INTO HORARIO VALUES(8,0,0,'21:00 - 23:00','21:00 - 23:00',NULL,'0002-12-31 21:00:00.000000000','0002-12-31 23:00:00.000000000',3);

INSERT INTO DISCIPLINA VALUES(1,0,0,'Disciplina 1','dis1',NULL,2,1);
INSERT INTO DISCIPLINA VALUES(2,0,0,'Disciplina 2','dis2',NULL,4,1);
INSERT INTO DISCIPLINA VALUES(3,0,0,'Disciplina 3','dis3',NULL,4,2);
INSERT INTO DISCIPLINA VALUES(4,0,0,'Disciplina 4','dis4',NULL,6,5);
INSERT INTO DISCIPLINA VALUES(5,0,0,'Disciplina 5','dis5',NULL,4,3);
INSERT INTO DISCIPLINA VALUES(6,0,0,'Disciplina 6','dis6',NULL,4,4);
INSERT INTO DISCIPLINA_CURSO VALUES(1,1);
INSERT INTO DISCIPLINA_CURSO VALUES(1,2);
INSERT INTO DISCIPLINA_CURSO VALUES(2,1);
INSERT INTO DISCIPLINA_CURSO VALUES(2,2);
INSERT INTO DISCIPLINA_CURSO VALUES(3,3);
INSERT INTO DISCIPLINA_CURSO VALUES(4,3);
INSERT INTO DISCIPLINA_CURSO VALUES(5,1);
INSERT INTO DISCIPLINA_CURSO VALUES(5,2);
INSERT INTO DISCIPLINA_CURSO VALUES(6,1);
INSERT INTO DISCIPLINA_CURSO VALUES(6,2);
