package paim.gh.app;

import paim.wingchun.controller.Localizar;

import paim.gh.model.modelos.GradeModel;
import paim.gh.model.pojos.Grade;

public final class Facade {

    private static Facade instancia;

    private Facade() {
        initialize();
    }

    public static Facade getInstancia() {
        if ( instancia == null )
            instancia = new Facade();
        return instancia;
    }

    /** essa sessao deve ser a mesma de GHApp, e ira conter o objeto grade */
    // public static Session getSessao() {
    // return GHApp.getSessao();
    // }

    protected void initialize() {}

    public void removeObjeto() {}

    public void createNewGH() {
        /* so cria um novo para trabalhar */
        Grade grade = new Grade();
        grade.setNome("nova grade...");
        grade.setSigla("nova...");
        try {
            GradeModel.getInstance().saveOrUpdate(GHApp.sessao, grade);
            // getSessao().flush();
            Gradiador.getInstance().setGrade(grade);
        }
        catch ( Exception e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void openGH() {

        new Localizar<Grade>() {

            // @Override
            // protected LocalizarView<Grade> c_instanciaVisao() throws Exception {
            // LocalizarView<Grade> novaVisao = new LocalizarView<Grade>(Grade.class);
            // return novaVisao;
            // }

            @Override
            protected void devolver(Object pojo) throws Exception {
                Grade gradeSelecionada = (Grade) pojo;
                Gradiador.getInstance().loadGrade(gradeSelecionada.getId());
                /* fecha localizador */
                getVisao().fechar();
            }
        };
    }

    /**
     * @author paim 07/11/2011 void
     */
    public void closeGH() {
        Gradiador.getInstance().setGrade(null);
    }

    /**
     * PAIM sera mesmo necessario esse metodo??????/
     *
     * @author paim 07/11/2011 void
     */
    public void saveGH() {
        try {
            Gradiador.getInstance().saveOrUpdate();
        }
        catch ( Exception e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * @author paim 07/11/2011 void
     */
    public void deleteGH() {
        // TODO Auto-generated method stub

    }

    public void habilitaComponentes() {}

    boolean fechar(boolean imediatamente) {
        return imediatamente;
    }
}
