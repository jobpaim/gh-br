package paim.gh.app;

import java.util.EnumSet;
import java.util.List;

/**
 * @author paim 03/11/2010*
 */
@Deprecated
public class GHTipos {

    public static enum Funcoes_GH {
        INVALIDO("", ""),
        NOVA("nova", "Nova"),
        ABRIR("abrir", "Abrir"),
        FECHAR("fechar", "Fechar"),
        EXCLUIR("excluir", "Excluir"),
        IMPRIMIR("imprimir", "Imprimir"),
        AJUDA("ajuda", "Ajuda"),
        AJUDA_CONTEUDO("ajudaConteudo", "Conteúdo"),
        AJUDA_APRESENTACAO("ajudaApresentacao", "Apresentação"),
        AJUDA_FUNCOES("ajudaFuncoes", "Funções Disponíveis"),
        AJUDA_FUNCOES$1("ajudaFuncoes1", "Menus"),
        AJUDA_FUNCOES$2("ajudaFuncoes2", "Barra de Ícones"),
        AJUDA_DICAS("ajudaDicas", "Dicas de Navegação"),
        AJUDA_LEIAME("ajudaLeiame", "Leia-me"),
        SOBRE("sobre", "Sobre "),
        AJUDA_BOTAO("ajudaBotao", ""),
        SAIR("sair", "Sair do Programa"),
        EXIBIR_AVISOS("exibirAvisos", "Sempre Mostrar Leia-me"),
        EXIBIR_SPLASH("exibirSplash", "Sempre Mostrar Animação de Abertura");

        private final String nome;

        private final String texto;

        private Funcoes_GH(String nome, String texto) {
            this.nome = nome;
            this.texto = texto;
        }

        public String getNome() {
            return this.nome;
        }

        public String getTexto() {
            return this.texto;
        }

        public static Funcoes_GH getByActionCommand(String nome) {
            for ( Funcoes_GH a : EnumSet.allOf(Funcoes_GH.class) ) {
                if ( a.getActionCommand().equals(nome) ) {
                    return a;
                }
            }
            return INVALIDO;
        }

        public String getActionCommand() {
            return "a_" + getNome();
        }

        public String getButtonNome() {
            return "b_" + getNome();
        }

        public String getMItemNome() {
            return "mi_" + getNome();
        }

        public String[] getListaComponentes(List<Funcoes_GH> lista) {
            for ( @SuppressWarnings("unused")
            Funcoes_GH f : lista ) {
                // TODO ver o VGDBF
            }

            return null;

        }

    }
}
