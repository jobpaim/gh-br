package paim.gh.app;


import java.util.logging.Level;
import java.util.prefs.Preferences;

import paim.wingchun.app.WC_App;
import paim.wingchun.app.WC_Log;

import paim.gh.view.GHView;


/** @author paim 03/11/2010* */
public class GHApp extends WC_App<GHView> {

    protected static GHApp app;

    public final static Preferences preferencias;

    static {
        preferencias = Preferences.userNodeForPackage(GHApp.class);
    }

    public static GHApp getInstancia() {
        return app;
    }

    protected GHApp() {
        super();
        app = this;

        if ( !initialize() )
            /* tonto cocoduche! */
            System.exit(1);
    }

    /* se nao inicializar qlq coisa entao retorna false */
    // @Override
    protected boolean initialize() {
        // boolean result = true;
        // boolean showSplash = Preferencias.getBoolean(Funcoes_GH.EXIBIR_SPLASH.getNome(), true);
        // SplashScreen splash = null;
        // if ( showSplash ) {
        // /* mostra splash */
        // // splash = new SplashScreen(null, 12, lSplash);
        // }
        //
        // result = super.initialize();

        WC_Log.getInstancia().getlogger().log(Level.INFO, "Inicializando Facade...");
        Facade.getInstancia();

        WC_Log.getInstancia().getlogger().log(Level.INFO, "Inicializando Gerenciador...");
        Gerenciavel gerenciador = Gerenciador.getInstancia();

        WC_Log.getInstancia().getlogger().log(Level.INFO, "Inicializando Gradiador...");
        Gerenciavel gradiador = Gradiador.getInstance();

        gerenciador.setListeners();
        gradiador.setListeners();

        // /* espera fim splash */
        // if ( splash != null ) {
        // // splash.aguardaTerminar();
        // }
        return true;
    }

    @Override
    protected Class<GHView> getVClass() {
        return GHView.class;
    }

    public static void main(String[] args) {
        if ( !antesInstanciarApp(args) )
            System.exit(1);
        new GHApp();
    }

    /** Retorna referência ao help da aplicação. */
    // public static HelpUtil getHelp() {
    // return help;
    // }

    // /** Manda exibir a Ajuda, ja acertando o ícone da janela... */
    // protected static void showHelp() {
    // final ImageIcon imageIcon = new ImageIcon(DBFApp.class.getResource("/imagens/srf.gif"));
    //
    // getHelp().exibeAjuda();
    // SwingUtilities.invokeLater(new Runnable() {
    //
    // public void run() {
    // DefaultHelpBroker hb = (DefaultHelpBroker) getHelp().getHelpBroker();
    // WindowPresentation presentation = hb.getWindowPresentation();
    // if ( presentation != null ) {
    // Window helpWindow = presentation.getHelpWindow();
    // if ( helpWindow != null && helpWindow instanceof JFrame )
    // ((JFrame) helpWindow).setIconImage(imageIcon.getImage());
    // }
    // }
    // });
    // }
    //
    // public static void chamaHelp(String iD) {
    // try {
    // getHelp().getHelpBroker().setCurrentID(iD.toString());
    // showHelp();
    // }
    // catch ( Exception e ) {
    // DBFLog.getInstancia().getlogger().log(Level.WARNING, e.getMessage(), e);
    // chamaHelpPadrao();
    // }
    // }

}
