/**
 * @author paim 08/03/2012
 */
package paim.gh.view.paineis.toolBar;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.event.EventListenerList;

import paim.wingchun.controller.WC_Event;
import paim.wingchun.view.Fireable;
import paim.wingchun.view.componentes.toolbar.AbstractJToolBar;

/**
 * @author paim 08/03/2012
 */
public class GradeTools extends AbstractJToolBar implements Fireable<GradeToolsListener> {

    private JButton jButtonStart;

    private JButton jButtonStop;

    private JButton jButtonPause;

    private JButton jButtonContinua;

    protected enum Eventos {
        START, STOP, PAUSE, CONTINUA, SET_MESTRE, SHOW_MESTRE
    };

    @Override
    public EventListenerList getListenersList() {
        return this.listenerList;
    }

    @Override
    public void fire(WC_Event e) {
        //GradeToolsListener[] listeners = getListenersList().getListeners(GradeToolsListener.class);
        for ( GradeToolsListener listener : fireListeners() ) {
            switch ( (Eventos) e.evento() ) {
                case STOP:
                    listener.stop();
                    break;
                case START:
                    listener.start();
                    break;
                case CONTINUA:
                    listener.continua();
                    break;
                case PAUSE:
                    listener.pause();
                    break;
                case SHOW_MESTRE:
                    listener.showMestre();
                    break;
                case SET_MESTRE:
                    listener.changeMestre();
                    break;

                default:
                    return;
            }
        }
    }

    public GradeTools() {
        super(JToolBar.HORIZONTAL);
        initialize();
    }

    private void initialize() {
        add(getjButtonStart());
        add(getjButtonStop());
        add(getjButtonPause());
        add(getjButtonContinua());
    }

    @Override
    public void setEnabled(boolean enabled) {
        // TODO Auto-generated method stub

    }

    private JButton getjButtonStart() {
        if ( jButtonStart == null ) {
            jButtonStart = new JButtonP(Eventos.START);
        }
        return jButtonStart;
    }

    private JButton getjButtonStop() {
        if ( jButtonStop == null ) {
            jButtonStop = new JButtonP(Eventos.STOP);
        }
        return jButtonStop;
    }

    private JButton getjButtonPause() {
        if ( jButtonPause == null ) {
            jButtonPause = new JButtonP(Eventos.PAUSE);
        }
        return jButtonPause;
    }

    private JButton getjButtonContinua() {
        if ( jButtonContinua == null ) {
            jButtonContinua = new JButtonP(Eventos.CONTINUA);
        }
        return jButtonContinua;
    }

}
