/**
 * @author paim 08/03/2012
 * 
 */
package paim.gh.view.paineis.toolBar;

import java.util.EventListener;

/**
 * @author paim 08/03/2012
 * 
 */
public interface GradeToolsListener extends EventListener {

    public boolean start();

    public void stop();

    public void pause();

    public void continua();

    public void changeMestre();

    public void showMestre();

}
