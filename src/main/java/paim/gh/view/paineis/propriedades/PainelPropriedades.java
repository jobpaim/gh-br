/** @author paim 10/11/2011 */
package paim.gh.view.paineis.propriedades;


import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.JPanel;

import paim.wingchun.controller.AbstractUniqueController;
import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.AbstractView.Estado;
import paim.wingchun.view.FireListener;
import paim.wingchun.view.componentes.WC_TextField;

import paim.gh.model.pojos.Aula;
import paim.gh.model.pojos.Grade;
import paim.gh.model.pojos.Professor;
import paim.gh.model.pojos.Turma;

import static paim.wingchun.app.Reflections.getRotulo;

/** @author paim 10/11/2011 */
@SuppressWarnings("serial")
public final class PainelPropriedades extends JPanel {

    private CardLayout cardLayout;

    private JPanel emptyPainel;

    private PropriedadesAula propriedadesAula;

    private PropriedadesTurma propriedadesTurma;

    private PropriedadesProfessor propriedadesProfessor;

    private PropriedadesGrade propriedadesGrade;

    public PainelPropriedades() {
        super();
        initialize();
    }

    private void initialize() {
        setLayout(getCardLayout());
        add(emptyPainel().getName(), emptyPainel());
        add(propriedadesGrade().getName(), propriedadesGrade());
        add(propriedadesAula().getName(), propriedadesAula());
        add(propriedadesProfessor().getName(), propriedadesProfessor());
    }

    private CardLayout getCardLayout() {
        if ( cardLayout == null ) {
            cardLayout = new CardLayout();
            cardLayout.setHgap(8);
            cardLayout.setVgap(3);
        }
        return cardLayout;
    }

    private PropriedadesAula propriedadesAula() {
        if ( propriedadesAula == null )
            propriedadesAula = new PropriedadesAula();

        return propriedadesAula;
    }

    private PropriedadesTurma propriedadesTurma() {
        if ( propriedadesTurma == null )
            propriedadesTurma = new PropriedadesTurma();

        return propriedadesTurma;
    }

    private PropriedadesProfessor propriedadesProfessor() {
        if ( propriedadesProfessor == null )
            propriedadesProfessor = new PropriedadesProfessor();

        return propriedadesProfessor;
    }

    private PropriedadesGrade propriedadesGrade() {
        if ( propriedadesGrade == null )
            propriedadesGrade = new PropriedadesGrade();

        return propriedadesGrade;
    }

    private JPanel emptyPainel() {
        if ( emptyPainel == null ) {
            emptyPainel = new JPanel();
            emptyPainel.setName(PainelPropriedades.class.getSimpleName() + "_Vazio");
            // emptyPainel.setLayout(new BorderLayout());

            // emptyPainel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

            // JLabel rotulo = new JLabel("Properties");
            // rotulo.setFont(new Font("Tahoma", Font.BOLD, 14));
            // rotulo.setForeground(Color.blue);
            // rotulo.setVerticalAlignment(SwingConstants.CENTER);
            // rotulo.setHorizontalAlignment(SwingConstants.CENTER);
            //
            // emptyPainel.add(rotulo, BorderLayout.CENTER);
        }

        return emptyPainel;
    }

    public void showEmpty() {
        show(null);
    }

    public void show(Pojo pojo) {
        if ( pojo == null )
            getCardLayout().show(this, emptyPainel().getName());
        else if ( pojo instanceof Grade ) {
            getCardLayout().show(this, propriedadesGrade().getName());
            propriedadesGrade().update((Grade) pojo);
        }
        else if ( pojo instanceof Aula ) {
            getCardLayout().show(this, propriedadesAula().getName());
            propriedadesAula().update((Aula) pojo);
        }
        else if ( pojo instanceof Professor ) {
            getCardLayout().show(this, propriedadesProfessor().getName());
            propriedadesProfessor().update((Professor) pojo);
        }
        else if ( pojo instanceof Turma ) {
            getCardLayout().show(this, propriedadesAula().getName());
            propriedadesTurma().update(((Turma) pojo));
        }
    }

    public void addListener(PropriedadesListener listener) {
        propriedadesGrade().addListener(listener);
        propriedadesAula().addListener(listener);
        propriedadesProfessor().addListener(listener);
        propriedadesTurma().addListener(listener);
    }

    private class PropriedadesTurma extends AbstractPropriedades<Turma> {

        public PropriedadesTurma() {
            super();
            setName(PainelPropriedades.class.getSimpleName() + "_" + getRotulo(Aula.class));
        }

        @Override
        void addComponentes() {
            add(campoNome().setEditable(false));
            add(campoSigla().setEditable(false));
            add(campoProfessor());
            add(campoDiaSemana());
            add(campoHorario());
            add(campoTurno());
            add(campoSala());
            add(campoCurso());
            add(campoSemestre());
            add(Box.createVerticalGlue());
            add(getjPreferencia());
        }

        @Override
        protected void addListeners() {
            super.addListeners();
            campoProfessor().addActionListener(new FireListener(this, Eventos.PROFESSOR));
        }

        @Override
        void update(Turma pojo) {
            campoNome().setText(pojo.getAula().getNome());
            campoSigla().setText(pojo.getSigla());
            // FIXME como ficam os professores???
            // getjProfessor().setText(pojo.get)

        }
    }

    private class PropriedadesAula extends AbstractPropriedades<Aula> {

        @Override
        void addComponentes() {
            add(campoNome().setEditable(false));
            add(campoSigla().setEditable(false));
            add(campoProfessor());
            add(campoTurma());
            add(campoDiaSemana());
            add(campoHorario());
            add(campoTurno());
            add(campoSala());
            add(campoCurso());
            add(campoSemestre());
            add(Box.createVerticalGlue());
            add(getjPreferencia());
        }

        @Override
        protected void addListeners() {
            super.addListeners();
            campoTurma().addActionListener(new FireListener(this, Eventos.TURMA, campoTurma()));
            campoProfessor().addActionListener(new FireListener(this, Eventos.PROFESSOR));
        }

        @Override
        void update(Aula pojo) {
            campoNome().setText(pojo.getNome());
            campoSigla().setText(pojo.getSigla());
            // FIXME como ficam os professores???
            // getjProfessor().setText(pojo.get)
        }
    }

    private class PropriedadesProfessor extends AbstractPropriedades<Professor> {

        @Override
        void addComponentes() {
            add(campoNome().setEditable(false));
            add(campoSigla().setEditable(false));
            add(campoAula());
            add(campoDiaSemana());
            add(campoHorario());
            add(campoTurno());
            add(campoSala());
            add(campoCurso());
            add(campoSemestre());
            add(Box.createVerticalGlue());
            add(getjPreferencia());
        }

        @Override
        protected void addListeners() {
            super.addListeners();
            campoAula().addActionListener(new FireListener(this, Eventos.TURMA));
        }

        @Override
        void update(Professor pojo) {
            campoNome().setText(pojo.getNome());
            campoSigla().setText(pojo.getSigla());
        }
    }

    private class PropriedadesGrade extends AbstractPropriedades<Grade> {

        private WC_TextField<?> campoDescricao;

        private final WC_TextField<?> campoDescricao() {
            if ( campoDescricao == null ) {
                campoDescricao = new WC_TextField<>();
                campoDescricao.setLabel("Descricao");
            }
            return campoDescricao;
        }

        @Override
        void addComponentes() {
            add(campoNome().addConstrutor());
            add(campoSigla().addConstrutor());
            add(campoDescricao().addConstrutor());
            add(Box.createVerticalGlue());
        }

        @Override
        protected void addListeners() {
            campoNome().addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {

                    new AbstractUniqueController<Grade, GradePropriedadesView>() {

                        @Override
                        protected Long getIdInicial() {
                            // preciso saber quem eh a grade bla bla.....
                            return null;
                        }

                        @Override
                        protected Estado c_confirmar(Object objeto, List<Erro> erros) throws Exception {
                            // TODO Auto-generated method stub
                            return null;
                        }

                        @Override
                        protected void devolver(Object objeto) throws Exception {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        protected boolean c_cancelar(Object objeto) throws Exception {
                            // TODO Auto-generated method stub
                            return false;
                        }
                    };

                }
            });
            // TODO criar janela extendendo o uniqueview
            // revisar esses nomes unique, list,multi usar single, usar ...
        }

        @Override
        void update(Grade pojo) {
            campoNome().setText(pojo.getNome());
            campoSigla().setText(pojo.getSigla());
        }
    }
}
