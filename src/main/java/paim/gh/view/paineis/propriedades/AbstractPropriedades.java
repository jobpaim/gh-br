/** @author temujin Feb 6, 2012 */
package paim.gh.view.paineis.propriedades;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.EventListenerList;

import paim.wingchun.app.Reflections;
import paim.wingchun.controller.WC_Event;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.FireListener;
import paim.wingchun.view.Fireable;
import paim.wingchun.view.componentes.WC_TextField;

import paim.gh.model.pojos.*;

import static paim.wingchun.app.Reflections.getRotulo;
import static paim.wingchun.app.Reflections.getRotuloS;

/** @author temujin Feb 6, 2012 */
public abstract class AbstractPropriedades<P extends Pojo> extends JPanel implements Fireable<PropriedadesListener> {

    // private P pojo;

    private WC_TextField<?> campoSigla;

    private WC_TextField<?> campoNome;

    private WC_TextField<Professor> campoProfessor;

    private WC_TextField<Aula> campoAula;

    private WC_TextField<Dia> campoDiaSemana;

    private WC_TextField<Horario> campoHorario;

    private WC_TextField<Turno> campoTurno;

    private WC_TextField<Sala> campoSala;

    private WC_TextField<Curso> campoCurso;

    private WC_TextField<Semestre> campoSemestre;

    private WC_TextField<Turma> campoTurma;

    private JButton jPreferencia;

    private Class<P> clazz;

    public enum Eventos {
        NOTHING,

        PROFESSOR, TURMA, DIA, HORARIO, TURNO, SALA, CURSO, SEMESTRE, AULA,

        HIDE, HIDEALL, ADDTURMA, REMOVETURMA;
    }

    /** este construtor ira buscar a classe no tipo parametrizado */
    AbstractPropriedades() {
        this.clazz = Reflections.getClassParameter(this.getClass(), Pojo.class);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBorder(getDefaultBorder());
        addComponentes();
        addListeners();
        setName(this.getClass().getSimpleName() + "_" + getRotulo(clazz));
    }

    abstract void update(P pojo);

    // abstract void initialize();
    abstract void addComponentes();

    @Override
    public EventListenerList getListenersList() {
        return this.listenerList;
    }

    @Override
    public void fire(WC_Event e) {
        for ( PropriedadesListener listener : fireListeners() ) {
            switch ( (Eventos) e.evento() ) {
                case PROFESSOR:
                    listener.selectProfessor(e);
                break;
                case TURMA:
                    listener.selectTurma(e);
                break;
                case DIA:
                    listener.selectDia(e);
                break;
                case HORARIO:
                    listener.selectHorario(e);
                break;
                case TURNO:
                    listener.selectTurno(e);
                break;
                case SALA:
                    listener.selectSala(e);
                break;
                case CURSO:
                    listener.selectCurso(e);
                break;
                case SEMESTRE:
                    listener.selectSemestre(e);
                break;
                case AULA:
                    listener.selectAula(e);
                break;

                default:
                break;
            }
        }
    }

    protected void addListeners() {
        campoDiaSemana().addActionListener(new FireListener(this, Eventos.DIA));
        campoHorario().addActionListener(new FireListener(this, Eventos.HORARIO));
        campoTurno().addActionListener(new FireListener(this, Eventos.TURNO));
        campoSala().addActionListener(new FireListener(this, Eventos.SALA));
        campoCurso().addActionListener(new FireListener(this, Eventos.CURSO));
        campoSemestre().addActionListener(new FireListener(this, Eventos.SEMESTRE));
    }

    String getTitulo() {
        return getRotulo(clazz);
    }

    AbstractBorder getDefaultBorder() {
        Border borda = BorderFactory.createEtchedBorder();
        TitledBorder titledBorder = BorderFactory.createTitledBorder(borda, getTitulo(), TitledBorder.CENTER,
                        TitledBorder.ABOVE_TOP);
        titledBorder.setTitleFont(new Font("Tahoma", Font.ITALIC, 11));
        titledBorder.setTitleColor(Color.WHITE);

        return titledBorder;
    }

    final WC_TextField<?> campoNome() {
        if ( campoNome == null ) {
            campoNome = new WC_TextField<>();
            campoNome.setLabel("Nome");
        }
        return campoNome;
    }

    final WC_TextField<?> campoSigla() {
        if ( campoSigla == null ) {
            campoSigla = new WC_TextField<>();
            campoSigla.setLabel("Sigla");
        }
        return campoSigla;
    }

    final WC_TextField<Professor> campoProfessor() {
        if ( campoProfessor == null ) {
            campoProfessor = new WC_TextField<Professor>(Professor.class);
        }
        return campoProfessor;
    }

    final WC_TextField<Aula> campoAula() {
        if ( campoAula == null ) {
            campoAula = new WC_TextField<Aula>(Aula.class);
        }
        return campoAula;
    }

    final WC_TextField<Dia> campoDiaSemana() {
        if ( campoDiaSemana == null ) {
            campoDiaSemana = new WC_TextField<Dia>(Dia.class);
        }
        return campoDiaSemana;
    }

    final WC_TextField<Horario> campoHorario() {
        if ( campoHorario == null ) {
            campoHorario = new WC_TextField<Horario>(Horario.class);
        }
        return campoHorario;
    }

    final WC_TextField<Turno> campoTurno() {
        if ( campoTurno == null ) {
            campoTurno = new WC_TextField<Turno>(Turno.class);
        }
        return campoTurno;
    }

    final WC_TextField<Sala> campoSala() {
        if ( campoSala == null ) {
            campoSala = new WC_TextField<Sala>(Sala.class);
        }
        return campoSala;
    }

    final WC_TextField<Curso> campoCurso() {
        if ( campoCurso == null ) {
            campoCurso = new WC_TextField<Curso>(Curso.class);
        }
        return campoCurso;
    }

    final WC_TextField<Semestre> campoSemestre() {
        if ( campoSemestre == null ) {
            campoSemestre = new WC_TextField<Semestre>(Semestre.class);
        }
        return campoSemestre;
    }

    final WC_TextField<Turma> campoTurma() {
        if ( campoTurma == null ) {
            campoTurma = new WC_TextField<Turma>(Turma.class);
        }
        return campoTurma;
    }

    public final JButton getjPreferencia() {
        if ( jPreferencia == null ) {
            jPreferencia = new JButton();
            jPreferencia.setText(getRotuloS(Preferencia.class));
            jPreferencia.setMaximumSize(new Dimension(Short.MAX_VALUE, 35));
            jPreferencia.setMinimumSize(new Dimension(80, 35));
        }
        return jPreferencia;
    }

}
