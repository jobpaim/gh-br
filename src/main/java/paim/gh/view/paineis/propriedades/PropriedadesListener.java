/**
 * @author temujin
 * Jul 7, 2012
 *
 */
package paim.gh.view.paineis.propriedades;

import java.util.EventListener;

import paim.wingchun.controller.WC_Event;



/**
 * @author temujin
 * Jul 7, 2012
 *
 */

public interface PropriedadesListener extends EventListener {

    public void selectProfessor(WC_Event e);

    public void selectAula(WC_Event e);

    public void selectTurma(WC_Event e);

    public void selectDia(WC_Event e);

    public void selectHorario(WC_Event e);

    public void selectTurno(WC_Event e);

    public void selectSala(WC_Event e);

    public void selectCurso(WC_Event e);

    public void selectSemestre(WC_Event e);

}