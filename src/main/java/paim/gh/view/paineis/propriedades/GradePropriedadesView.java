package paim.gh.view.paineis.propriedades;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import paim.wingchun.app.Reflections;
import paim.wingchun.view.AbstractUniqueView;
import paim.wingchun.view.componentes.WC_TextField;

import paim.gh.model.pojos.Grade;

/** @author paim 19/03/2014
 * @since */
public class GradePropriedadesView extends AbstractUniqueView<Grade> {

    private static final long serialVersionUID = 1L;

    private WC_TextField<?> campoSigla;

    private WC_TextField<?> campoNome;

    private WC_TextField<?> campoDescricao;

    private JPanel painel;

    public GradePropriedadesView() {
        super();
        setTitle(Reflections.getRotuloS(this.getpClass()));
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        Grade pojo = (Grade) objeto;

        campoNome().setText(pojo.getNome());
        campoSigla().setText(pojo.getSigla());
        campoSigla().setText(pojo.getDescricao());

        return pojo;
    }

    @Override
    protected JPanel getJPanelUsuario() {
        if ( painel == null ) {
            painel = new JPanel();
            painel.setLayout(new GridBagLayout());

            GridBagConstraints constraints1 = new GridBagConstraints();
            constraints1.anchor = GridBagConstraints.LINE_START;
            constraints1.fill = GridBagConstraints.HORIZONTAL;
            constraints1.gridx = 0;
            constraints1.gridy = 0;
            constraints1.insets = new Insets(0, 0, 5, 0);
            painel.add(campoNome(), constraints1);

            GridBagConstraints constraints3 = new GridBagConstraints();
            constraints3.fill = GridBagConstraints.HORIZONTAL;
            constraints3.anchor = GridBagConstraints.LINE_START;
            constraints3.gridx = 0;
            constraints3.gridy = 1;
            constraints3.insets = new Insets(0, 0, 5, 0);
            painel.add(campoSigla(), constraints3);

            GridBagConstraints constraints5 = new GridBagConstraints();
            constraints5.fill = GridBagConstraints.HORIZONTAL;
            constraints5.anchor = GridBagConstraints.LINE_START;
            constraints5.gridx = 0;
            constraints5.gridy = 2;
            constraints5.insets = new Insets(0, 0, 5, 0);
            painel.add(campoDescricao(), constraints5);

        }
        return painel;
    }

    private WC_TextField<?> campoNome() {
        if ( campoNome == null ) {
            campoNome = new WC_TextField<>();
            campoNome.setLabel("Nome");
        }
        return campoNome;
    }

    private WC_TextField<?> campoSigla() {
        if ( campoSigla == null ) {
            campoSigla = new WC_TextField<>();
            campoSigla.setLabel("Sigla");
        }
        return campoSigla;
    }

    private WC_TextField<?> campoDescricao() {
        if ( campoDescricao == null ) {
            campoDescricao = new WC_TextField<>();
            campoDescricao.setLabel("Descrição");
        }
        return campoDescricao;
    }

}
