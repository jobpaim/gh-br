/**
 * @author temujin Aug 31, 2011
 */
package paim.gh.view.paineis.arvore;

import java.util.EventListener;


/**
 * @author temujin Aug 31, 2011
 */
public interface ArvoreListener extends EventListener {

    public void selecao();
    
    public void add();

    public void addAll();

    public void remove();

    public void removeAll();

    public void show();

    public void showAll();

    public void hide();

    public void hideAll();

    public void addTurma();

    public void removeTurma();
    
  
}
