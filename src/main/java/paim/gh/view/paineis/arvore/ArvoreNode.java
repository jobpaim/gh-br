/**
 * @author temujin Sep 7, 2011
 */
package paim.gh.view.paineis.arvore;

import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.pojos.AbstractTabela;
import paim.gh.model.pojos.Aula;
import paim.gh.model.pojos.Professor;
import paim.gh.model.pojos.Turma;

/**
 * @author temujin Sep 7, 2011
 */
public class ArvoreNode extends DefaultMutableTreeNode {

    private static final long serialVersionUID = 1L;
    protected Pojo objeto;

    protected Class<? extends Pojo> classe;

    ArvoreNode(String userObject, Pojo pojo) {
        super(userObject);
        this.objeto = pojo;
        this.classe = pojo.getClass();
    }

    ArvoreNode(Class<? extends Pojo> classe) {
        super(Reflections.getRotuloS(classe));
        this.classe = classe;
    }
    
    
    public Class<? extends Pojo> getClasse() {
        return this.classe;
    }

    public Pojo getObjeto() {
        return this.objeto;
    }

    public boolean isNodeObjeto() {
        if ( objeto != null )
            return true;
        return false;
    }
    
    /** swap o nodo de objeto para classe e
     * @author paim 07/12/2011
     * @throws Exception
     * @return POJO, o objeto do nodo antes do swap
     */
    public Pojo swapNodoClasse() throws Exception {
        if ( !isNodeObjeto() )
            throw new Exception("Nodo nï¿½o eh objeto");
        Pojo o = objeto;
        objeto = null;
        return o;
    }

    /**swap o nodo de classe para objeto
     * @author paim 07/12/2011
     * @param objeto
     * @throws Exception
     * void
     */
    public void swapNodoObjeto(Pojo objeto) throws Exception {
        if ( isNodeObjeto() )
            throw new Exception("Nodo nï¿½o eh classe");

        this.objeto = objeto;
    }


    /**
     * adiciona nodo a este nodo
     * 
     * @author temujin Sep 11, 2011
     * @param pojo
     *            * void
     * @see DefaultMutableTreeNode#add(MutableTreeNode)
     */
    public void add(Pojo pojo) {
        ArvoreNode node;

        /** se for um alula */
        if ( pojo instanceof Aula ) {
            Aula aula = (Aula) pojo;
            node = new ArvoreNode(aula.getSigla(), pojo);

            if ( !aula.isTurmaUnica() ) {
                try {
                    // node.swapNodoClasse();
                    for ( Turma turma : aula.getTurmas() )
                        node.add(turma);
                }
                catch ( Exception e ) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        // Aula aula = (Aula) pojo;
        //
        // /* eh uma folha -> significa que esta adicionando turma pela primeira vez e deve-se preparar este nodo para tal */
        // if ( this.isLeaf() ) {
        //
        // TreeNode nodoPai = this.getParent();
        // int index = nodoPai.getIndex(this);
        //
        //
        // node = new ArvoreNode(((Aula) pojo).getSigla(), pojo);
        // }
        // /* ja tem turmas entao eh so adicionar mais uma */
        // else {
        //
        // }

        else if ( pojo instanceof Turma ) {
            node = new ArvoreNode(((Turma) pojo).getSigla(), pojo);
            /* nao tera filhos */
            node.setAllowsChildren(false);
        }
        else if ( pojo instanceof Professor ) {
            node = new ArvoreNode(((Professor) pojo).getSigla(), pojo);
            /* nao tera filhos */
            node.setAllowsChildren(false);
        }
        else {
            node = new ArvoreNode(((AbstractTabela) pojo).getSigla(), pojo);
        }
        add(node);
    }

    /**
     * adiciona nodos a este nodo.
     * 
     * @author temujin Sep 11, 2011
     * @param lista
     *            void
     * @see ArvoreNode#add(Pojo)
     */
    public void addAll(List<? extends Pojo> lista) {
        if ( lista.isEmpty() )
            return;

        for ( Pojo pojo : lista ) {
            add(pojo);
        }

    }

}
