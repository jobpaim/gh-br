package paim.gh.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

import paim.wingchun.view.GUIUtils;
import paim.wingchun.view.IVisaoApp;

import paim.gh.app.Facade;
import paim.gh.app.GHApp;
import paim.gh.controller.*;
import paim.gh.model.pojos.*;

import static paim.wingchun.app.Reflections.getRotuloS;
import static paim.wingchun.view.GUIUtils.getImageIcon;

/**
 * @author paim 03/11/2010*
 */
public class GHView extends JFrame implements IVisaoApp {

    private static final long serialVersionUID = 1L;

    private final int splitDividerSize = 2;

    private JMenuBar menuBar;

    private JToolBar jToolBar;

    private JPanel jAreaStatus;

    private JSplitPane jSplitPane;

    private JDesktopPane jDesktopPane;

    private JPanel jContentPane;

    private JSplitPane jSplitPaneSuperior;

    private JSplitPane jSplitPaneHorizontalRight;

    private JScrollPane jAreaArvore;

    private JScrollPane jAreaPropriedades;

    private JPanel jAreaFiltros;

    private JScrollPane jAreaGrade;

    private JMenuItem miTabelaSistema;

    private JMenuItem miAbout;

    private JCheckBoxMenuItem cbmishowSplash;

    private JMenu menuArquivo;

    private JMenu menuEditar;

    private JMenu menuTabela;

    private JMenu menuJanela;

    private JMenu menuAjuda;

    private JMenuItem createNewGH;

    private JMenuItem exit;

    private JMenuItem openGH;

    private JMenuItem closeGH;

    private JMenuItem saveGH;

    private JMenuItem deleteGH;

    private JMenuItem miDoscente;

    private JMenuItem miHorario;

    private JMenuItem miDiaSemana;

    private JMenuItem miDisciplina;

    private JMenuItem miTurno;

    private JMenuItem miCurso;

    private JMenuItem miSemestre;

    private JMenuItem miSala;

    public GHView() {
        super();
        initialize();
        // this.addWindowListener(new WindowAdapter() {
        //
        // @Override
        // public void windowClosing(WindowEvent e) {
        // GHApp.getInstancia().sairApp();
        // }
        // });
    }

    private void initialize() {
        this.setSize(900, 600);
        this.setResizable(true);
        this.setMinimumSize(new Dimension(1024, 768));
        this.setPreferredSize(new Dimension(1024, 768));
        this.setJMenuBar(getmenuBar());
        this.setContentPane(getjContentPane());

        // FIXME ira para o framework wc

    }

    /*@formatter:off*/
    /**
     *  ------------------------------------------------------------------------------------------------------------------------------ <BR>
     *  | {@link #getjToolBar()}                                                                                                                         | <BR>
     *  ------------------------------------------------------------------------------------------------------------------------------ <BR>
     *  |                                     |                                                                 |{@link #getjAreaPropriedades()} | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  | {@link #getjAreaArvore()}           |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |                                     |                                                                 |                                                      | <BR>
     *  |-----------------------------------------------------------------------------------------------------------------------------| <BR>
     *  |                                                                                                                                                              | <BR>
     *  | {@link #getjAreaFiltros()}                                                                                                                    | <BR>
     *  |                                                                                                                                                              | <BR>
     *  |                                                                                                                                                              | <BR>
     *  ------------------------------------------------------------------------------------------------------------------------------ <BR>
     *  | {@link #getjAreaStatus()}                                                                                                                  | <BR>
     *  ------------------------------------------------------------------------------------------------------------------------------- <BR>
     */
    /*@formatter:on*/

    private JPanel getjContentPane() {
        if ( jContentPane == null ) {
            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints2.gridx = 0;
            gridBagConstraints2.gridy = 2;

            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.fill = GridBagConstraints.BOTH;
            gridBagConstraints1.weightx = 1D;
            gridBagConstraints1.weighty = 1D;
            gridBagConstraints1.gridx = 0;
            gridBagConstraints1.gridy = 1;

            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
            gridBagConstraints.weightx = 1D;
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;

            jContentPane = new JPanel();
            jContentPane.setLayout(new GridBagLayout());
            jContentPane.add(getjToolBar(), gridBagConstraints);
            jContentPane.add(getjSplitPane(), gridBagConstraints1);
            jContentPane.add(getjAreaStatus(), gridBagConstraints2);
        }
        return jContentPane;
    }

    public JToolBar getjToolBar() {
        if ( jToolBar == null ) {
            jToolBar = new JToolBar();
        }
        return jToolBar;
    }

    public JPanel getjAreaStatus() {
        if ( jAreaStatus == null ) {
            jAreaStatus = new JPanel();
        }
        return jAreaStatus;
    }

    private JSplitPane getjSplitPane() {
        if ( jSplitPane == null ) {
            jSplitPane = new JSplitPane();
            jSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
            jSplitPane.setTopComponent(getjSplitPaneSuperior());
            jSplitPane.setBottomComponent(getjAreaFiltros());
            jSplitPane.setOneTouchExpandable(true);
            jSplitPane.setResizeWeight(0.75D);
            jSplitPane.setDividerLocation(0.8D);
            jSplitPane.setDividerSize(splitDividerSize);
        }
        return jSplitPane;
    }

    private JSplitPane getjSplitPaneSuperior() {
        if ( jSplitPaneSuperior == null ) {
            jSplitPaneSuperior = new JSplitPane();
            jSplitPaneSuperior.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
            jSplitPaneSuperior.setLeftComponent(getjAreaArvore());
            jSplitPaneSuperior.setRightComponent(getjSplitPaneHorizontaRight());
            jSplitPaneSuperior.setOneTouchExpandable(true);
            jSplitPaneSuperior.setResizeWeight(0.1D);
            jSplitPaneSuperior.setDividerLocation(0.1D);
            jSplitPaneSuperior.setDividerSize(splitDividerSize);

        }
        return jSplitPaneSuperior;
    }

    private JSplitPane getjSplitPaneHorizontaRight() {
        if ( jSplitPaneHorizontalRight == null ) {
            jSplitPaneHorizontalRight = new JSplitPane();
            jSplitPaneHorizontalRight.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
            jSplitPaneHorizontalRight.setLeftComponent(getjAreaGrade());
            jSplitPaneHorizontalRight.setRightComponent(getjAreaPropriedades());
            jSplitPaneHorizontalRight.setOneTouchExpandable(true);
            jSplitPaneHorizontalRight.setResizeWeight(0.8D);
            jSplitPaneHorizontalRight.setDividerLocation(0.8D);
            jSplitPaneHorizontalRight.setDividerSize(splitDividerSize);
        }
        return jSplitPaneHorizontalRight;
    }

    public JScrollPane getjAreaArvore() {
        if ( jAreaArvore == null ) {
            jAreaArvore = new JScrollPane();
            jAreaArvore.setName("jAreaArvore");
        }
        return jAreaArvore;
    }

    public JScrollPane getjAreaPropriedades() {
        if ( jAreaPropriedades == null ) {
            jAreaPropriedades = new JScrollPane();
            jAreaPropriedades.setName("jAreaPropriedades");
        }
        return jAreaPropriedades;
    }

    public JPanel getjAreaFiltros() {
        if ( jAreaFiltros == null ) {
            jAreaFiltros = new JPanel();
            jAreaFiltros.setName("jAreaFiltros");
        }
        return jAreaFiltros;
    }

    public JScrollPane getjAreaGrade() {
        if ( jAreaGrade == null ) {
            jAreaGrade = new JScrollPane();
            jAreaGrade.setName("jAreaGrade");
            jAreaGrade.setViewportView(getjDesktopPane());
        }
        return jAreaGrade;
    }

    @Override
    public JDesktopPane getjDesktopPane() {
        if ( jDesktopPane == null ) {
            jDesktopPane = new JDesktopPane();
            DefaultDesktopManager desktopManager = new DefaultDesktopManager();
            jDesktopPane.setDesktopManager(desktopManager);
        }
        return jDesktopPane;
    }

    private JMenuBar getmenuBar() {
        if ( menuBar == null ) {
            menuBar = new JMenuBar();
            menuBar.add(getMenuArquivo());
            menuBar.add(getMenuEditar());
            menuBar.add(getMenuTabela());
            // menuBar.add(getMenuUtilitarios());
            // menuBar.add(getMenuFerramentas());
            menuBar.add(getMenuJanela());
            menuBar.add(getMenuAjuda());
        }
        return menuBar;
    }

    private JMenu getMenuArquivo() {
        if ( menuArquivo == null ) {
            menuArquivo = new JMenu();
            menuArquivo.setText("Arquivo");
            menuArquivo.setName("m_Arquivo");
            menuArquivo.setEnabled(true);
            menuArquivo.add(getCreateNewGH());
            menuArquivo.add(new JSeparator());
            menuArquivo.add(getOpenGH());
            menuArquivo.add(getCloseGH());
            menuArquivo.add(new JSeparator());
            menuArquivo.add(getSaveGH());
            menuArquivo.add(getDeleteGH());
            menuArquivo.add(new JSeparator());
            menuArquivo.add(getExit());

        }
        return menuArquivo;
    }

    private JMenu getMenuEditar() {
        if ( menuEditar == null ) {
            menuEditar = new JMenu();
            menuEditar.setText("Editar");
            menuEditar.setEnabled(false);
        }
        return menuEditar;
    }

    private JMenu getMenuTabela() {
        if ( menuTabela == null ) {
            menuTabela = new JMenu();
            menuTabela.setText("Tabelas");
            menuTabela.setEnabled(true);
            menuTabela.add(new JSeparator());
            menuTabela.add(getMiDoscente());
            menuTabela.add(getMiHorario());
            menuTabela.add(getMiDiaSemana());
            menuTabela.add(getMiDisciplina());
            menuTabela.add(getMiTurno());
            menuTabela.add(getMiCurso());
            menuTabela.add(getMiSemestre());
            menuTabela.add(getMiSala());
        }
        return menuTabela;
    }

    private JMenu getMenuJanela() {
        if ( menuJanela == null ) {
            menuJanela = new JMenu();
            menuJanela.setText("Janela");
            menuJanela.setEnabled(false);
        }
        return menuJanela;
    }

    private JMenu getMenuAjuda() {
        if ( menuAjuda == null ) {
            menuAjuda = new JMenu();
            menuAjuda.setText("Ajuda");
            menuAjuda.setEnabled(false);
        }
        return menuAjuda;
    }

    private JMenuItem getExit() {
        if ( exit == null ) {
            exit = new JMenuItem();
            exit.setText("Exit");
            exit.setName("mi_Exit");
            exit.setMnemonic(KeyEvent.VK_UNDEFINED);
            exit.setIcon(getImageIcon("icon.menu.exit"));
            exit.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    GHApp.getInstancia().sairApp();
                }
            });
        }
        return exit;
    }

    private JMenuItem getCreateNewGH() {
        if ( createNewGH == null ) {
            createNewGH = new JMenuItem();
            createNewGH.setText("Novo");
            createNewGH.setName("mi_Novo");
            createNewGH.setEnabled(true);
            createNewGH.setMnemonic(KeyEvent.VK_UNDEFINED);
            createNewGH.setIcon(getImageIcon("icon.menu.new"));
            createNewGH.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Facade.getInstancia().createNewGH();
                }
            });
        }
        return createNewGH;
    }

    private JMenuItem getOpenGH() {
        if ( openGH == null ) {
            openGH = new JMenuItem();
            openGH.setText("Open");
            openGH.setName("mi_Open");
            openGH.setMnemonic(KeyEvent.VK_UNDEFINED);
            openGH.setIcon(getImageIcon("icon.menu.open"));
            openGH.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Facade.getInstancia().openGH();
                }
            });
        }
        return openGH;
    }

    private JMenuItem getCloseGH() {
        if ( closeGH == null ) {
            closeGH = new JMenuItem();
            closeGH.setEnabled(false);
            closeGH.setText("Close");
            closeGH.setName("mi_Close");
            closeGH.setMnemonic(KeyEvent.VK_UNDEFINED);
            closeGH.setIcon(getImageIcon("icon.menu.close"));
            closeGH.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Facade.getInstancia().closeGH();
                }
            });
        }
        return closeGH;
    }

    private JMenuItem getSaveGH() {
        if ( saveGH == null ) {
            saveGH = new JMenuItem();
            saveGH.setEnabled(false);
            saveGH.setText("Save");
            saveGH.setName("mi_Save");
            saveGH.setMnemonic(KeyEvent.VK_UNDEFINED);
            saveGH.setIcon(getImageIcon("icon.menu.save"));
            saveGH.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Facade.getInstancia().saveGH();
                }
            });
        }
        return saveGH;
    }

    private JMenuItem getDeleteGH() {
        if ( deleteGH == null ) {
            deleteGH = new JMenuItem();
            deleteGH.setEnabled(false);
            deleteGH.setText("Delete");
            deleteGH.setName("mi_Delete");
            deleteGH.setMnemonic(KeyEvent.VK_UNDEFINED);
            deleteGH.setIcon(getImageIcon("icon.menu.delete"));
            deleteGH.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Facade.getInstancia().deleteGH();
                }
            });
        }
        return deleteGH;
    }

    private JMenuItem getMiDoscente() {
        if ( miDoscente == null ) {
            miDoscente = new JMenuItem();
            miDoscente.setText(getRotuloS(Doscente.class));
            miDoscente.setIcon(getImageIcon(GUIUtils.EMPTY_ICON));
            miDoscente.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new DoscenteController();
                }
            });
        }
        return miDoscente;
    }

    private JMenuItem getMiHorario() {
        if ( miHorario == null ) {
            miHorario = new JMenuItem();
            miHorario.setText(getRotuloS(Horario.class));
            miHorario.setIcon(getImageIcon(GUIUtils.EMPTY_ICON));
            miHorario.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new HorarioController();
                }
            });
        }
        return miHorario;
    }

    private JMenuItem getMiDiaSemana() {
        if ( miDiaSemana == null ) {
            miDiaSemana = new JMenuItem();
            miDiaSemana.setText(getRotuloS(Dia.class));
            miDiaSemana.setIcon(getImageIcon(GUIUtils.EMPTY_ICON));
            miDiaSemana.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new DiaController();
                }
            });
        }
        return miDiaSemana;

    }

    private JMenuItem getMiDisciplina() {
        if ( miDisciplina == null ) {
            miDisciplina = new JMenuItem();
            miDisciplina.setText(getRotuloS(Disciplina.class));
            miDisciplina.setIcon(getImageIcon(GUIUtils.EMPTY_ICON));
            miDisciplina.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new DisciplinaController();
                }
            });
        }
        return miDisciplina;
    }

    private JMenuItem getMiTurno() {
        if ( miTurno == null ) {
            miTurno = new JMenuItem();
            miTurno.setText(getRotuloS(Turno.class));
            miTurno.setIcon(getImageIcon(GUIUtils.EMPTY_ICON));
            miTurno.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new TurnoController();
                }
            });
        }
        return miTurno;
    }

    private JMenuItem getMiCurso() {
        if ( miCurso == null ) {
            miCurso = new JMenuItem();
            miCurso.setText(getRotuloS(Curso.class));
            miCurso.setIcon(getImageIcon(GUIUtils.EMPTY_ICON));
            miCurso.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new CursoController();
                }
            });
        }
        return miCurso;
    }

    private JMenuItem getMiSemestre() {
        if ( miSemestre == null ) {
            miSemestre = new JMenuItem();
            miSemestre.setText(getRotuloS(Semestre.class));
            miSemestre.setIcon(getImageIcon(GUIUtils.EMPTY_ICON));
            miSemestre.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new SemestreController();
                }
            });
        }
        return miSemestre;
    }

    private JMenuItem getMiSala() {
        if ( miSala == null ) {
            miSala = new JMenuItem();
            miSala.setText(getRotuloS(Sala.class));
            miSala.setIcon(getImageIcon(GUIUtils.EMPTY_ICON));
            miSala.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new SalaController();
                }
            });
        }
        return miSala;
    }

    @SuppressWarnings("unused")
    private JMenuItem getMiTabelaSistema() {
        return this.miTabelaSistema;
    }

    @SuppressWarnings("unused")
    private JMenuItem getMiAbout() {
        miAbout.setMnemonic(KeyEvent.VK_UNDEFINED);
        miAbout.setName("mi_" + "About");
        return this.miAbout;
    }

    @SuppressWarnings("unused")
    private JCheckBoxMenuItem getCbmishowSplash() {
        return this.cbmishowSplash;
    }

}
