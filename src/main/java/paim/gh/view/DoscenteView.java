package paim.gh.view;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import paim.wingchun.app.Reflections;
import paim.wingchun.view.componentes.WC_Label;

import paim.gh.model.pojos.Doscente;

/**
 * @author paim 16/05/2011
 */
public class DoscenteView extends AbstractTabelaView<Doscente> {

    private static final long serialVersionUID = 1L;

    private JPanel panel;

    private JPanel panelSpinners;

    private JSpinner nCargaHoraria;

    private WC_Label jlnCargaHoraria;

    public DoscenteView() {
        super();
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        Doscente pojo = (Doscente) super.v_setObjeto(objeto);
        /* coloca o que esta no objeto, se for null eu pego o default do jspinner */
        Integer value = pojo.getCargaHoraria() == null ? (Integer) getnCargaHoraria().getValue() : pojo
                .getCargaHoraria();
        getnCargaHoraria().getModel().setValue(value);
        return pojo;
    }

    @Override
    protected JPanel getJPanel() {
        if ( panel == null ) {
            panel = super.getJPanel();
            GridBagConstraints constraints0 = new GridBagConstraints();
            constraints0.anchor = GridBagConstraints.NORTH;
            constraints0.gridx = 0;
            constraints0.gridy = GridBagConstraints.RELATIVE;
            constraints0.gridwidth = GridBagConstraints.REMAINDER;
            constraints0.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelSpinners(), constraints0);
        }
        return panel;
    }

    private JPanel getPanelSpinners() {
        if ( panelSpinners == null ) {
            panelSpinners = new JPanel();
            panelSpinners.setLayout(new FlowLayout());
            panelSpinners.add(getjlnCargaHoraria());
            panelSpinners.add(getnCargaHoraria());
        }
        return panelSpinners;
    }

    public JSpinner getnCargaHoraria() {
        if ( nCargaHoraria == null ) {
            SpinnerModel model = new SpinnerNumberModel(0, 0, 60, 1);
            nCargaHoraria = new JSpinner(model);
            ((JSpinner.DefaultEditor) nCargaHoraria.getEditor()).getTextField().setEnabled(false);
        }
        return nCargaHoraria;
    }

    private WC_Label getjlnCargaHoraria() {
        if ( jlnCargaHoraria == null ) {
            jlnCargaHoraria = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "cargaHoraria");
            jlnCargaHoraria.setText(rotulo);
        }
        return jlnCargaHoraria;
    }

    @Override
    protected void setMapLabel() {
        super.setMapLabel();
        getMapLabel().put("cargaHoraria", getjlnCargaHoraria());
    }

}
