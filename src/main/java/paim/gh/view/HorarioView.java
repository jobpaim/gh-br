package paim.gh.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SwingConstants;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.swing.JSpinnerTime;
import paim.wingchun.view.componentes.WC_Label;

import paim.gh.model.pojos.Horario;

/**
 * @author paim 16/05/2011
 */
public class HorarioView extends AbstractTabelaView<Horario> {

    private static final long serialVersionUID = 1L;

    public HorarioView() {
        super();
    }

    private JSpinner jInicio;

    private JSpinner jFim;

    private JPanel panel;

    private JPanel panelSpinners;

    private JPanel panelTurno;

    private WC_Label jlTurno;

    private JComboBox<Pojo> jTurno;

    private WC_Label jlInicio;

    private WC_Label jlFim;

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        Horario pojo = (Horario) super.v_setObjeto(objeto);
        /* coloca o que esta no objeto */
        getjInicio().getModel().setValue(pojo.getInicio());
        getjFim().getModel().setValue(pojo.getFim());

        getJTurno().getModel().setSelectedItem(pojo.getTurno());
        return pojo;
    }

    @Override
    protected JPanel getJPanel() {
        if ( panel == null ) {
            panel = super.getJPanel();
            GridBagConstraints constraints0 = new GridBagConstraints();
            constraints0.anchor = GridBagConstraints.NORTH;
            constraints0.gridx = 0;
            constraints0.gridy = GridBagConstraints.RELATIVE;
            constraints0.gridwidth = GridBagConstraints.REMAINDER;
            constraints0.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelSpinners(), constraints0);

            GridBagConstraints constraints1 = new GridBagConstraints();
            constraints1.anchor = GridBagConstraints.NORTH;
            constraints1.gridx = 0;
            constraints1.gridy = GridBagConstraints.RELATIVE;
            constraints1.gridwidth = GridBagConstraints.REMAINDER;
            constraints1.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelTurno(), constraints1);

        }
        return panel;
    }

    private WC_Label getjlInicio() {
        if ( jlInicio == null ) {
            jlInicio = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "inicio");
            jlInicio.setText(rotulo);
        }
        return jlInicio;
    }

    private WC_Label getjlFim() {
        if ( jlFim == null ) {
            jlFim = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "fim");
            jlFim.setText(rotulo);
        }
        return jlFim;
    }

    private JPanel getPanelSpinners() {
        if ( panelSpinners == null ) {
            panelSpinners = new JPanel();
            panelSpinners.setLayout(new FlowLayout());
            panelSpinners.add(getjlInicio());
            panelSpinners.add(getjInicio());
            panelSpinners.add(new JSeparator(SwingConstants.VERTICAL));
            panelSpinners.add(getjlFim());
            panelSpinners.add(getjFim());
        }
        return panelSpinners;
    }

    public JSpinner getjInicio() {
        if ( jInicio == null ) {
            jInicio = new JSpinnerTime();
        }
        return jInicio;
    }

    public JSpinner getjFim() {
        if ( jFim == null ) {
            jFim = new JSpinnerTime();
        }
        return jFim;
    }

    private JPanel getPanelTurno() {
        if ( panelTurno == null ) {
            panelTurno = new JPanel();
            panelTurno.setLayout(new FlowLayout());
            panelTurno.add(getjlTurno());
            panelTurno.add(getJTurno());
        }
        return panelTurno;
    }

    private WC_Label getjlTurno() {
        if ( jlTurno == null ) {
            jlTurno = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "Turno");
            jlTurno.setText(rotulo);
        }
        return jlTurno;
    }

    public JComboBox<Pojo> getJTurno() {
        if ( jTurno == null ) {
            jTurno = new JComboBox<Pojo>();
            jTurno.setEnabled(true);
            Dimension d = new Dimension(100, 20);
            jTurno.setMinimumSize(d);
            jTurno.setMaximumSize(d);
            jTurno.setPreferredSize(d);

        }
        return jTurno;
    }

    @Override
    protected void setMapLabel() {
        super.setMapLabel();
        getMapLabel().put("inicio", getjlInicio());
        getMapLabel().put("fim", getjlFim());
        getMapLabel().put("turno", getjlTurno());

    }

}
