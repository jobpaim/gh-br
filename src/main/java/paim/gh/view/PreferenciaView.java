package paim.gh.view;


import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import paim.wingchun.app.Reflections;
import paim.wingchun.view.AbstractListView;
import paim.wingchun.view.GUIUtils;

import paim.gh.model.Opcionalidade;
import paim.gh.model.pojos.Preferencia;

/** @author paim 30/09/2013
 * @since */
public class PreferenciaView extends AbstractListView<Preferencia> {

    private static final long serialVersionUID = 1013872524426993004L;

    private JPanel jpanel;

    private JTabbedPane jTabbedPane;

    public PreferenciaView(Class<Preferencia> pClass) {
        super(pClass);
        this.setTitle(Reflections.getRotuloS(this.getpClass()));
    }

    @Override
    protected JPanel getJPanelUsuario() {
        if ( jpanel == null ) {
            jpanel = new JPanel();
            jpanel.setLayout(new BorderLayout());
            jpanel.setBorder(GUIUtils.getBordaPadrao("Selecione"));
            jpanel.add(getjTabbedPane(), BorderLayout.CENTER);
        }
        return jpanel;
    }

    public JTabbedPane getjTabbedPane() {
        if ( jTabbedPane == null ) {
            jTabbedPane = new JTabbedPane();
            jTabbedPane.setToolTipText(null);
            jTabbedPane.setTabPlacement(JTabbedPane.TOP);
            ImageIcon imageIcon = GUIUtils.getImageIcon("icon.preferencia."
                            + Opcionalidade.INEVITAVEL.name().toLowerCase());
            jTabbedPane.addTab("Tab 1", imageIcon, getInevitavelPanel(), "tip Does nothing");
            imageIcon = GUIUtils.getImageIcon("icon.preferencia." + Opcionalidade.PREFERIVEL.name().toLowerCase());
            jTabbedPane.addTab("Tab 2", imageIcon, getInevitavelPanel(), "tip Does nothing");
            imageIcon = GUIUtils.getImageIcon("icon.preferencia." + Opcionalidade.POSSIVEL.name().toLowerCase());
            jTabbedPane.addTab("Tab 3", imageIcon, getInevitavelPanel(), "tip Does nothing");
            imageIcon = GUIUtils.getImageIcon("icon.preferencia." + Opcionalidade.EVITAVEL.name().toLowerCase());
            jTabbedPane.addTab("Tab 4", imageIcon, getInevitavelPanel(), "tip Does nothing");
            imageIcon = GUIUtils.getImageIcon("icon.preferencia." + Opcionalidade.IMPOSSIVEL.name().toLowerCase());
            jTabbedPane.addTab("Tab 5", imageIcon, getInevitavelPanel(), "tip Does nothing");
            imageIcon = GUIUtils.getImageIcon("icon.preferencia.colisao");
            jTabbedPane.addTab("Tab 6", imageIcon, getInevitavelPanel(), "tip Does nothing");
        }
        return jTabbedPane;
    }

    private JPanel inevitavelPanel;

    private JPanel getInevitavelPanel() {
        if ( inevitavelPanel == null ) {
            inevitavelPanel = new JPanel();
        }
        return inevitavelPanel;
    }

    private JPanel preferivelPanel;

    private JPanel possivelPanel;

    private JPanel evitavelPanel;

    private JPanel impossivelPanel;

    public JPanel getPreferivelPanel() {
        if ( preferivelPanel == null ) {
            preferivelPanel = new JPanel();

        }
        return preferivelPanel;
    }

    @Override
    protected Object v_getObjeto() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getHelpId() {
        // TODO Auto-generated method stub
        return null;
    }

    public static void main(String[] args) {
        new PreferenciaView(null);
    }

}
