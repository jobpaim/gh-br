package paim.gh.view;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import paim.wingchun.app.Reflections;
import paim.wingchun.view.componentes.WC_Label;

import paim.gh.model.pojos.Semestre;

/**
 * @author paim 16/05/2011
 */
public class SemestreView extends AbstractTabelaView<Semestre> {

    private static final long serialVersionUID = 1L;

    private JPanel panel;

    private JPanel panelSpinners;

    private JSpinner nSemestre;

    public SemestreView() {
        super();
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        // FIXME observe que esta iniciando o valor no no POJO, teria como nao ser assim?
        Semestre pojo = (Semestre) super.v_setObjeto(objeto);
        /* coloca o que esta no objeto, se for null eu pego o default do jspinner */
        Integer value = pojo.getNumSemestre() == null ? (Integer) getnSemestre().getValue() : pojo.getNumSemestre();
        getnSemestre().getModel().setValue(value);

        return pojo;
    }

    @Override
    protected JPanel getJPanel() {
        if ( panel == null ) {
            panel = super.getJPanel();
            GridBagConstraints constraints0 = new GridBagConstraints();
            constraints0.anchor = GridBagConstraints.NORTH;
            constraints0.gridx = 0;
            constraints0.gridy = GridBagConstraints.RELATIVE;
            constraints0.gridwidth = GridBagConstraints.REMAINDER;
            constraints0.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelSpinners(), constraints0);
        }
        return panel;
    }

    private JPanel getPanelSpinners() {
        if ( panelSpinners == null ) {
            panelSpinners = new JPanel();
            panelSpinners.setLayout(new FlowLayout());
            panelSpinners.add(getjlnumSemestre());
            panelSpinners.add(getnSemestre());
        }
        return panelSpinners;
    }

    public JSpinner getnSemestre() {
        if ( nSemestre == null ) {
            SpinnerModel model = new SpinnerNumberModel(0, 0, 16, 1);
            nSemestre = new JSpinner(model);
        }
        return nSemestre;
    }

    private WC_Label jlnumSemestre;

    private WC_Label getjlnumSemestre() {
        if ( jlnumSemestre == null ) {
            jlnumSemestre = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "numSemestre");
            jlnumSemestre.setText(rotulo);
        }
        return jlnumSemestre;
    }

    @Override
    protected void setMapLabel() {
        super.setMapLabel();
        getMapLabel().put("numSemestre", getjlnumSemestre());
    }
}
