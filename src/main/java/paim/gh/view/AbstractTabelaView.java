package paim.gh.view;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JTextField;

import paim.wingchun.app.Reflections;
import paim.wingchun.view.AbstractSimpleView;
import paim.wingchun.view.componentes.WC_Label;

import paim.gh.model.pojos.AbstractTabela;

import static paim.wingchun.app.Reflections.getRotulo;

/** classe que centraliza quase toda visao de qualquer item do menu tabela<BR>
 * manter tabela
 *
 * @author paim 12/05/2011 */
public abstract class AbstractTabelaView<P extends AbstractTabela> extends AbstractSimpleView<P> {

    private static final long serialVersionUID = 1L;

    JPanel jPanel;

    private WC_Label jlSigla;

    private JTextField jSigla;

    private WC_Label jlNome;

    private JTextField jNome;

    private WC_Label jlDescricao;

    private JTextField jDescricao;

    protected AbstractTabelaView() {
        super();
        this.setTitle(Reflections.getRotuloS(this.getpClass()));
    }

    @Override
    protected JPanel getJPanel() {
        if ( jPanel == null ) {
            jPanel = new JPanel();
            jPanel.setLayout(new GridBagLayout());

            GridBagConstraints constraints1 = new GridBagConstraints();
            constraints1.anchor = GridBagConstraints.LINE_END;
            constraints1.gridx = 0;
            constraints1.gridy = 0;
            constraints1.insets = new Insets(0, 0, 5, 0);
            jPanel.add(getjlNome(), constraints1);

            GridBagConstraints constraints2 = new GridBagConstraints();
            constraints2.fill = GridBagConstraints.HORIZONTAL;
            constraints2.anchor = GridBagConstraints.LINE_START;
            constraints2.gridx = 1;
            constraints2.gridy = 0;
            constraints2.weightx = 1D;
            constraints2.insets = new Insets(0, 5, 5, 0);
            jPanel.add(getJNome(), constraints2);

            GridBagConstraints constraints3 = new GridBagConstraints();
            constraints3.anchor = GridBagConstraints.EAST;
            constraints3.gridx = 0;
            constraints3.gridy = 1;
            constraints3.insets = new Insets(0, 0, 5, 0);
            jPanel.add(getjlSigla(), constraints3);

            GridBagConstraints constraints4 = new GridBagConstraints();
            constraints4.anchor = GridBagConstraints.LINE_START;
            constraints4.gridx = 1;
            constraints4.gridy = 1;
            constraints4.insets = new Insets(0, 5, 5, 0);
            jPanel.add(getJSigla(), constraints4);

            GridBagConstraints constraints5 = new GridBagConstraints();
            constraints5.anchor = GridBagConstraints.EAST;
            constraints5.gridx = 0;
            constraints5.gridy = 2;
            constraints5.insets = new Insets(0, 0, 5, 0);
            jPanel.add(getjlDescricao(), constraints5);

            GridBagConstraints constraints6 = new GridBagConstraints();
            constraints6.fill = GridBagConstraints.HORIZONTAL;
            constraints6.anchor = GridBagConstraints.LINE_START;
            constraints6.gridx = 1;
            constraints6.gridy = 2;
            constraints6.insets = new Insets(0, 5, 5, 0);
            jPanel.add(getJDescricao(), constraints6);

        }
        return jPanel;
    }

    private WC_Label getjlNome() {
        if ( jlNome == null ) {
            jlNome = new WC_Label();
            String rotulo = getRotulo(getpClass(), "nome");
            jlNome.setText(rotulo);
        }
        return jlNome;
    }

    private WC_Label getjlSigla() {
        if ( jlSigla == null ) {
            jlSigla = new WC_Label();
            String rotulo = getRotulo(getpClass(),"sigla");
            jlSigla.setText(rotulo);
        }
        return jlSigla;
    }

    private WC_Label getjlDescricao() {
        if ( jlDescricao == null ) {
            jlDescricao = new WC_Label();
            String rotulo = getRotulo(getpClass(), "descricao");
            jlDescricao.setText(rotulo);

        }
        return jlDescricao;
    }

    public JTextField getJNome() {
        if ( jNome == null ) {
            jNome = new JTextField();
            Dimension d = new Dimension(250, 20);
            jNome.setMinimumSize(d);
            jNome.setPreferredSize(d);

        }
        return jNome;
    }

    public JTextField getJSigla() {
        if ( jSigla == null ) {
            jSigla = new JTextField();
            Dimension d = new Dimension(100, 20);
            jSigla.setMinimumSize(d);
            jSigla.setMaximumSize(d);
            jSigla.setPreferredSize(d);
        }
        return jSigla;
    }

    public JTextField getJDescricao() {
        if ( jDescricao == null ) {
            jDescricao = new JTextField();
            Dimension d = new Dimension(250, 20);
            jDescricao.setMinimumSize(d);
            jDescricao.setPreferredSize(d);
        }
        return jDescricao;
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        AbstractTabela pojo = (AbstractTabela) objeto;

        getJNome().setText(pojo.getNome());
        getJSigla().setText(pojo.getSigla());
        getJDescricao().setText(pojo.getDescricao());

        // TODO deve setar foco aqui se necessario
        return pojo;
    }

    @Override
    protected void setMapLabel() {
        getMapLabel().put("descricao", getjlDescricao());
        getMapLabel().put("nome", getjlNome());
        getMapLabel().put("sigla", getjlSigla());
    }

}
