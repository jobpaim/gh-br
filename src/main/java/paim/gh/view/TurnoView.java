package paim.gh.view;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SwingConstants;

import paim.wingchun.app.Reflections;
import paim.wingchun.swing.JSpinnerTime;
import paim.wingchun.view.componentes.WC_Label;

import paim.gh.model.Turnos;
import paim.gh.model.pojos.Turno;

/**
 * @author paim 16/05/2011
 */
public class TurnoView extends AbstractTabelaView<Turno> {

    private static final long serialVersionUID = 1L;

    private JPanel panel;

    private JPanel panelLista;

    private List<JRadioButton> listaRadio;

    private WC_Label jlTurno;

    private JSpinner jInicio;

    private JSpinner jFim;

    private JPanel panelSpinners;

    private WC_Label jlInicio;

    private WC_Label jlFim;

    public TurnoView() {
        super();
    }

    /* (non-Javadoc)
     * @see paim.gh.visao.VGGHTabela#setObjeto(java.lang.Object) */
    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        Turno pojo = (Turno) super.v_setObjeto(objeto);

        Turnos turnos = pojo.getTurno();

        /*seta os radiobuttons*/
        if ( turnos != null ) {
            /* encontra na lista por nome do radio, e seleciona-o */
            for ( JRadioButton radioButton : getListaRadioButtons() ) {
                if ( radioButton.getName().equals(turnos.name()) ) {
                    radioButton.setSelected(true);
                    break;
                }
            }
        }
        else {
            /* se objeto novo provavelmente, entao nao seleciona nada */
            getButtonGroup().clearSelection();
        }

        /*seta os spinners*/
        /* coloca o que esta no objeto */
        getjInicio().getModel().setValue(pojo.getInicio());
        getjFim().getModel().setValue(pojo.getFim());

        return pojo;
    }

    @Override
    protected JPanel getJPanel() {
        if ( panel == null ) {
            panel = super.getJPanel();
            GridBagConstraints constraints0 = new GridBagConstraints();
            constraints0.anchor = GridBagConstraints.NORTH;
            constraints0.gridx = 0;
            constraints0.gridy = GridBagConstraints.RELATIVE;
            constraints0.gridwidth = GridBagConstraints.REMAINDER;
            constraints0.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelLista(), constraints0);

            GridBagConstraints constraints1 = new GridBagConstraints();
            constraints1.anchor = GridBagConstraints.NORTH;
            constraints1.gridx = 0;
            constraints1.gridy = GridBagConstraints.RELATIVE;
            constraints1.gridwidth = GridBagConstraints.REMAINDER;
            constraints1.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelSpinners(), constraints1);
        }
        return panel;
    }

    private ButtonGroup buttonGroup;

    public ButtonGroup getButtonGroup() {
        if ( buttonGroup == null ) {
            buttonGroup = new ButtonGroup();
        }
        return buttonGroup;
    }

    private JPanel getPanelLista() {
        if ( panelLista == null ) {
            panelLista = new JPanel();
            panelLista.setLayout(new FlowLayout());
            panelLista.add(getjlTurno());
            /* adicionando os radioButtons */
            for ( JRadioButton rButton : getListaRadioButtons() ) {
                getButtonGroup().add(rButton);
                panelLista.add(rButton);
            }
        }
        return panelLista;
    }

    public final List<JRadioButton> getListaRadioButtons() {

        if ( listaRadio == null ) {
            listaRadio = new ArrayList<>();
            for ( Turnos turnos : Turnos.values() ) {
                JRadioButton rButton = new JRadioButton(turnos.getNome());
                /* apenas para saber qual botao depois, prefiro por enum */
                rButton.setActionCommand(turnos.name());
                rButton.setName(turnos.name());
                listaRadio.add(rButton);
            }
        }
        return listaRadio;
    }

    private WC_Label getjlTurno() {
        if ( jlTurno == null ) {
            jlTurno = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "turno");
            jlTurno.setText(rotulo);
        }
        return jlTurno;
    }

    private WC_Label getjlInicio() {
        if ( jlInicio == null ) {
            jlInicio = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "inicio");
            jlInicio.setText(rotulo);
        }
        return jlInicio;
    }

    private WC_Label getjlFim() {
        if ( jlFim == null ) {
            jlFim = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "fim");
            jlFim.setText(rotulo);
        }
        return jlFim;
    }

    private JPanel getPanelSpinners() {
        if ( panelSpinners == null ) {
            panelSpinners = new JPanel();
            panelSpinners.setLayout(new FlowLayout());
            panelSpinners.add(getjlInicio());
            panelSpinners.add(getjInicio());
            panelSpinners.add(new JSeparator(SwingConstants.VERTICAL));
            panelSpinners.add(getjlFim());
            panelSpinners.add(getjFim());
        }
        return panelSpinners;
    }

    public JSpinner getjInicio() {
        if ( jInicio == null ) {
            jInicio = new JSpinnerTime();
        }
        return jInicio;
    }

    public JSpinner getjFim() {
        if ( jFim == null ) {
            jFim = new JSpinnerTime();
        }
        return jFim;
    }

    @Override
    protected void setMapLabel() {
        super.setMapLabel();
        getMapLabel().put("turno", getjlTurno());
        getMapLabel().put("fim", getjlFim());
        getMapLabel().put("turno", getjlTurno());
    }

}
