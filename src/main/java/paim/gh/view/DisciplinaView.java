package paim.gh.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import paim.wingchun.app.Reflections;
import paim.wingchun.app.WC_Strings;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.componentes.WC_Label;
import paim.wingchun.view.componentes.botoes.JButtonConstrutor;

import paim.gh.model.pojos.Disciplina;

/**
 * @author paim 05/05/2011
 */
public class DisciplinaView extends AbstractTabelaView<Disciplina> {

    private static final long serialVersionUID = 1L;

    private JPanel panel;

    private JPanel panelSpinners;

    private JSpinner nPeriodos;

    private WC_Label jlnPeriodos;

    private WC_Label jlSemestre;

    private JComboBox<Pojo> jSemestre;

    private JPanel panelSemestre;

    private JButton jBConstrutor;

    private JPanel panelCurso;

    private WC_Label jlCurso;

    private JTextField jCurso;

    public DisciplinaView() {
        super();
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        Disciplina pojo = (Disciplina) super.v_setObjeto(objeto);
        /* coloca o que esta no objeto */
        getnPeriodos().getModel().setValue(pojo.getNumPeriodos());
        getJSemestre().getModel().setSelectedItem(pojo.getSemestre());

        List<String> siglas = Reflections.getValorField(pojo.getCursos(), String.class, "sigla");
        getJCurso().setText(WC_Strings.formataLista(siglas));

        return pojo;
    }

    @Override
    protected JPanel getJPanel() {
        if ( panel == null ) {
            panel = super.getJPanel();
            GridBagConstraints constraints0 = new GridBagConstraints();
            constraints0.anchor = GridBagConstraints.NORTH;
            constraints0.gridx = 0;
            constraints0.gridy = GridBagConstraints.RELATIVE;
            constraints0.gridwidth = GridBagConstraints.REMAINDER;
            constraints0.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelSpinners(), constraints0);

            GridBagConstraints constraints1 = new GridBagConstraints();
            constraints1.fill = GridBagConstraints.HORIZONTAL;
            constraints1.anchor = GridBagConstraints.NORTH;
            constraints1.gridx = 0;
            constraints1.gridy = GridBagConstraints.RELATIVE;
            constraints1.gridwidth = GridBagConstraints.REMAINDER;
            constraints1.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelSemestre(), constraints1);

            GridBagConstraints constraints2 = new GridBagConstraints();
            constraints2.fill = GridBagConstraints.HORIZONTAL;
            constraints2.anchor = GridBagConstraints.NORTH;
            constraints2.gridx = 0;
            constraints2.gridy = GridBagConstraints.RELATIVE;
            constraints2.gridwidth = GridBagConstraints.REMAINDER;
            constraints2.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelCurso(), constraints2);

            // GridBagConstraints constraints3 = new GridBagConstraints();
            // constraints3.anchor = GridBagConstraints.EAST;
            // constraints3.gridx = 0;
            // constraints3.gridy = GridBagConstraints.RELATIVE;
            // constraints3.insets = new Insets(0, 0, 5, 0);
            // panel.add(getjlSemestre(), constraints3);
            //
            // GridBagConstraints constraints4 = new GridBagConstraints();
            // // constraints4.fill = GridBagConstraints.HORIZONTAL;
            // constraints4.anchor = GridBagConstraints.LINE_START;
            // constraints4.gridx = 1;
            // constraints4.gridy = GridBagConstraints.RELATIVE;
            // constraints4.insets = new Insets(0, 5, 5, 0);
            // panel.add(getJSemestre(), constraints4);
            //

        }
        return panel;
    }

    private JPanel getPanelSpinners() {
        if ( panelSpinners == null ) {
            panelSpinners = new JPanel();
            panelSpinners.setLayout(new FlowLayout());
            panelSpinners.add(getjlnPeriodos());
            panelSpinners.add(getnPeriodos());
        }
        return panelSpinners;
    }

    public JSpinner getnPeriodos() {
        if ( nPeriodos == null ) {
            SpinnerModel model = new SpinnerNumberModel(1, 1, 40, 1);
            nPeriodos = new JSpinner(model);
            // FIXME spinner.setToolTipText("Informar quantos nï¿½veis ???????");
        }
        return nPeriodos;
    }

    private WC_Label getjlnPeriodos() {
        if ( jlnPeriodos == null ) {
            jlnPeriodos = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "numPeriodos");
            jlnPeriodos.setText(rotulo);
        }
        return jlnPeriodos;
    }

    private JPanel getPanelSemestre() {
        if ( panelSemestre == null ) {
            panelSemestre = new JPanel();
            panelSemestre.setLayout(new FlowLayout());
            panelSemestre.add(getjlSemestre());
            panelSemestre.add(getJSemestre());
        }
        return panelSemestre;
    }

    private WC_Label getjlSemestre() {
        if ( jlSemestre == null ) {
            jlSemestre = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "semestre");
            jlSemestre.setText(rotulo);
        }
        return jlSemestre;
    }

    public JComboBox<Pojo> getJSemestre() {
        if ( jSemestre == null ) {
            jSemestre = new JComboBox<Pojo>();
            jSemestre.setEnabled(true);
            Dimension d = new Dimension(100, 20);
            jSemestre.setMinimumSize(d);
            jSemestre.setMaximumSize(d);
            jSemestre.setPreferredSize(d);

        }
        return jSemestre;
    }

    public JButton getJBConstrutor() {
        if ( jBConstrutor == null ) {
            jBConstrutor = new JButtonConstrutor();
        }
        return jBConstrutor;
    }

    private JPanel getPanelCurso() {
        if ( panelCurso == null ) {
            panelCurso = new JPanel();
            panelCurso.setLayout(new FlowLayout());
            panelCurso.add(getjlCurso());
            panelCurso.add(getJCurso());
            panelCurso.add(getJBConstrutor());
            // panelCurso.setBorder(BorderFactory.createEtchedBorder());
        }
        return panelCurso;
    }

    private WC_Label getjlCurso() {
        if ( jlCurso == null ) {
            jlCurso = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "curso");
            jlCurso.setText(rotulo);
        }
        return jlCurso;
    }

    public JTextField getJCurso() {
        if ( jCurso == null ) {
            jCurso = new JTextField();
            jCurso.setEnabled(false);
            Dimension d = new Dimension(100, 20);
            jCurso.setMinimumSize(d);
            jCurso.setMaximumSize(d);
            jCurso.setPreferredSize(d);

        }
        return jCurso;
    }

    // public JTextField getJSemestre() {
    // if ( jSemestre == null ) {
    // jSemestre = new JTextField();
    // jSemestre.setEnabled(false);
    // Dimension d = new Dimension(100, 20);
    // jSemestre.setMinimumSize(d);
    // jSemestre.setMaximumSize(d);
    // jSemestre.setPreferredSize(d);
    //
    // }
    // return jSemestre;
    // }

    @Override
    protected void setMapLabel() {
        super.setMapLabel();
        getMapLabel().put("numPeriodos", getjlnPeriodos());
        getMapLabel().put("semestre", getjlSemestre());
        getMapLabel().put("curso", getjlCurso());

    }

}
