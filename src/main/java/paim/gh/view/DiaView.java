package paim.gh.view;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import paim.wingchun.app.Reflections;
import paim.wingchun.view.componentes.WC_Label;

import paim.gh.model.DiasSemana;
import paim.gh.model.pojos.Dia;

/**
 * @author paim 16/05/2011
 */
public class DiaView extends AbstractTabelaView<Dia> {

    private static final long serialVersionUID = 1L;

    private JPanel panel;

    private JPanel panelLista;

    private ButtonGroup buttonGroup;

    private List<JRadioButton> listaRadio;

    private WC_Label jlDias;

    public DiaView() {
        super();
        getJNome().setEnabled(false);
        getJSigla().setEnabled(false);
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        Dia pojo = (Dia) super.v_setObjeto(objeto);

        DiasSemana diasSemana = pojo.getDiaSemana();

        /* se objeto novo provavelmente, entao nao seleciona nada */
        if ( diasSemana == null ) {
            getButtonGroup().clearSelection();
            return pojo;
        }

        /* encontra na lista por nome do radio, e seleciona-o */
        for ( JRadioButton radioButton : getListaRadioButtons() ) {
            if ( radioButton.getName().equals(diasSemana.name()) ) {
                radioButton.setSelected(true);
                break;
            }
        }

        return pojo;
    }

    @Override
    protected JPanel getJPanel() {
        if ( panel == null ) {
            panel = super.getJPanel();
            GridBagConstraints constraints0 = new GridBagConstraints();
            constraints0.anchor = GridBagConstraints.NORTH;
            constraints0.gridx = 0;
            constraints0.gridy = GridBagConstraints.RELATIVE;
            constraints0.gridwidth = GridBagConstraints.REMAINDER;
            constraints0.insets = new Insets(0, 5, 0, 0);
            panel.add(getPanelLista(), constraints0);
        }
        return panel;
    }

    public ButtonGroup getButtonGroup() {
        if ( buttonGroup == null ) {
            buttonGroup = new ButtonGroup();
        }
        return buttonGroup;
    }

    private JPanel getPanelLista() {
        if ( panelLista == null ) {
            panelLista = new JPanel();
            panelLista.setLayout(new FlowLayout());
            panelLista.add(getjlDias());
            /* adicionando os radioButtons */
            for ( JRadioButton rButton : getListaRadioButtons() ) {
                getButtonGroup().add(rButton);
                panelLista.add(rButton);
            }
        }
        return panelLista;
    }

    public final List<JRadioButton> getListaRadioButtons() {

        if ( listaRadio == null ) {
            listaRadio = new ArrayList<JRadioButton>();
            for ( DiasSemana diaSemana : DiasSemana.values() ) {
                JRadioButton rButton = new JRadioButton(diaSemana.getSigla());
                /* apenas para saber qual botao depois, prefiro por enum */
                rButton.setActionCommand(diaSemana.name());
                rButton.setName(diaSemana.name());
                listaRadio.add(rButton);
            }
        }
        return listaRadio;
    }

    private WC_Label getjlDias() {
        if ( jlDias == null ) {
            jlDias = new WC_Label();
            String rotulo = Reflections.getRotulo(getpClass(), "diaSemana");
            jlDias.setText(rotulo);
        }
        return jlDias;
    }

    @Override
    protected void setMapLabel() {
        super.setMapLabel();
        getMapLabel().put("diaSemana", getjlDias());
    }

}
