/**
 * @author temujin Jul 15, 2012
 */
package paim.gh.model;

import com.google.common.base.Function;

import paim.gh.model.pojos.Turma;

/**
 * @author temujin Jul 15, 2012
 */
public class Legendas implements Function<Turma, Character> {

    @Override
    public Character apply(Turma turma) {
        return turma.getLegenda();
    }
}
