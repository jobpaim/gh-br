package paim.gh.model;


@Deprecated
public enum Entidades {
    NENHUM("", ""),
    GRADE("Grade", "Grades"),
    PREFERENCIA("Preferencia", "Preferencias"),
    AULA("Turma", "Aulas"),
    CURSO("Curso", "Cursos"),
    DIASEMANA("Dia da Semana", "Dias da Semana"),
    DISCIPLINA("Disciplina", "Disciplinas"),
    PERIODO("Período", "Períodos"),
    PROFESSOR("Professor", "Professores"),
    DOSCENTE("Doscente", "Doscentes"),
    SALA("Sala", "Salas"),
    SEMESTRE("Semestre", "Semestres"),
    TURNO("Turno", "Turnos"),
    BALDE("Balde", "Balde");

    private final String rotulo;

    private final String rotuloS;

    private Entidades(String rotulo, String rotuloS) {
        this.rotulo = rotulo;
        this.rotuloS = rotuloS;
    }

    public String getRotulo() {
        return rotulo;
    }

    public String getRotuloS() {
        return rotuloS;
    }

}
