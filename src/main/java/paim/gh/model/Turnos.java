package paim.gh.model;


public enum Turnos {
    MANHA("Manhã", "M"), TARDE("Tarde", "T"), NOITE("Noite", "N"), OUTRO("Outro", "O");

    private String nome;

    private String sigla;

    private Turnos(String nome, String sigla) {
        this.nome = nome;
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public String getSigla() {
        return sigla;
    }

}
