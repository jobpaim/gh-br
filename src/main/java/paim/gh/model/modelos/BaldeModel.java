package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Balde;

public class BaldeModel extends Model<Balde> {

    private static BaldeModel modelo;

    private BaldeModel() {}

    public static BaldeModel getInstance() {
        if ( modelo == null )
            modelo = new BaldeModel();

        return modelo;
    }
}
