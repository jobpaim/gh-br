package paim.gh.model.modelos;


import java.util.List;

import org.hibernate.Session;

import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.Gradeavel;
import paim.gh.model.pojos.*;

/**
 * @author paim  25/09/2013
 * @since
 * @param <P>
 */
@Deprecated
public interface ContratoGradeavel<P extends Pojo & Gradeavel> {

    public abstract List<P> list(Session session, Grade grade);

    // ///////////////////////////////////////////////////////////////////////////////////
    public abstract List<P> list_by_Professor(Session session, Grade grade, Professor pojo);

    public abstract List<P> list_by_Professores(Session session, Grade grade, List<Professor> pojos);

    public abstract List<P> list_by_Periodo(Session session, Grade grade, Periodo pojo);

    public abstract List<P> list_by_Periodos(Session session, Grade grade, List<Periodo> pojos);

    public abstract List<P> list_by_Aula(Session session, Grade grade, Aula pojo);

    public abstract List<P> list_by_Aulas(Session session, Grade grade, List<Aula> pojos);

    public abstract List<P> list_by_Turma(Session session, Grade grade, Turma pojo);

    public abstract List<P> list_by_Turmas(Session session, Grade grade, List<Turma> pojos);

    public abstract List<P> list_by_Disciplina(Session session, Grade grade, Disciplina pojo);

    public abstract List<P> list_by_Disciplinas(Session session, Grade grade, List<Disciplina> pojos);

    public abstract List<P> list_by_Doscente(Session session, Grade grade, Doscente pojo);

    public abstract List<P> list_by_Doscentes(Session session, Grade grade, List<Doscente> pojos);

    public abstract List<P> list_by_Curso(Session session, Grade grade, Curso pojo);

    public abstract List<P> list_by_Cursos(Session session, Grade grade, List<Curso> pojos);

    public abstract List<P> list_by_Dia(Session session, Grade grade, Dia pojo);

    public abstract List<P> list_by_Dias(Session session, Grade grade, List<Dia> pojos);

    public abstract List<P> list_by_Horario(Session session, Grade grade, Horario pojo);

    public abstract List<P> list_by_Horarios(Session session, Grade grade, List<Horario> pojos);

    public abstract List<P> list_by_Sala(Session session, Grade grade, Sala pojo);

    public abstract List<P> list_by_Salas(Session session, Grade grade, List<Sala> pojos);

    public abstract List<P> list_by_Semestre(Session session, Grade grade, Semestre pojo);

    public abstract List<P> list_by_Semestres(Session session, Grade grade, List<Semestre> pojos);

    public abstract List<P> list_by_Turno(Session session, Grade grade, Turno pojo);

    public abstract List<P> list_by_Turnos(Session session, Grade grade, List<Turno> pojos);

    public abstract List<Periodo> getPeriodos(Session session, P pojo);

    public abstract List<Periodo> getPeriodos(Session session, List<P> pojos);

    public abstract List<Aula> getAulas(Session session, P pojo);

    public abstract List<Aula> getAulas(Session session, List<P> pojos);

    public abstract List<Professor> getProfessores(Session session, P pojo);

    public abstract List<Professor> getProfessores(Session session, List<P> pojos);

    public abstract List<Turma> getTurmas(Session session, P pojo);

    public abstract List<Turma> getTurmas(Session session, List<P> pojos);

    public abstract List<Disciplina> getDisciplinas(Session session, P pojo);

    public abstract List<Disciplina> getDisciplinas(Session session, List<P> pojos);

    public abstract List<Doscente> getDoscentes(Session session, P pojo);

    public abstract List<Doscente> getDoscentes(Session session, List<P> pojos);

    public abstract List<Curso> getCursos(Session session, P pojo);

    public abstract List<Curso> getCursos(Session session, List<P> pojos);

    public abstract List<Dia> getDias(Session session, P pojo);

    public abstract List<Dia> getDias(Session session, List<P> pojos);

    public abstract List<Horario> getHorarios(Session session, P pojo);

    public abstract List<Horario> getHorarios(Session session, List<P> pojos);

    public abstract List<Sala> getSalas(Session session, P pojo);

    public abstract List<Sala> getSalas(Session session, List<P> pojos);

    public abstract List<Semestre> getSemestres(Session session, P pojo);

    public abstract List<Semestre> getSemestres(Session session, List<P> pojos);

    public abstract List<Turno> getTurnos(Session session, P pojo);

    public abstract List<Turno> getTurnos(Session session, List<P> pojos);

}
