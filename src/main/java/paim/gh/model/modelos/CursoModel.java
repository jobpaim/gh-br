package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Curso;

public class CursoModel extends Model<Curso> {

    private static CursoModel modelo;

    private CursoModel() {}

    public static CursoModel getInstance() {
        if ( modelo == null )
            modelo = new CursoModel();

        return modelo;
    }
}
