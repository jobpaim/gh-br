/**
 * @author paim 14/11/2011
 */
package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Periodo;

/**
 * @author paim 14/11/2011
 */
public class PeriodoModel extends Model<Periodo> {

    private static PeriodoModel modelo;

    private PeriodoModel() {}

    public static PeriodoModel getInstance() {
        if ( modelo == null )
            modelo = new PeriodoModel();

        return modelo;
    }

}
