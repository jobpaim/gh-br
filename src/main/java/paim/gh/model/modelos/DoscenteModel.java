package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Doscente;

public class DoscenteModel extends Model<Doscente> {

    private static DoscenteModel modelo;

    private DoscenteModel() {}

    public static DoscenteModel getInstance() {
        if ( modelo == null )
            modelo = new DoscenteModel();

        return modelo;
    }
}
