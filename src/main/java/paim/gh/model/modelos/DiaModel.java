package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Dia;

public class DiaModel extends Model<Dia> {

    private static DiaModel modelo;

    private DiaModel() {}

    public static DiaModel getInstance() {
        if ( modelo == null )
            modelo = new DiaModel();

        return modelo;
    }

}
