package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Sala;

public class SalaModel extends Model<Sala> {

    private static SalaModel modelo;

    private SalaModel() {}

    public static SalaModel getInstance() {
        if ( modelo == null )
            modelo = new SalaModel();

        return modelo;
    }
}
