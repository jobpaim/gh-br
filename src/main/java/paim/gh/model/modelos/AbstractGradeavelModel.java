package paim.gh.model.modelos;


import java.util.List;

import org.hibernate.Session;

import paim.wingchun.model.modelos.Model;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.Gradeavel;
import paim.gh.model.pojos.*;

/** @author paim 24/09/2013
 * @since */
@SuppressWarnings({ "deprecation", "unchecked" })
@Deprecated
public abstract class AbstractGradeavelModel<P extends Pojo & Gradeavel> extends Model<P> implements
                ContratoGradeavel<P> {

    @Override
    public List<P> list(Session session, Grade grade) {
        return (List<P>) ((ContratoGradeavel<P>) getDAO()).list(session, grade);
    }

    @Override
    public List<P> list_by_Professor(Session session, Grade grade, Professor pojo) {
        return (List<P>) ((ContratoGradeavel<P>) getDAO()).list_by_Professor(session, grade, pojo);
    }

    @Override
    public List<P> list_by_Professores(Session session, Grade grade, List<Professor> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Periodo(Session session, Grade grade, Periodo pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Periodos(Session session, Grade grade, List<Periodo> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Aula(Session session, Grade grade, Aula pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Aulas(Session session, Grade grade, List<Aula> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Turma(Session session, Grade grade, Turma pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Turmas(Session session, Grade grade, List<Turma> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Disciplina(Session session, Grade grade, Disciplina pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Disciplinas(Session session, Grade grade, List<Disciplina> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Doscente(Session session, Grade grade, Doscente pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Doscentes(Session session, Grade grade, List<Doscente> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Curso(Session session, Grade grade, Curso pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Cursos(Session session, Grade grade, List<Curso> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Dia(Session session, Grade grade, Dia pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Dias(Session session, Grade grade, List<Dia> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Horario(Session session, Grade grade, Horario pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Horarios(Session session, Grade grade, List<Horario> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Sala(Session session, Grade grade, Sala pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Salas(Session session, Grade grade, List<Sala> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Semestre(Session session, Grade grade, Semestre pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Semestres(Session session, Grade grade, List<Semestre> pojos) {
        return null;
    }

    @Override
    public List<P> list_by_Turno(Session session, Grade grade, Turno pojo) {
        return null;
    }

    @Override
    public List<P> list_by_Turnos(Session session, Grade grade, List<Turno> pojos) {
        return null;
    }

    @Override
    public List<Periodo> getPeriodos(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Periodo> getPeriodos(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Aula> getAulas(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Aula> getAulas(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Professor> getProfessores(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Professor> getProfessores(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Turma> getTurmas(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Turma> getTurmas(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Disciplina> getDisciplinas(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Disciplina> getDisciplinas(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Doscente> getDoscentes(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Doscente> getDoscentes(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Curso> getCursos(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Curso> getCursos(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Dia> getDias(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Dia> getDias(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Horario> getHorarios(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Horario> getHorarios(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Sala> getSalas(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Sala> getSalas(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Semestre> getSemestres(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Semestre> getSemestres(Session session, List<P> pojos) {
        return null;
    }

    @Override
    public List<Turno> getTurnos(Session session, P pojo) {
        return null;
    }

    @Override
    public List<Turno> getTurnos(Session session, List<P> pojos) {
        return null;
    }
}
