package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Grade;

public class GradeModel extends Model<Grade> {

    private static GradeModel modelo;

    private GradeModel() {}

    public static GradeModel getInstance() {
        if ( modelo == null )
            modelo = new GradeModel();

        return modelo;
    }
    
    
}
