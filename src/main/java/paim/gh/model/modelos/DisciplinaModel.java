package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Disciplina;

public class DisciplinaModel extends Model<Disciplina> {

    private static DisciplinaModel modelo;

    private DisciplinaModel() {}

    public static DisciplinaModel getInstance() {
        if ( modelo == null )
            modelo = new DisciplinaModel();

        return modelo;
    }
}
