package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Turno;

public class TurnoModel extends Model<Turno> {

    private static TurnoModel modelo;

    private TurnoModel() {}

    public static TurnoModel getInstance() {
        if ( modelo == null )
            modelo = new TurnoModel();

        return modelo;
    }
}
