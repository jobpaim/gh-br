package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Horario;

public class HorarioModel extends Model<Horario> {

    private static HorarioModel modelo;

    private HorarioModel() {}

    public static HorarioModel getInstance() {
        if ( modelo == null )
            modelo = new HorarioModel();

        return modelo;
    }

}
