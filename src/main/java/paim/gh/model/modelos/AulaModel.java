/** @author paim 14/11/2011 */
package paim.gh.model.modelos;


import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Aula;

/** @author paim 14/11/2011 */
public class AulaModel extends Model<Aula> {

    private static AulaModel modelo;

    private AulaModel() {}

    public static synchronized AulaModel getInstance() {
        if ( modelo == null )
            modelo = new AulaModel();

        return modelo;
    }

}
