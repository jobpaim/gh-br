package paim.gh.model.modelos;

import java.util.List;

import org.hibernate.Session;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.modelos.Model;
import paim.wingchun.model.modelos.ErroModel.Level;
import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.DAO.SemestreDAO;
import paim.gh.model.pojos.Semestre;

public class SemestreModel extends Model<Semestre> {

    private static SemestreModel modelo;

    private SemestreModel() {}

    public static SemestreModel getInstance() {
        if ( modelo == null )
            modelo = new SemestreModel();

        return modelo;
    }

    @Override
    protected List<Erro> aposValidar(Session session, Pojo pojo, List<Erro> erros) throws Exception {

        Semestre objeto = (Semestre) pojo;

        String rotuloClasse = Reflections.getRotulo(Semestre.class);
        String rotuloAtributo = Reflections.getRotulo(Semestre.class, "numSemestre");

        if ( SemestreDAO.getInstance().semestreDuplicado(session, objeto) ) {
            Erro erro = new Erro(2L, Level.ERRO$IMPEDITIVO, pojo, "numSemestre", new String[] {
                    rotuloClasse,
                    rotuloAtributo,
                    objeto.getNumSemestre().toString() });
            erros.add(erro);
        }

        return erros;
    }

}
