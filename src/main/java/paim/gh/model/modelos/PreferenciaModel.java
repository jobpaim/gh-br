package paim.gh.model.modelos;

import paim.wingchun.model.modelos.Model;

import paim.gh.model.pojos.Preferencia;

public class PreferenciaModel extends Model<Preferencia> {

    private static PreferenciaModel modelo;

    private PreferenciaModel() {}

    public static PreferenciaModel getInstance() {
        if ( modelo == null )
            modelo = new PreferenciaModel();

        return modelo;
    }


    //    /**
    //     * busca as preferencias que estao ligadas a esse pojo
    //     *
    //     * @author paim 09/12/2011
    //     * @param session
    //     * @param pojo
    //     * @return List<Preferencias>
    //     */
    //    public List<Preferencia> getPreferencias(Session session, POJO pojo) {
    //        return PreferenciaDAO.get().getPreferencias(session, pojo);
    //    }

}
