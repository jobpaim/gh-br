package paim.gh.model.modelos;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.modelos.Model;
import paim.wingchun.model.modelos.ErroModel.Level;
import paim.wingchun.model.modelos.ErroModel.Tipo;
import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.pojos.Periodo;
import paim.gh.model.pojos.Preferencia;
import paim.gh.model.pojos.Professor;

public class ProfessorModel extends Model<Professor> {

    private static ProfessorModel modelo;

    private ProfessorModel() {}

    public static ProfessorModel getInstance() {
        if ( modelo == null )
            modelo = new ProfessorModel();

        return modelo;
    }

    @Override
    public boolean disengage(Session session, Pojo pojo, List<Erro> engates) throws Exception {
        List<Pojo> pojos = new ArrayList<>();

        for ( Erro e : engates ) {
            if ( e.getPojo() instanceof Preferencia ) {
                /* ajustando cada objeto preferencia para nao mais usar esse professor */
                ((Preferencia) e.getPojo()).setProfessor(null);
                pojos.add(e.getPojo());
            }
            else if ( e.getPojo() instanceof Periodo ) {
                ((Periodo) e.getPojo()).setProfessor(null);
                pojos.add(e.getPojo());
            }
        }

        /* persistir a lista de pojos alterados */
        if ( !engates.isEmpty() )
            Model.getInstance().saveOrUpdate(session, pojos);

        return true;
    }

    @Override
    public List<Erro> couplings(Session session, Pojo pojo) throws Exception {
        List<Erro> engates = new ArrayList<>();
        String fieldName = Reflections.getFieldName(Professor.class);

        /* primeiro procura em preferencias */
        List<Preferencia> listP = PreferenciaModel.getInstance().getObjetos(session, pojo);

        for ( Preferencia p : listP ) {
            Erro engate = new Erro(3L, Level.ERRO$IMPEDITIVO, Tipo.EXCLUSAO, p, fieldName, new String[] {
                            pojo.toString(),
                            p.toString() });
            engates.add(engate);
        }

        List<Periodo> listAS = PeriodoModel.getInstance().getObjetos(session, pojo);
        for ( Periodo as : listAS ) {

            Erro engate = new Erro(3L, Level.ERRO$IMPEDITIVO, Tipo.EXCLUSAO, as, fieldName, new String[] {
                            pojo.toString(),
                            as.toString() });
            engates.add(engate);
        }

        return engates;
    }

}
