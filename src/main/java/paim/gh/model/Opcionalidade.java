package paim.gh.model;


/** <b>INEVITAVEL</b> Que nao se pode evitar; fatal. nao me venha com chorumelas <i>(tem que ser)</i><BR>
 * <b>PREFERIVEL</b> tenta conseguir <i>(prefere que seja)</i><BR>
 * <b>POSSIVEL</b> se vier nao tem problema <i>(pode ser)</i><BR>
 * <b>EVITAVEL</b> tenta evitar <i>(prefere que nao seja)</i><BR>
 * <b>IMPOSSIVEL</b> nem a pau juvenal <i>(nao pode ser)</i><BR> */
public enum Opcionalidade {
    INEVITAVEL, PREFERIVEL, POSSIVEL, EVITAVEL, IMPOSSIVEL,
}
