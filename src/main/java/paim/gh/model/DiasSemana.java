package paim.gh.model;


import java.text.DateFormatSymbols;

public enum DiasSemana {
    DOMINGO, SEGUNDA, TERCA, QUARTA, QUINTA, SEXTA, SABADO;

    public String getNome() {
        return DateFormatSymbols.getInstance().getWeekdays()[ordinal() + 1];
    }

    public String getSigla() {
        return DateFormatSymbols.getInstance().getShortWeekdays()[ordinal() + 1];
    }
}
