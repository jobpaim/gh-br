package paim.gh.model;


/** @author paim 18/09/2013
 * @since */
public class GradeavelException extends RuntimeException {

    public GradeavelException() {}

    public GradeavelException(String message) {
        super(message);
    }

    public GradeavelException(Throwable cause) {
        super(cause);
    }

    public GradeavelException(String message, Throwable cause) {
        super(message, cause);
    }

    public GradeavelException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
