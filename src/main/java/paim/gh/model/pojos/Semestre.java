package paim.gh.model.pojos;


import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel.Level;


@AType(rotulo = "Semestre", rotuloS = "Semestres")
@Entity
public class Semestre extends AbstractTabela {

    private static final long serialVersionUID = 1L;

    @AField(rotulo = "Nº do Semestre")
    @Validacao(nulo = Level.ERRO$IMPEDITIVO/* FIXME , negativo = Level.ERRO$IMPEDITIVO, zero = Level.ERRO$IMPEDITIVO */)
    @Column(nullable = false)
    private Integer numSemestre = 0;

    public Integer getNumSemestre() {
        return this.numSemestre;
    }

    public void setNumSemestre(Integer numSemestre) {
        this.numSemestre = numSemestre;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Semestre other = (Semestre) obj;

        if ( !Objects.equals(this.numSemestre, other.numSemestre) )
            return false;

        return true;
    }
}
