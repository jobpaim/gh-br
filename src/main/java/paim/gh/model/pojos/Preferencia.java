package paim.gh.model.pojos;


import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Replicable;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.Opcionalidade;

@AType(rotulo = "Preferência", rotuloS = "Preferências")
@Entity
public class Preferencia extends Pojo implements Replicable {

    private static final long serialVersionUID = 1L;

    @AField(rotulo = "opcionalidade")
    @Column(nullable = false)
    private Opcionalidade opcionalidade;

    /** preferencia de turma */
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    private Professor professor;

    /** preferencia de professor */
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    private Turma turma;

    /** preferencia de professor */
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    private Curso curso;

    /** preferencia de professor ou turma */
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    private Dia dia;

    /** preferencia de professor ou turma */
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    private Horario horario;

    /** preferencia de professor ou turma */
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    private Sala sala;

    /** preferencia de professor */
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    private Semestre semestre;

    /** preferencia de professor ou turma */
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    private Turno turno;

    public Opcionalidade getOpcionalidade() {
        return this.opcionalidade;
    }

    public void setOpcionalidade(Opcionalidade opcionalidade) {
        this.opcionalidade = opcionalidade;
    }

    public Curso getCurso() {
        return this.curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Dia getDiaSemana() {
        return this.dia;
    }

    public void setDiaSemana(Dia dia) {
        this.dia = dia;
    }

    public Semestre getSemestre() {
        return this.semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public Turno getTurno() {
        return this.turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }

    public Horario getHorario() {
        return this.horario;
    }

    public Sala getSala() {
        return this.sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Professor getProfessor() {
        return this.professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Turma getTurma() {
        return this.turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    @Override
    public Preferencia replica() {
        Preferencia replica = new Preferencia();
        // TODO Auto-generated method stub
        return replica;
    }

    @Override
    public String toString() {
        String s = (turma != null ? turma.getSigla() : professor.getSigla());
        return s;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Preferencia other = (Preferencia) obj;

        if ( !Objects.equals(this.opcionalidade, other.opcionalidade) )
            return false;
        if ( !equals(this.professor, other.professor) )
            return false;
        if ( !equals(this.turma, other.turma) )
            return false;
        if ( !equals(this.curso, other.curso) )
            return false;
        if ( !equals(this.dia, other.dia) )
            return false;
        if ( !equals(this.horario, other.horario) )
            return false;
        if ( !equals(this.sala, other.sala) )
            return false;
        if ( !equals(this.semestre, other.semestre) )
            return false;
        if ( !equals(this.turno, other.turno) )
            return false;

        return true;
    }
}
