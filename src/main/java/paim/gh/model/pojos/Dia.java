package paim.gh.model.pojos;



import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel.Level;

import paim.gh.model.DiasSemana;

@AType(rotulo = "Dia da semana", rotuloS = "Dias da semana")
@Entity
public class Dia extends AbstractTabela {

    private static final long serialVersionUID = 1L;

    @AField(rotulo = "Dias", isHidden = true)
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private DiasSemana diaSemana;

    public void setDiaSemana(DiasSemana diaSemana) {
        this.diaSemana = diaSemana;
    }

    public DiasSemana getDiaSemana() {
        return diaSemana;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Dia other = (Dia) obj;

        if ( !Objects.equals(this.diaSemana, other.diaSemana) )
            return false;

        return true;
    }

}
