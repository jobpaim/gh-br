package paim.gh.model.pojos;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Replicable;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel;
import paim.wingchun.model.modelos.ErroModel.Level;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.Gradeavel;

/**
 * classe que representara uma turma uma aula eh um mestre de subAulas....
 *
 * @author temujin Nov 10, 2011
 */
@AType(rotulo = "Turma", rotuloS = "Turmas")
@Entity
public class Turma extends Pojo implements Replicable, Gradeavel {

    private static final long serialVersionUID = 1L;

    public Turma() {}

    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Cascade({  CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    private Aula aula;

    // PAIM OK nao mexer mais no mamepamento, falta verificar cascade
    /* @OrderBy("opcionalidade") */
    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({  CascadeType.SAVE_UPDATE,CascadeType.DELETE, CascadeType.DELETE_ORPHAN })
    @ManyToMany(targetEntity = Preferencia.class, fetch = FetchType.LAZY)
    @JoinTable()
    private List<Preferencia> preferencias = new ArrayList<>();

    @Validacao(vazio = ErroModel.Level.NENHUM)
    @Cascade({  CascadeType.SAVE_UPDATE,CascadeType.DELETE, CascadeType.DELETE_ORPHAN })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable()
    private List<Periodo> periodos = new ArrayList<>();

    @AField(rotulo = "Turma")
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false/*, length = 1*/)
    private Character legenda;

    @AField(rotulo = "Sigla")
    private transient String sigla;

    @Transient
    public String getSigla() {
        this.sigla = getAula().getSigla() + " (" + getLegenda() + ")";
        return sigla;
    }

    public List<Preferencia> getPreferencias() {
        return this.preferencias;
    }

    public void setPreferencias(List<Preferencia> preferencias) {
        this.preferencias = preferencias;
    }

    public Aula getAula() {
        return this.aula;
    }

    public void setAula(Aula aula) {
        this.aula = aula;
    }

    public List<Periodo> getPeriodos() {
        return this.periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }

    public Character getLegenda() {
        return this.legenda;
    }

    public void setLegenda(Character legenda) {
        this.legenda = legenda;
    }

    @Override
    public Turma replica() {
        Turma replica = new Turma();
        replica.aula = this.getAula();

        // TODO Auto-generated method stub
        return replica;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Turma other = (Turma) obj;

        if ( !equals(this.aula, other.aula) )
            return false;
        if ( !Objects.equals(this.legenda, other.legenda) )
            return false;

        return true;
    }

    @Override
    public String toString() {
        return this.getSigla();
    }
}
