package paim.gh.model.pojos;


// import java.sql.Date;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel.Level;

@AType(rotulo = "Horário", rotuloS = "Horários")
@Entity
public class Horario extends AbstractTabela {

    private static final long serialVersionUID = 1L;

    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(nullable = false)
    private Turno turno;

    /** Inicializado com new GregorianCalendar(0, 0, 0, 0, 0, 0) */
    @AField(rotulo = "Inicio", isHidden = true)
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private Date inicio = new GregorianCalendar(0, 0, 0, 0, 0, 0).getTime();

    /** Inicializado com new GregorianCalendar(0, 0, 0, 0, 0, 0) */
    @AField(rotulo = "Fim", isHidden = true)
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private Date fim = new GregorianCalendar(0, 0, 0, 0, 0, 0).getTime();

    public Turno getTurno() {
        return this.turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public Date getInicio() {
        return this.inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return this.fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Horario other = (Horario) obj;

        if ( !Objects.equals(this.inicio, other.inicio) )
            return false;
        if ( !Objects.equals(this.fim, other.fim) )
            return false;
        if ( !equals(this.turno, other.turno) )
            return false;

        return true;
    }

}
