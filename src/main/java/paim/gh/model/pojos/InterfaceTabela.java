package paim.gh.model.pojos;


/**
 * @author paim 06/06/2013
 * @since
 */
public interface InterfaceTabela {


    public String getNome();

    public String getSigla();

    public String getDescricao();

}
