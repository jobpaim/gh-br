/** @author paim 07/12/2011 */
package paim.gh.model.pojos;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Replicable;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.Gradeavel;

/** @author paim 07/12/2011 */
@AType(rotulo = "Aula", rotuloS = "Aulas")
@Entity
public class Aula extends Pojo implements Replicable, InterfaceTabela, Gradeavel {

    private static final long serialVersionUID = 1L;

    public Aula() {}

    public Aula(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public Aula(Grade grade, Disciplina disciplina) {
        this.disciplina = disciplina;
        this.grade = grade;
    }

    @Validacao(nulo = ErroModel.Level.ERRO$IMPEDITIVO)
    @Cascade({
                    CascadeType.ALL,
                    CascadeType.REFRESH,
                    CascadeType.SAVE_UPDATE,
                    CascadeType.DELETE,
                    CascadeType.DELETE_ORPHAN })
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable
    private List<Turma> turmas = new ArrayList<>();

    @Validacao(nulo = ErroModel.Level.ERRO$IMPEDITIVO)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    private Disciplina disciplina;

    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    private Grade grade;

    @AField(rotulo = "Descrição")
    private transient String descricao;

    @AField(rotulo = "Nome")
    private transient String nome;

    @AField(rotulo = "Sigla")
    private transient String sigla;

    @Override
    @Transient
    public String getDescricao() {
        descricao = getDisciplina().getDescricao();
        return descricao;
    }

    @Override
    @Transient
    public String getNome() {
        nome = getDisciplina().getNome();
        return nome;
    }

    @Override
    @Transient
    public String getSigla() {
        sigla = getDisciplina().getSigla();
        return sigla;
    }

    public Disciplina getDisciplina() {
        return this.disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public Grade getGrade() {
        return this.grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public List<Turma> getTurmas() {
        return this.turmas;
    }

    public void setTurmas(List<Turma> turmas) {
        this.turmas = turmas;
    }

    public boolean isTurmaUnica() {
        return getTurmas().size() == 1;
    }

    public Turma primeiraTurma() {
        return getTurmas().get(0);
    }

    @Override
    public Aula replica() {
        /* deve replicar quase tudo menos a grade */
        Aula replica = new Aula();
        // TODO Auto-generated method stub
        return replica;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Aula other = (Aula) obj;

        if ( !equals(this.grade, other.grade) )
            return false;
        if ( !equals(this.disciplina, other.disciplina) )
            return false;

        return true;
    }

}
