package paim.gh.model.pojos;


import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel.Level;

import paim.gh.model.Turnos;


@AType(rotulo = "Turno", rotuloS = "Turnos")
@Entity
public class Turno extends AbstractTabela {

    private static final long serialVersionUID = 1L;

    @AField(rotulo = "Turno", isHidden = true)
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private Turnos turno;

    /** Inicializado com new GregorianCalendar(0, 0, 0, 0, 0, 0) */
    @AField(rotulo = "Inicio", isHidden = true)
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private Date inicio = new GregorianCalendar(0, 0, 0, 0, 0, 0).getTime();

    /** Inicializado com new GregorianCalendar(0, 0, 0, 0, 0, 0) */
    @AField(rotulo = "Fim", isHidden = true)
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private Date fim = new GregorianCalendar(0, 0, 0, 0, 0, 0).getTime();

    public void setTurno(Turnos turno) {
        this.turno = turno;
    }

    public Turnos getTurno() {
        return turno;
    }

    public Date getInicio() {
        return this.inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return this.fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Turno other = (Turno) obj;

        if ( !Objects.equals(this.turno, other.turno) )
            return false;
        if ( !Objects.equals(this.inicio, other.inicio) )
            return false;
        if ( !Objects.equals(this.fim, other.fim) )
            return false;

        return true;
    }

}
