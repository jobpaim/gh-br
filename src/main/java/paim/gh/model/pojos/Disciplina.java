package paim.gh.model.pojos;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel.Level;

@AType(rotulo = "Disciplina", rotuloS = "Disciplinas")
@Entity
public class Disciplina extends AbstractTabela {

    private static final long serialVersionUID = 1L;

    @AField(rotulo = "Nº de Períodos")
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private Integer numPeriodos = new Integer(1);

    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(nullable = false)
    private Semestre semestre;

    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @ManyToMany(targetEntity = Curso.class, fetch = FetchType.LAZY)
    // @OneToMany(fetch = FetchType.LAZY)
    @JoinTable()
    private List<Curso> cursos = new ArrayList<>();

    public Semestre getSemestre() {
        return this.semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public void setNumPeriodos(Integer numPeriodos) {
        this.numPeriodos = numPeriodos;
    }

    public Integer getNumPeriodos() {
        return numPeriodos;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Disciplina other = (Disciplina) obj;

        if ( !Objects.equals(this.numPeriodos, other.numPeriodos) )
            return false;
        if ( !equals(this.semestre, other.semestre) )
            return false;

        return true;
    }

}
