package paim.gh.model.pojos;


import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import paim.wingchun.model.AField;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel.Level;
import paim.wingchun.model.pojos.Pojo;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractTabela extends Pojo implements InterfaceTabela {

    private static final long serialVersionUID = 1L;

    @AField(rotulo = "Descrição")
    @Column(nullable = true)
    private String descricao;

    @AField(rotulo = "Nome")
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private String nome;

    @AField(rotulo = "Sigla")
    @Validacao(nulo = Level.ERRO$IMPEDITIVO)
    @Column(nullable = false)
    private String sigla;

    @Override
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.getSigla();
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        final AbstractTabela other = (AbstractTabela) obj;

        if ( !Objects.equals(this.descricao, other.descricao) )
            return false;
        if ( !Objects.equals(this.nome, other.nome) )
            return false;
        if ( !Objects.equals(this.sigla, other.sigla) )
            return false;

        return true;
    }

}
