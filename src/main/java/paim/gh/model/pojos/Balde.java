package paim.gh.model.pojos;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import paim.wingchun.model.AType;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel;
import paim.wingchun.model.pojos.Pojo;

/**
 * Balde contem os elementos disponiveis e utilizados para iteracoes<BR>
 * por exemplo, ao setar dias da semana como seg e ter, o sistema somente ira encontrar um ajuste que ocupe esses dias
 *
 * @author temujin Sep 11, 2011
 */
@AType(rotulo = "Balde", rotuloS = "Balde")
@Entity
public class Balde extends Pojo {

    private static final long serialVersionUID = 1L;

    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<Curso> cursos = new ArrayList<>();

    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<Dia> dias = new ArrayList<>();

    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<Disciplina> disciplinas = new ArrayList<>();

    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<Doscente> doscentes = new ArrayList<>();

    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<Horario> horarios = new ArrayList<>();

    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<Sala> salas = new ArrayList<>();

    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<Semestre> semestres = new ArrayList<>();

    @Validacao(nulo = ErroModel.Level.NENHUM)
    @Cascade({ CascadeType.EVICT, CascadeType.REFRESH })
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<Turno> turnos = new ArrayList<>();

    public List<Dia> getDias() {
        return this.dias;
    }

    public void setDias(List<Dia> dias) {
        this.dias = dias;
    }

    public List<Horario> getHorarios() {
        return this.horarios;
    }

    public void setHorarios(List<Horario> horarios) {
        this.horarios = horarios;
    }

    public List<Sala> getSalas() {
        return this.salas;
    }

    public void setSalas(List<Sala> salas) {
        this.salas = salas;
    }

    public List<Turno> getTurnos() {
        return this.turnos;
    }

    public void setTurnos(List<Turno> turnos) {
        this.turnos = turnos;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List<Disciplina> getDisciplinas() {
        return this.disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<Doscente> getDoscentes() {
        return this.doscentes;
    }

    public void setDoscentes(List<Doscente> doscentes) {
        this.doscentes = doscentes;
    }

    public List<Semestre> getSemestres() {
        return this.semestres;
    }

    public void setSemestres(List<Semestre> semestres) {
        this.semestres = semestres;
    }

}
