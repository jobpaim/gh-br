package paim.gh.model.pojos;


import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;

import paim.wingchun.model.AField;
import paim.wingchun.model.AType;
import paim.wingchun.model.Validacao;
import paim.wingchun.model.modelos.ErroModel.Level;


/**
 * representa um simples objeto professor, apenas para cadastro
 *
 * @author paim 16/05/2011
 */
@AType(rotulo = "Doscente", rotuloS = "Doscentes")
@Entity
public class Doscente extends AbstractTabela {

    private static final long serialVersionUID = 1L;

    @AField(rotulo = "Carga Horária")
    @Validacao(nulo = Level.ERRO$IMPEDITIVO/* FIXME , negativo = Level.ERRO$IMPEDITIVO, zero = Level.ERRO$IMPEDITIVO */)
    @Column(nullable = false)
    private Integer cargaHoraria = 0;

    public void setCargaHoraria(Integer cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public Integer getCargaHoraria() {
        return cargaHoraria;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj) )
            return false;

        Doscente other = (Doscente) obj;

        if ( !Objects.equals(this.cargaHoraria, other.cargaHoraria) )
            return false;

        return true;
    }

}
