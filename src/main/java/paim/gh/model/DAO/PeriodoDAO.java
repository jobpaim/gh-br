/** @author paim 16/11/2011 */
package paim.gh.model.DAO;


import java.util.List;

import org.hibernate.Session;

import paim.gh.model.GradeavelException;
import paim.gh.model.pojos.Periodo;

/** @author paim 16/11/2011 */
public class PeriodoDAO extends AbstractGradeavelDAO<Periodo> {

    /** Referencia ao DAO correspondente. */
    private static PeriodoDAO objetoDAO;

    private PeriodoDAO() {}

    /** Retorna a instancia do DAO. <b>(Padrao de Projeto - Singleton)<b> */
    public static PeriodoDAO getInstance() {
        if ( objetoDAO == null )
            objetoDAO = new PeriodoDAO();
        return objetoDAO;
    }

    @Deprecated
    @Override
    public List<Periodo> getPeriodos(Session session, Periodo pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public List<Periodo> getPeriodos(Session session, List<Periodo> pojos) {
        throw new GradeavelException();
    }
}
