/** @author paim 16/11/2011 */
package paim.gh.model.DAO;


import paim.wingchun.model.DAO.DAO;

import paim.gh.model.pojos.Grade;

/** @author paim 16/11/2011 */
public class GradeDAO extends DAO<Grade> {

    /** Referencia ao DAO correspondente. */
    private static GradeDAO objetoDAO;

    private GradeDAO() {}

    /** Retorna a GradeDAO do DAO. <b>(Padrao de Projeto - Singleton)<b> */
    public static GradeDAO getInstance() {
        if ( objetoDAO == null )
            objetoDAO = new GradeDAO();
        return objetoDAO;
    }

}
