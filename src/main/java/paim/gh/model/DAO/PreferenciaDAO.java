/**
 * @author paim 09/12/2011
 */
package paim.gh.model.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.DAO.DAO;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.model.pojos.Preferencia;

/**
 * @author paim 09/12/2011
 */
public class PreferenciaDAO extends DAO<Preferencia> {

    /** Referencia ao DAO correspondente. */
    private static PreferenciaDAO objetoDAO;

    private PreferenciaDAO() {}

    /** Retorna a instancia do DAO. <b>(Padrao de Projeto - Singleton)<b> */
    public static PreferenciaDAO getInstance() {
        if ( objetoDAO == null )
            objetoDAO = new PreferenciaDAO();
        return objetoDAO;
    }

    /**
     * busca as preferencias que estao ligadas a esse pojo
     * 
     * @author paim 09/12/2011
     * @param session
     * @param pojo
     * @return List<Preferencias>
     */
    public List<Preferencia> getPreferencias(Session session, Pojo pojo) {
        String fieldName = Reflections.getFieldName(pojo.getClass());
        StringBuffer consulta = new StringBuffer();
        consulta.append("SELECT DISTINCT preferencia ");
        consulta.append(" FROM preferencia IN CLASS " + Preferencia.class.getSimpleName());
        consulta.append(" WHERE preferencia." + fieldName + ".id = idPojo");

        Query query = session.createQuery(consulta.toString());
        query.setLong("idPojo", pojo.getId());

        return query.list();
    }
}
