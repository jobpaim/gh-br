package paim.gh.model.DAO;


import java.util.List;

import org.hibernate.Session;

import paim.gh.model.GradeavelException;
import paim.gh.model.pojos.Grade;
import paim.gh.model.pojos.Turma;

public class TurmaDAO extends AbstractGradeavelDAO<Turma> {

    /** Referencia ao DAO correspondente. */
    private static TurmaDAO objetoDAO;

    private TurmaDAO() {}

    /** Retorna a instancia do DAO. <b>(Padrao de Projeto - Singleton)<b> */
    public static TurmaDAO getInstance() {
        if ( objetoDAO == null )
            objetoDAO = new TurmaDAO();
        return objetoDAO;
    }

    @Deprecated
    @Override
    public final List<Turma> list_by_Turma(Session session, Grade grade, Turma pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public final List<Turma> list_by_Turmas(Session session, Grade grade, List<Turma> pojos) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public final List<Turma> getTurmas(Session session, Turma pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public final List<Turma> getTurmas(Session session, List<Turma> pojos) {
        throw new GradeavelException();
    }
}
