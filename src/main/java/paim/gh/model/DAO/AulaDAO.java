/** @author temujin Jul 13, 2012 */
package paim.gh.model.DAO;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import paim.gh.model.GradeavelException;
import paim.gh.model.pojos.Aula;
import paim.gh.model.pojos.Disciplina;
import paim.gh.model.pojos.Grade;

/** @author temujin Jul 13, 2012 */
public class AulaDAO extends AbstractGradeavelDAO<Aula> {

    /** Referencia ao DAO correspondente. */
    private static AulaDAO objetoDAO;

    private AulaDAO() {}

    /** Retorna a instancia do DAO. <b>(Padrao de Projeto - Singleton)<b> */
    public static AulaDAO getInstance() {
        if ( objetoDAO == null )
            objetoDAO = new AulaDAO();
        return objetoDAO;
    }

    /** @author temujin Jul 14, 2012
     * @param session
     * @param grade
     * @param disciplina
     * @return Aula */
    public Aula getAula(Session session, Grade grade, Disciplina disciplina) {
        StringBuffer consulta = new StringBuffer();
        consulta.append("SELECT DISTINCT o FROM o IN CLASS " + Aula.class.getSimpleName());
        consulta.append("       WHERE o.grade.id = :idGrade");
        consulta.append("       AND o.disciplina.id = :idDisciplina");

        Query query = session.createQuery(consulta.toString());
        query.setLong("idGrade", grade.getId());
        query.setLong("idDisciplina", disciplina.getId());

        return (Aula) query.uniqueResult();
    }

    @Deprecated
    @Override
    public List<Aula> list_by_Aula(Session session, Grade grade, Aula pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public List<Aula> list_by_Aulas(Session session, Grade grade, List<Aula> pojos) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public List<Aula> list_by_Disciplina(Session session, Grade grade, Disciplina pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public List<Aula> list_by_Disciplinas(Session session, Grade grade, List<Disciplina> pojos) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public List<Aula> getAulas(Session session, Aula pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public List<Aula> getAulas(Session session, List<Aula> pojos) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public List<Disciplina> getDisciplinas(Session session, Aula pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public List<Disciplina> getDisciplinas(Session session, List<Aula> pojos) {
        throw new GradeavelException();
    }

}
