/**
 * @author paim 23/05/2011
 */
package paim.gh.model.DAO;


import org.hibernate.Query;
import org.hibernate.Session;

import paim.wingchun.model.DAO.DAO;

import paim.gh.model.pojos.Semestre;

/**
 * @author paim 23/05/2011
 */
public final class SemestreDAO extends DAO<Semestre> {

    private static SemestreDAO objetoDAO;

    private SemestreDAO() {}

    /** Retorna a instancia do DAO. <b>(Padrao de Projeto - Singleton)<b> */
    public static SemestreDAO getInstance() {
        if ( objetoDAO == null )
            objetoDAO = new SemestreDAO();
        return objetoDAO;
    }

    public boolean semestreDuplicado(Session session, Semestre pojo) {
        StringBuffer consulta = new StringBuffer();
        consulta.append("SELECT DISTINCT o FROM o IN CLASS ").append(Semestre.class.getSimpleName());
        consulta.append("        WHERE obj." + "numSemestre" + " = :numSemestre");

        if ( pojo.getId() != null )
            consulta.append(" AND obj.id <> :id");

        Query query = session.createQuery(consulta.toString());
        query.setLong("numSemestre", pojo.getNumSemestre());

        if ( pojo.getId() != null )
            query.setLong("id", pojo.getId());

        query.setMaxResults(1);

        return query.uniqueResult() != null;
    }

}
