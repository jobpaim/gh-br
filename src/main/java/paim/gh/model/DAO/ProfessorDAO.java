/** @author paim 14/11/2011 */
package paim.gh.model.DAO;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import paim.gh.model.GradeavelException;
import paim.gh.model.pojos.Doscente;
import paim.gh.model.pojos.Grade;
import paim.gh.model.pojos.Professor;

import static paim.wingchun.app.Reflections.getFieldName;

/** @author paim 14/11/2011 */
public class ProfessorDAO extends AbstractGradeavelDAO<Professor> {

    /** Referencia ao DAO correspondente. */
    private static ProfessorDAO objetoDAO;

    private ProfessorDAO() {}

    /** Retorna a instancia do DAO. <b>(Padrao de Projeto - Singleton)<b> */
    public static ProfessorDAO getInstance() {
        if ( objetoDAO == null )
            objetoDAO = new ProfessorDAO();
        return objetoDAO;
    }



    public Professor getProfessor(Session session, Grade grade, Doscente doscente) {
        StringBuffer consulta = new StringBuffer();
        consulta.append("SELECT  DISTINCT o FROM o IN CLASS ").append(Professor.class.getSimpleName());
        consulta.append(" WHERE o.").append(getFieldName(Grade.class)).append(".id = :idGrade");
        consulta.append(" AND o.").append(getFieldName(Doscente.class)).append(".id = :idPojo");

        Query query = session.createQuery(consulta.toString());
        query.setLong("idGrade", grade.getId());
        query.setLong("idPojo", doscente.getId());
        query.setMaxResults(1);

        return (Professor) query.uniqueResult();
    }

    @Override
    @Deprecated
    public final List<Professor> list_by_Professor(Session session, Grade grade, Professor pojo) {
        throw new GradeavelException();
    }

    @Override
    @Deprecated
    public final List<Professor> list_by_Professores(Session session, Grade grade, List<Professor> pojos) {
        throw new GradeavelException();
    }

    @Override
    @Deprecated
    public List<Professor> list_by_Doscente(Session session, Grade grade, Doscente pojo) {
        throw new GradeavelException();
    }

    @Override
    @Deprecated
    public List<Professor> list_by_Doscentes(Session session, Grade grade, List<Doscente> pojos) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public final List<Professor> getProfessores(Session session, Professor pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public final List<Professor> getProfessores(Session session, List<Professor> pojos) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public final List<Doscente> getDoscentes(Session session, Professor pojo) {
        throw new GradeavelException();
    }

    @Deprecated
    @Override
    public final List<Doscente> getDoscentes(Session session, List<Professor> pojos) {
        throw new GradeavelException();
    }
}
