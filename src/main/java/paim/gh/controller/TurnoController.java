package paim.gh.controller;


import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Date;
import java.util.Objects;

import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import paim.wingchun.controller.WC_Event;

import paim.gh.model.Turnos;
import paim.gh.model.pojos.Turno;
import paim.gh.view.TurnoView;

/** @author paim 16/05/2011 */
public class TurnoController extends AbstractTabelaController<Turno, TurnoView> implements ItemListener,
                ChangeListener, MouseWheelListener {

    public TurnoController() {
        super();
    }

    @Override
    protected void apos_addListeners() {
        super.apos_addListeners();
        for ( JRadioButton radioButton : getVisao().getListaRadioButtons() ) {
            radioButton.addItemListener(this);
        }
        getVisao().getjInicio().addChangeListener(this);
        getVisao().getjFim().addChangeListener(this);
        getVisao().getjInicio().addMouseWheelListener(this);
        getVisao().getjFim().addMouseWheelListener(this);

        /* adicionando listener somente na parte Texto do jspinner */
        ((JSpinner.DefaultEditor) getVisao().getjInicio().getEditor()).getTextField().addFocusListener(this);
        ((JSpinner.DefaultEditor) getVisao().getjFim().getEditor()).getTextField().addFocusListener(this);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        /* verifica se nao esta parado */
        if ( !canDisparaEvento() )
            return;

        if ( e.getStateChange() != ItemEvent.SELECTED )
            return;

        if ( !(e.getSource() instanceof JRadioButton) )
            return;

        Object valorNovo = null;
        Object valorAntigo = null;

        JRadioButton componente = (JRadioButton) e.getSource();
        Turno pojo = (Turno) getVisao().getObjetoReferencia();

        valorNovo = Turnos.valueOf(componente.getActionCommand());
        valorAntigo = pojo.getTurno();

        /* se nada mudou cai fora */
        if ( Objects.equals(valorAntigo, valorNovo) )
            return;

        Turnos turnos = (Turnos) valorNovo;

        /* altera no objeto */
        pojo.setTurno(turnos);
        pojo.setNome(turnos.getNome());
        pojo.setSigla(turnos.getSigla());

        /* altera na visao */
        getVisao().getJNome().setText(turnos.getNome());
        getVisao().getJSigla().setText(turnos.getSigla());
        /* notifica mudanca */
        fire(new WC_Event(Eventos.ALTERANDO, pojo, valorAntigo, valorNovo, e, componente));
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        /* verifica se nao esta parado */
        if ( !canDisparaEvento() )
            return;

        if ( !(e.getSource() instanceof JSpinner) )
            return;

        Object valorNovo = null;
        Object valorAntigo = null;

        JSpinner componente = (JSpinner) e.getSource();
        valorNovo = componente.getModel().getValue();

        Turno pojo = (Turno) getVisao().getObjetoReferencia();

        if ( getVisao().getjInicio().equals(componente) ) {
            valorAntigo = pojo.getInicio();
            if ( Objects.equals(valorAntigo, valorNovo) )
                return;
            /* altera no objeto */
            pojo.setInicio((Date) valorNovo);
            /* notifica mudanca */
            fire(new WC_Event(Eventos.ALTERANDO, pojo, valorAntigo, valorNovo, e, componente));
            /* se data de fim eh anterior ao inicio, ajuste e dispara evento automaticamnete */
            if ( pojo.getFim().compareTo(pojo.getInicio()) < 0 ) {
                getVisao().getjFim().getModel().setValue(pojo.getInicio());
            }

        }
        else if ( getVisao().getjFim().equals(componente) ) {
            valorAntigo = pojo.getFim();
            if ( Objects.equals(valorAntigo, valorNovo) )
                return;
            /* altera no objeto */
            pojo.setFim((Date) valorNovo);
            /* notifica mudanca */
            fire(new WC_Event(Eventos.ALTERANDO, pojo, valorAntigo, valorNovo, e, componente));
            /* se data de inicio eh posterior ao fim, ajuste e dispara evento automaticamnete */
            if ( pojo.getInicio().compareTo(pojo.getFim()) > 0 ) {
                getVisao().getjInicio().getModel().setValue(pojo.getFim());
            }
        }
        else
            /* se nao for outro componente cai fora */
            return;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        /* verifica se nao esta parado */
        if ( !canDisparaEvento() )
            return;

        if ( !(e.getSource() instanceof JSpinner) )
            return;
        JSpinner componente = (JSpinner) e.getSource();

        if ( e.getWheelRotation() < 0 ) {
            if ( componente.getNextValue() != null )
                componente.setValue(componente.getNextValue());
        }
        else {
            if ( componente.getPreviousValue() != null )
                componente.setValue(componente.getPreviousValue());
        }
    }

}
