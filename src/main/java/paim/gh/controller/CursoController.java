package paim.gh.controller;


import paim.gh.model.pojos.Curso;
import paim.gh.view.CursoView;

/** @author paim 12/05/2011 */
public class CursoController extends AbstractTabelaController<Curso, CursoView> {

    public CursoController() {
        super();
    }
}
