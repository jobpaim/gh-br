package paim.gh.controller;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Objects;

import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import paim.wingchun.controller.WC_Event;

import paim.gh.model.pojos.Semestre;
import paim.gh.view.SemestreView;

/**
 * @author paim 16/05/2011
 */
public class SemestreController extends AbstractTabelaController<Semestre, SemestreView> implements ChangeListener,
                MouseWheelListener {

    public SemestreController() {
        super();
    }

    @Override
    protected void apos_addListeners() {
        super.apos_addListeners();
        getVisao().getnSemestre().addChangeListener(this);
        getVisao().getnSemestre().addMouseWheelListener(this);
        /* adicionando listener somente na parte Texto do jspinner */
        ((JSpinner.DefaultEditor) getVisao().getnSemestre().getEditor()).getTextField().addFocusListener(this);

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        /* verifica se nao esta parado */
        if ( !canDisparaEvento() )
            return;

        if ( !(e.getSource() instanceof JSpinner) )
            return;
        JSpinner componente = (JSpinner) e.getSource();

        if ( e.getWheelRotation() < 0 ) {
            /* se for null, eh por que ultrapasou a escala, entao nao faz nada */
            if ( componente.getNextValue() != null )
                componente.setValue(componente.getNextValue());
        }
        else {
            /* se for null, eh por que ultrapasou a escala, entao nao faz nada */
            if ( componente.getPreviousValue() != null )
                componente.setValue(componente.getPreviousValue());
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        /* verifica se nao esta parado */
        if ( !canDisparaEvento() )
            return;

        if ( !(e.getSource() instanceof JSpinner) )
            return;

        Object valorNovo = null;
        Object valorAntigo = null;

        JSpinner componente = (JSpinner) e.getSource();
        valorNovo = componente.getModel().getValue();

        Semestre pojo = (Semestre) getVisao().getObjetoReferencia();

        if ( getVisao().getnSemestre().equals(componente) ) {
            valorAntigo = pojo.getNumSemestre();
            if ( Objects.equals(valorAntigo, valorNovo) )
                return;
            /* altera no objeto */
            pojo.setNumSemestre((Integer) valorNovo);
            String nome = valorNovo + "Nº Semestre";
            String sigla = valorNovo + "Nº Sem.";

            pojo.setNome(nome);
            pojo.setSigla(sigla);
            /* altera na visao */
            getVisao().getJNome().setText(nome);
            getVisao().getJSigla().setText(sigla);
            /* notifica mudanca */
            fire(new WC_Event(Eventos.ALTERANDO, pojo, valorAntigo, valorNovo, e, componente));
        }
        else
            /* se nao for outro componente cai fora */
            return;
    }
}
