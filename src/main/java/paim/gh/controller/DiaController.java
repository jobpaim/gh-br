package paim.gh.controller;


import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Objects;

import javax.swing.JRadioButton;

import paim.wingchun.controller.WC_Event;

import paim.gh.model.DiasSemana;
import paim.gh.model.pojos.Dia;
import paim.gh.view.DiaView;

/** @author paim 16/05/2011 */
public class DiaController extends AbstractTabelaController<Dia, DiaView> implements ItemListener {

    public DiaController() {
        super();
    }

    @Override
    protected void apos_addListeners() {
        super.apos_addListeners();

        for ( JRadioButton radioButton : getVisao().getListaRadioButtons() ) {
            radioButton.addItemListener(this);
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        /* verifica se nao esta parado */
        if ( !canDisparaEvento() )
            return;

        if ( e.getStateChange() != ItemEvent.SELECTED )
            return;

        if ( !(e.getSource() instanceof JRadioButton) )
            return;
        Object valorNovo = null;
        Object valorAntigo = null;

        JRadioButton componente = (JRadioButton) e.getSource();
        Dia pojo = (Dia) getVisao().getObjetoReferencia();

        valorNovo = DiasSemana.valueOf(componente.getActionCommand());
        valorAntigo = pojo.getDiaSemana();

        /* se nada mudou cai fora */
        if ( Objects.equals(valorAntigo, valorNovo) )
            return;

        DiasSemana dayw = (DiasSemana) valorNovo;

        /* altera no objeto */
        pojo.setDiaSemana(dayw);
        pojo.setNome(dayw.getNome());
        pojo.setSigla(dayw.getSigla());

        /* altera na visao */
        getVisao().getJNome().setText(dayw.getNome());
        getVisao().getJSigla().setText(dayw.getSigla());

        /* notifica mudanca */
        fire(new WC_Event(Eventos.ALTERANDO, pojo, valorAntigo, valorNovo, e, componente));
    }

}
