package paim.gh.controller;

import paim.gh.model.pojos.Sala;
import paim.gh.view.SalaView;

/**
 * @author paim 16/05/2011
 */
public class SalaController extends AbstractTabelaController<Sala, SalaView> {

    public SalaController() {
        super();
    }

}
