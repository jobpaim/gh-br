package paim.gh.controller;


import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Objects;

import javax.swing.JTextField;

import paim.wingchun.controller.AbstractController;
import paim.wingchun.controller.AbstractSimpleController;
import paim.wingchun.controller.WC_Event;

import paim.gh.model.pojos.AbstractTabela;
import paim.gh.view.AbstractTabelaView;

/** @author paim 12/05/2011 */
public abstract class AbstractTabelaController<P extends AbstractTabela, V extends AbstractTabelaView<P>> extends
                AbstractSimpleController<P, V> implements FocusListener {

    protected AbstractTabelaController() {
        super();
    }

    @Override
    protected void apos_addListeners() {
        /* adicionando listener aos componentes */
        getVisao().getJSigla().addFocusListener(this);
        getVisao().getJNome().addFocusListener(this);
        getVisao().getJDescricao().addFocusListener(this);
        getVisao().getJDescricao().addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                /* verifica se nao esta parado */
                if ( !canDisparaEvento() )
                    return;
                fire(new WC_Event(AbstractController.Eventos.EDITANDO, e));
            }

            @Override
            public void keyPressed(KeyEvent e) {}

            @Override
            public void keyReleased(KeyEvent e) {}
        });
    }

    @Override
    public void focusGained(FocusEvent e) {
        /* verifica se nao esta parado */
        if ( !canDisparaEvento() )
            return;

        Object componente = e.getSource();
        if ( componente instanceof JTextField ) {
            ((JTextField) componente).setForeground(Color.BLUE);
        }

    }

    @Override
    public void focusLost(FocusEvent e) {
        /* verifica se nao esta parado */
        if ( !canDisparaEvento() )
            return;

        Object valorNovo = null;
        Object valorAntigo = null;

        if ( !(e.getSource() instanceof JTextField) )
            return;

        JTextField componente = (JTextField) e.getSource();

        componente.setForeground(Color.BLACK);
        valorNovo = componente.getText();

        AbstractTabela pojo = (AbstractTabela) getVisao().getObjetoReferencia();

        if ( getVisao().getJSigla().equals(componente) ) {
            valorAntigo = pojo.getSigla();
            /* agora analizando os valores */
            if ( Objects.equals(valorAntigo, valorNovo) )
                return;
            /* OK deve se alterar o objeto referencia */
            pojo.setSigla((String) valorNovo);
            /* notifica mudanca */
            fire(new WC_Event(Eventos.ALTERANDO, pojo, valorAntigo, valorNovo, e, componente));
        }
        else if ( getVisao().getJNome().equals(componente) ) {
            valorAntigo = pojo.getNome();
            if ( Objects.equals(valorAntigo, valorNovo) )
                return;
            pojo.setNome((String) valorNovo);
            /* notifica mudanca */
            fire(new WC_Event(Eventos.ALTERANDO, pojo, valorAntigo, valorNovo, e, componente));
        }
        else if ( getVisao().getJDescricao().equals(componente) ) {
            valorAntigo = pojo.getDescricao();
            if ( Objects.equals(valorAntigo, valorNovo) )
                return;
            pojo.setDescricao((String) valorNovo);
            /* notifica mudanca */
            fire(new WC_Event(Eventos.ALTERANDO, pojo, valorAntigo, valorNovo, e, componente));
        }
        else
            /* se nao for outro componente cai fora */
            return;
    }
}
