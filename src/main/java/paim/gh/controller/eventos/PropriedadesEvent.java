/** @author temujin Jul 7, 2012 */
package paim.gh.controller.eventos;


import java.util.List;

import paim.wingchun.controller.Visualizar;
import paim.wingchun.controller.WC_Event;
import paim.wingchun.model.pojos.Pojo;

import paim.gh.app.Gerenciador;
import paim.gh.model.GradeavelException;
import paim.gh.model.DAO.AulaDAO;
import paim.gh.model.DAO.ProfessorDAO;
import paim.gh.model.DAO.TurmaDAO;
import paim.gh.model.pojos.*;
import paim.gh.view.paineis.propriedades.PropriedadesListener;

/** @author temujin Jul 7, 2012 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class PropriedadesEvent implements PropriedadesListener {

    @Override
    public void selectProfessor(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Professor>() {

            @SuppressWarnings("deprecation")
            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getProfessores(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getProfessores(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getProfessores(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }

    @Override
    public void selectAula(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Aula>() {

            @SuppressWarnings("deprecation")
            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getAulas(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getAulas(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getAulas(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }

    @Override
    public void selectDia(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Dia>() {

            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getDias(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getDias(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getDias(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }

    @Override
    public void selectHorario(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Horario>() {

            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getHorarios(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getHorarios(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getHorarios(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }

    @Override
    public void selectTurno(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Turno>() {

            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getTurnos(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getTurnos(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getTurnos(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }

    @Override
    public void selectSala(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Sala>() {

            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getSalas(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getSalas(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getSalas(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }

    @Override
    public void selectCurso(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Curso>() {

            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getCursos(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getCursos(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getCursos(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }

    @Override
    public void selectSemestre(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Semestre>() {

            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getSemestres(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getSemestres(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getSemestres(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }

    @Override
    public void selectTurma(WC_Event e) {
        final Pojo selecionado = Gerenciador.getInstancia().getSelecionado();
        new Visualizar<Turma>() {

            @SuppressWarnings("deprecation")
            @Override
            protected List getPojosIniciais() throws Exception {
                if ( selecionado instanceof Professor )
                    return ProfessorDAO.getInstance().getTurmas(getSessao(), (Professor) selecionado);
                else if ( selecionado instanceof Aula )
                    return AulaDAO.getInstance().getTurmas(getSessao(), (Aula) selecionado);
                else if ( selecionado instanceof Turma )
                    return TurmaDAO.getInstance().getTurmas(getSessao(), (Turma) selecionado);
                throw new GradeavelException();
            }
        };
    }


    // @SuppressWarnings({ "unchecked", "rawtypes", "unused" })
    // final Selecionar<?, ?> contro = new Selecionar(null, pai, e.getComponent(), Sala.class) {
    //
    // @Override
    // protected List<Sala> getPojosIniciais() throws Exception {
    // /* os pojos populados inicialmente devem ser os do banco menos os ja utilizados */
    // List<Sala> salas = ProfessorDAO.getInstance().getSalas(getSessao(), (Professor) pai);
    // List<Sala> objetos = getModelo().complementary(getSessao(), salas);
    // return objetos;
    // }
    //
    // /* ignora o super */
    // @Override
    // protected void apos_setObjeto(Object objeto) {}
    //
    // @Override
    // protected void apos_confirmar(Object objeto, List erros, AbstractView.Estado estado) {
    // getVisao().fechar();
    //
    // List<Pojo> selecao = (List<Pojo>) getSelecao();
    // if ( selecao.isEmpty() )
    // return;
    //
    // // TODO preciso adicionar as salas ao professor
    // // nao adicionar eu acho que isso eh questao de preferencias obrigatorias.ok poderia apenas mostrar
    // // prepara janela auxiliar para modo red only
    //
    // // addObjetos(selecao, nodo);
    // }
    // };

    // professor.getp
    // preciso adicionar os novos(somente os novos?) no objeto pai
    // criar metodo add e set no dao...

}
