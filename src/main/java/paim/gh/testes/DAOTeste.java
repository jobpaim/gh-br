package paim.gh.testes;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import paim.gh.model.DiasSemana;
import paim.gh.model.Opcionalidade;
import paim.gh.model.modelos.*;
import paim.gh.model.pojos.*;

@SuppressWarnings("unused")
public class DAOTeste {


    private Session sessao;

    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        try {
            Grade grade = new Grade();
            grade.setNome("grade1");
            grade.setSigla("gr1");

            Horario horario = new Horario();
            Disciplina disciplina = new Disciplina();
            Curso curso = new Curso();
            Turno turno = new Turno();
            Semestre semestre = new Semestre();
            Dia dia = new Dia();
            dia.setDiaSemana(DiasSemana.SEGUNDA);





            Professor professor = new Professor();
            //            professor.setNome("prof1");

            Preferencia  preferencia1Prof = new Preferencia();
            preferencia1Prof.setOpcionalidade(Opcionalidade.PREFERIVEL);
            //            preferencia1Prof.setNome("preferencia1Prof");
            //            preferencia1Prof.setProfessorDono(professor);

            Preferencia  preferencia2Prof = new Preferencia();
            preferencia2Prof.setOpcionalidade(Opcionalidade.PREFERIVEL);
            //            preferencia2Prof.setNome("preferencia2Prof");
            //            preferencia2Prof.setProfessorDono(professor);

            professor.getPreferencias().add(preferencia1Prof);
            professor.getPreferencias().add(preferencia2Prof);

            Sala sala   = new Sala();
            sala.setNome("sala1");

            Turma turma = new Turma();
            //            aula.setGrade(grade);
            //            aula.setNome("teste hibernate 3");
            //            aula.setSigla("au");
            //            aula.setHorario(periodo);
            //            aula.setDisciplina(disciplina);
            //            aula.setCurso(curso);
            //            aula.setTurno(turno);
            //            aula.setSemestre(semestre);
            //            aula.setDiaSemana(diaSemana);


            Preferencia preferencia1Aula = new Preferencia();
            //            preferencia1Aula.setNome("pref aula 1");
            preferencia1Aula.setOpcionalidade(Opcionalidade.INEVITAVEL);
            //            preferencia1Aula.setAula(aula);

            Preferencia preferencia2Aula = new Preferencia();
            //            preferencia2Aula.setNome("pref aula 2");
            preferencia2Aula.setOpcionalidade(Opcionalidade.INEVITAVEL);
            //            preferencia2Aula.setAulaDono(aula);

            turma.getPreferencias().add(preferencia1Aula);
            turma.getPreferencias().add(preferencia2Aula);





            //            aula.getProfessores().add(professor);
            //            aula.getSalas().add(sala);










            //            grade.getAula().add(aula);


            try {
                //                HSQLDBUtil.iniciaHSQLDB();
//           TODO arrumar nova versao     HyperSQLs.start(true, HibernateTeste.bancoPadrao, HibernateTeste.usuarioPadrao, HibernateTeste.senha);

            }
            catch ( Throwable e ) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//          TODO arrumar nova versao           if ( !HyperSQLs.isOnline() ) {
//          TODO arrumar nova versao                 return;
//          TODO arrumar nova versao                }

            SessionFactory sessionFactory = null;
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
            try {
                sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            }
            catch ( Exception e ) {
                /* The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory so
                 * destroy it manually. */
                StandardServiceRegistryBuilder.destroy(registry);
            }

            Session session = sessionFactory.openSession();
            session.setFlushMode(FlushMode.COMMIT);

            HorarioModel.getInstance().saveOrUpdate(session, horario);
            DisciplinaModel.getInstance().saveOrUpdate(session, disciplina);
            CursoModel.getInstance().saveOrUpdate(session, curso);
            TurnoModel.getInstance().saveOrUpdate(session, turno);
            SemestreModel.getInstance().saveOrUpdate(session, semestre);
            DiaModel.getInstance().saveOrUpdate(session, dia);

            ProfessorModel.getInstance().saveOrUpdate(session, professor);
            SalaModel.getInstance().saveOrUpdate(session, sala);

            GradeModel.getInstance().saveOrUpdate(session, grade);
            //            TurmaModel.get().persistir(session, aula);

            //            			session.getTransaction().commit();
            session.flush();

            System.out.println("Gravado contato: " + turma.getId());

        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}
