/**
 * @author paim 17/11/2011
 */
package paim.gh.testes;

import paim.wingchun.model.DAO.DAO;
import paim.wingchun.model.modelos.Model;

import paim.gh.model.modelos.TurmaModel;

/**
 * @author paim 17/11/2011
 */
public class testeClasse {

    public static void main(String[] args) {
        Class<?> classe = TurmaModel.class;
        if ( classe.isAssignableFrom(DAO.class) )
            System.out.println("classe.isAssignableFrom(DAO.class)");
        else if ( DAO.class.isAssignableFrom(classe) ) {
            System.out.println("DAO.class.isAssignableFrom(classe) ");
        }else if ( classe.isAssignableFrom(Model.class) )
            System.out.println("classe.isAssignableFrom(Modelo.class)");
        else if ( Model.class.isAssignableFrom(classe) ) {
            System.out.println("Modelo.class.isAssignableFrom(classe) ");
        }

    }
}
