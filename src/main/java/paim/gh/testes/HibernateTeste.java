package paim.gh.testes;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import paim.wingchun.app.WC_Properties;

import paim.gh.model.DiasSemana;
import paim.gh.model.pojos.*;

@SuppressWarnings({ "unused", "deprecation" })
public class HibernateTeste {
    @Deprecated
    public static final String bancoPadrao = "ghdb";

    @Deprecated
    public static final String usuarioPadrao = "sa";

    @Deprecated
    public static final String senha = "";
    private Session sessao;

    public static void main(String[] args) {
        try {
            WC_Properties.verificaPropertiesApp();

            /* simplismente inicializa constantes dependentes das propriedades da aplicacao */
            Grade grade = new Grade();
            grade.setNome("grade1");
            grade.setSigla("gr1");

            Horario horario = new Horario();
            Disciplina disciplina = new Disciplina();
            disciplina.setDescricao("descricao descricao descricao....");
            disciplina.setNome("nome disciplina nome nome nome");
            disciplina.setSigla("sigla sigla sigla");
            Curso curso = new Curso();
            Turno turno = new Turno();
            Semestre semestre = new Semestre();
            Dia dia = new Dia();
            dia.setDiaSemana(DiasSemana.SEGUNDA);

            Turma turma = new Turma();
            // aula.setGrade(grade);
            // aula.setNome("teste hibernate 3");
            // aula.setSigla("au");
            // aula.setHorario(periodo);
            // aula.setDisciplina(disciplina);
            // aula.setCurso(curso);
            // aula.setTurno(turno);
            // aula.setSemestre(semestre);
            // aula.setDiaSemana(diaSemana);

            try {
                // HSQLDBUtil.iniciaHSQLDB();
//              TODO arrumar nova versao             HyperSQLs.start(true, HibernateTeste.bancoPadrao, HibernateTeste.usuarioPadrao, HibernateTeste.senha);

            }
            catch ( Throwable e ) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            /* se jï¿½ estï¿½ em execuï¿½ï¿½o, dï¿½ erro */
//          TODO arrumar nova versao         if ( !HyperSQLs.isOnline() ) {
//          TODO arrumar nova versao                   return;
//          TODO arrumar nova versao                }


            SessionFactory sessionFactory = null;
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
            try {
                sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            }
            catch ( Exception e ) {
                /* The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory so
                 * destroy it manually. */
                StandardServiceRegistryBuilder.destroy(registry);
            }

            Session session = sessionFactory.openSession();
            session.setFlushMode(FlushMode.COMMIT);


            session.beginTransaction();

            session.save(horario);
            session.save(disciplina);
            session.save(curso);
            session.save(turno);
            session.save(semestre);
            session.save(dia);

            session.save(grade);
            // session.save(aula);
            session.getTransaction().commit();

            System.out.println("Gravado contato: " + turma.getId());

//          TODO arrumar nova versao         if ( HyperSQLs.isOnline() ) {
//          TODO arrumar nova versao                  HyperSQLs.shutdown();
//          TODO arrumar nova versao               }
            System.exit(0);

        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}
