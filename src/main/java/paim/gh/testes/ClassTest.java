/**
 * @author temujin
 * Feb 7, 2012
 *
 */
package paim.gh.testes;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 * @author temujin
 * Feb 7, 2012
 *
 */
@SuppressWarnings("serial")
public class ClassTest extends JPanel {
    private JTextField txtAsd;
    public ClassTest() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        JLabel lblNome = new JLabel("nome");
        add(lblNome);
        
        txtAsd = new JTextField();
        txtAsd.setEditable(false);
        txtAsd.setText("asd");
        add(txtAsd);
        txtAsd.setColumns(10);
    }

}
