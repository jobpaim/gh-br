/** @author paim 27/01/2012 */
package paim.wingchun.testes;


import java.util.Date;
import java.util.Objects;

/** @author paim 27/01/2012 */
public class TestEquals2 {

    /** verifica se dois objetos sao iguais
     *
     * @author paim 14/12/2011
     * @param esse
     * @param outro
     * @return boolean */
    @Deprecated
    public static boolean iguaisOLD(Object esse, Object outro) {
        if ( esse == null ) {
            if ( outro != null )
                return false;
        }
        else if ( !esse.equals(outro) )
            return false;
        return true;
    }

    static void oka() throws InterruptedException {
        boolean b;
        long xxx = 5_000_000_000L;

        System.out.println(new Date());

        for ( long i = 0; i < xxx; i++ ) {

            b = iguaisOLD(null, null);
            // System.out.println(b);
            b = iguaisOLD("A", null);
            // System.out.println(b);
            b = iguaisOLD(null, "A");
            // System.out.println(b);
            b = iguaisOLD("A", "B");
            // System.out.println(b);
            b = iguaisOLD("B", "A");
            // System.out.println(b);
            b = iguaisOLD("A", "A");
            // System.out.println(b);

        }
        System.out.println(new Date());
        Thread.sleep(1000);
        System.out.println("-----------------------------");
        System.out.println(new Date());
        for ( long i = 0; i < xxx; i++ ) {
            b = Objects.equals(null, null);
            // System.out.println(b);
            b = Objects.equals("A", null);
            // System.out.println(b);
            b = Objects.equals(null, "A");
            // System.out.println(b);
            b = Objects.equals("A", "B");
            // System.out.println(b);
            b = Objects.equals("B", "A");
            // System.out.println(b);
            b = Objects.equals("A", "A");
            // System.out.println(b);
        }
        System.out.println(new Date());
    }

    static void uka() {
        boolean b;

        b = Objects.equals(null, null);
        System.out.println("null, null\t" + b);
        b = Objects.equals("A", null);
        System.out.println("A, null\t" + b);
        b = Objects.equals(null, "A");
        System.out.println("null, A\t" + b);
        b = Objects.equals("A", "B");
        System.out.println("A, B\t" + b);
        b = Objects.equals("B", "A");
        System.out.println("B, A\t" + b);
        b = Objects.equals("A", "A");
        System.out.println("A, A\t" + b);
    }

    @SuppressWarnings("unused")
    public static void main(String[] args) throws InterruptedException {
        uka();
    }
}
