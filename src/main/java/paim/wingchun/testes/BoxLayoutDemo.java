package paim.wingchun.testes;


import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import paim.wingchun.view.componentes.WC_TextField;

public class BoxLayoutDemo {
    public static void addComponentsToPane(Container pane) {
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        addAButton("Button 1", pane);
        pane.add(Box.createVerticalGlue());
        addAButton("Button 2", pane);
        addAButton("Button 3", pane);
        pane.add(Box.createVerticalGlue());
        addAButton("Long-Named Button 4", pane);
        addAButton("5", pane);
    }

    private static void addAButton(String text, Container container) {
        //        JButton button = new JButton(text);
        //        JLabel button = new JLabel(text);
//        JTextField button = new JTextField(text);
//        button.setMaximumSize( button.getPreferredSize() );

//        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        final WC_TextField button = new WC_TextField();
        button.setLabel(text);
//        button.setWarning(Color.RED);
//        button.setMaximumSize( button.getPreferredSize() );
        
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
//                button.cleanWarning();
//                button.setWarning(Color.RED);
                
            }
        });
        
        container.add(button);
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("BoxLayoutDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set up the content pane.
        //        addComponentsToPane(frame.getContentPane());

        Container pane2 = frame.getContentPane();
        JPanel pane = new JPanel();
        pane2.add(pane);
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
        addAButton("Button 1", pane);
//        pane.add(Box.createVerticalGlue());
        addAButton("Button 2", pane);
        addAButton("Button 3", pane);
        pane.add(Box.createVerticalGlue());
        addAButton("Long-Named Button 4", pane);
//        addAButton("5", pane);
        //Display the window.
        //        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }
}