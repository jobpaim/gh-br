/**
 * @author paim 11/05/2010
 */
package paim.wingchun.testes;

import javax.swing.JFrame;

import paim.wingchun.tarefa.ExcecaoCancelamento;
import paim.wingchun.tarefa.Tarefa;
import paim.wingchun.tarefa.TarefaComBarras;

/**
 * @author paim 11/05/2010
 */
public class TesteTarefa extends TarefaComBarras {

    public TesteTarefa(String nomeTarefa) {
        super(nomeTarefa, 1);
    }

    @Override
    protected void fazProcessamento() throws Exception {
        try {
            int tamanho = 10;
            int tamanho2 = 10;
            getCTarefa().getBarra(0).setMaximo(tamanho);
            for ( int i = 1; i <= tamanho; i++ ) {
                String mess1 = ((Integer) i).toString();
                getCTarefa().addMenssagem("mess1");
                getCTarefa().getBarra(0).setRotulo(mess1);
                getCTarefa().getBarra(1).setMaximo(tamanho2);
                getCTarefa().getBarra(1).limpa();
                for ( int j = 1; j <= tamanho2; j++ ) {
                    String mess2 = ((Integer) j).toString();
                    getCTarefa().addMenssagem("\t\tmess2");
                    getCTarefa().getBarra(1).setRotulo(mess2);
                    getCTarefa().getBarra(1).incrementa();
                    Thread.sleep(200);
                }
                getCTarefa().getBarra(0).incrementa();
            }
            getCTarefa().setRotulo("feito");
        }
        catch ( ExcecaoCancelamento e ) {

        }        
        finally {
            getCTarefa().termina();
        }
    }

    @Override
    protected void log(String passo, boolean mostrarEmProgresso) {
    // TODO Auto-generated method stub

    }

    @Override
    protected JFrame getOwner() {
        return new JFrame("teste ");
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    Tarefa tarefa = new TesteTarefa("teste...");
                    tarefa.newThread();
                    tarefa.start();
                }
                catch ( Exception e ) {
                    e.printStackTrace();
                }
            }
        });

    }

}
