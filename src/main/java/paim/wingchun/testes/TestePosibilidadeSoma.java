package paim.wingchun.testes;

import java.util.Collections;
import java.util.Vector;

public class TestePosibilidadeSoma {

    public TestePosibilidadeSoma() {
        super();
    }

    public static void main(String[] args) {

        // Set<Vector> saka = new HashSet<Vector>();
        // saka.addAll(null);
        // ArrayList<Integer> laka = new ArrayList<Integer>();
        //
        // Vector<Integer> vaka2 = new Vector<Integer>();
        // vaka2.add(2);
        // saka.add(vaka2);
        //
        // // Vector<Integer> vaka = new Vector<Integer>();
        // Vector<Integer> vaka = new Vector<Integer>();
        // vaka.add(1);
        // vaka.add(2);
        // Collections.sort(vaka);
        // saka.add(vaka);
        //
        // // Vector<Integer> vaka3 = new Vector<Integer>();
        // Vector<Integer> vaka3 = new Vector<Integer>();
        // vaka3.add(2);
        // vaka3.add(1);
        // Collections.sort(vaka3);
        // saka.add(vaka3);
        //
        // if (vaka.equals(vaka3)) {
        // System.out.println("igual");
        // }

        for ( int l = 1; l <= 10; l++ ) {
            // Vector<Vector<Integer>> aaa = m(l);
            //
            // System.out.println(aaa);
            System.out.println(m2(l));
        }

    }

    public static Vector<Vector<Integer>> m(Integer i) {
        Vector<Vector<Integer>> saka = new Vector<Vector<Integer>>();

        for ( int j = i; j > 0; j-- ) {

            int rd = i / j;
            int rr = i % j;

            /* caso primeiro elemento */
            Vector<Integer> v = new Vector<Integer>();
            for ( int k = 1; k <= rd; k++ ) {
                v.add(j);
            }
            /* caso rr */
            if ( rr != 0 )
                v.add(rr);

            Collections.sort(v, Collections.reverseOrder());

            saka.add(v);

        }

        return saka;
    }

    @SuppressWarnings("unchecked")
    public static Vector<Vector<Integer>> m2(Integer i) {
        Vector<Vector<Integer>> result = new Vector<Vector<Integer>>();

        for ( int j = i; j > 0; j-- ) {

            int rd = i / j;
            int rr = i % j;

            /* caso primeiro elemento */
            Vector<Integer> possibilidade = new Vector<Integer>();
            for ( int k = 1; k <= rd; k++ ) {
                possibilidade.add(j);
            }

            /* caso rr */
            if ( rr != 0 )
                possibilidade.add(rr);

            /*deixa sempre ordenado, para que o add nao duplique possibilidades com odrens diferentes*/
            Collections.sort(possibilidade, Collections.reverseOrder());
            /*adiciono ao resultado*/
            result.add(possibilidade);


            /*analizando 2 elemento e posteriores..., */
            for ( int k = 1; k < possibilidade.size(); k++ ) {
                Integer ele = possibilidade.elementAt(k);
                /*se elemento >1 eh necessario quebrar essa possibilidade em outras */
                if ( ele > 1 ) {
                    Vector<Integer> clonePossibilidade = (Vector<Integer>) possibilidade.clone();
                    /*remove o elemento maior que 1*/
                    clonePossibilidade.removeElementAt(k);

                    /*recursivamente, encontra as possibilidades para esse elemento*/
                    Vector<Vector<Integer>> subPossibilidades = m2(ele);

                    /*monta as subpossibilidades desse elemento...*/
                    for ( Vector<Integer> subPossibilidade : subPossibilidades ) {
                        Vector<Integer> vetor = (Vector<Integer>) clonePossibilidade.clone();
                        vetor.addAll(subPossibilidade);

                        Collections.sort(vetor, Collections.reverseOrder());
                        if ( !result.contains(vetor) )
                            result.add(vetor);
                    }

                }

            }



            /* caso rr */
            /* if (rr == 0) { Collections.sort(v, Collections.reverseOrder()); saka.add(v); } else */
            // if (rr == 1) {
            // v.add(rr);
            // Collections.sort(v, Collections.reverseOrder());
            // saka.add(v);
            // } else if (rr > 1) {
            // Vector<Vector<Integer>> ol = m2(rr);
            // for (Vector<Integer> vector : ol) {
            // Vector<Integer> v2 = (Vector<Integer>) v.clone();
            // v2.addAll(vector);
            // Collections.sort(v2, Collections.reverseOrder());
            // saka.add(v2);
            // }
            // // v.addAll(v2);
            //
            // }

        }

        return result;
    }
}
