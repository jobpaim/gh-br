/**
 * @author temujin Mar 16, 2014
 */
package paim.wingchun.testes;


/**
 * @author temujin Mar 16, 2014
 */
public class Generics {
    
    Object ooo;
    
   

    public static class Vehicle {}
        
    public static class Motorcycle extends Vehicle{}
    
    public static class Animal{}
    
    public static class Ave extends Animal{}
    
    public static interface Voa{}
    
    
    public static class Mamifero extends Animal{}
    
    public static class Cachorro extends Mamifero{}
    
    
    public static class pinguim extends Ave{}
    
    public static class MamiferoVoador extends Mamifero implements Voa{}
    public void adiciona1(Class<? extends Ave> clazz){
        ooo = clazz;
    }
    
    public void adiciona2(Class<? super Mamifero> clazz){
        ooo = clazz;
    }
    public void adiciona3(Class<? extends Animal> clazz){
        ooo = clazz;
    }
    
    public void adiciona4(Class<? extends Voa> clazz){
        ooo = clazz;
    }
    
    public static void main(String[] args) {
        
        Generics g = new Generics();
       //g.adiciona1(Vehicle.class); 
      //g.adiciona1(Motorcycle.class); 
       //g.adiciona1(Animal.class); 
       //g.adiciona1(Mamifero.class); 
       //g.adiciona1(Cachorro.class); 
       g.adiciona1(Ave.class); 
       g.adiciona1(pinguim.class); 
       //g.adiciona1(MamiferoVoador.class); 
       
       
        //g.adiciona2(Vehicle.class); 
        //g.adiciona2(Motorcycle.class); 
        g.adiciona2(Animal.class); 
        g.adiciona2(Mamifero.class); 
        //g.adiciona2(Cachorro.class); 
        //g.adiciona2(Ave.class); 
        //g.adiciona2(pinguim.class); 
        //g.adiciona2(MamiferoVoador.class); 
        
        //g.adiciona3(Vehicle.class); 
        //g.adiciona2(Motorcycle.class); 
        g.adiciona3(Animal.class); 
        g.adiciona3(Mamifero.class); 
        g.adiciona3(Cachorro.class); 
        g.adiciona3(Ave.class); 
        g.adiciona3(pinguim.class); 
        g.adiciona3(MamiferoVoador.class); 
        
        //g.adiciona4(Animal.class); 
        //g.adiciona4(Mamifero.class); 
        //g.adiciona4(Cachorro.class); 
        //g.adiciona4(Ave.class); 
        //g.adiciona4(pinguim.class); 
        g.adiciona4(MamiferoVoador.class); 
       
    }
}
