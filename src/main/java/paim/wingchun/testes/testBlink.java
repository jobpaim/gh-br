/**
 * @author paim 11/05/2010
 */
package paim.wingchun.testes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import paim.wingchun.swing.JLabelBlinkWave;

/**
 * @author paim 11/05/2010
 */
public class testBlink {

    private static void createAndShowUI() {
        JFrame frame = new JFrame("JLabelBlink");
//        final JLabelBlink bl = new JLabelBlink("I'm blinking!", true);
        final JLabelBlinkWave bl = new JLabelBlinkWave("I'm blinking!", false);
        // bl.setForegroundBL(Color.red);

        frame.getContentPane().setLayout(new java.awt.FlowLayout());
        frame.getContentPane().add(bl);

        final JButton b = new JButton("toogle blink");
        b.setText("toogle blink :" + (bl.isBlinking() ? "ON" : "OFF"));
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                // bl.setBlinking(!bl.isBlinking());
                bl.setBlinking(!bl.isBlinking());
                b.setText("toogle blink :" + (bl.isBlinking() ? "ON" : "OFF"));
            }
        });
        frame.getContentPane().add(b);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                createAndShowUI();
            }
        });
    }

}
