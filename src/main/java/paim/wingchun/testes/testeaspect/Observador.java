package paim.wingchun.testes.testeaspect;

public class Observador {

    private Observavel observavel;

    public void atualizar(Object mensagem) {
        // faz alguma coisa com o objeto
        System.out.print(" Recebi um aviso de: " + mensagem.toString());
    }

    public void setObservavel(Observavel observavel) {
        this.observavel = observavel;
    }

    public Observavel getObservavel() {
        return this.observavel;
    }

}