package paim.wingchun.testes.testeaspect;

public interface Observavel {

    void adicionarObservador( Observador obs ) ;

    void avisarObservadores();

    void removerObservador( Observador obs );

}
