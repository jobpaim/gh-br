/**
 * @author temujin Jul 13, 2012
 */
package paim.wingchun.testes;

import org.apache.commons.lang3.CharSet;

import com.google.common.base.CharMatcher;

/**
 * @author temujin Jul 13, 2012
 */
public class testeteste {

    static byte a[] = {
            65,
            66,
            67,
            68,
            69,
            70,
            71,
            72,
            73,
            74,
            75,
            76,
            77,
            78,
            79,
            80,
            81,
            82,
            83,
            84,
            85,
            86,
            87,
            88,
            89,
            90 };

    static byte b[] = {
      'A', 'B', 'C', 'D', 'E', 'F'  
    };
    
    public static void main(String[] args) {

        String result = "US";
        CharMatcher ula = CharMatcher.JAVA_UPPER_CASE;
        CharMatcher isla = ula.noneOf(result);
        System.out.println(isla);

        CharMatcher.anyOf("eko").collapseFrom("bookkeeper", '-');
        CharMatcher.anyOf("ab").trimFrom("abacatbab");
        CharMatcher.anyOf("ab").trimLeadingFrom("abacatbab");
        CharMatcher.anyOf("ab").trimTrailingFrom("abacatbab");

        CharMatcher.noneOf(result).matchesAnyOf("AUSJ");
        CharMatcher.inRange('A', 'Z');

        CharSet olka = CharSet.ASCII_ALPHA_UPPER;

        System.out.println(new String(a));
        System.out.println(new String(b));
        for(char c = 'A'; c <= 'Z'; c++) {
            System.out.print(c);
        }
        
        System.out.println("end");
    }
}
