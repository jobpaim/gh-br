/**
 * @author paim 20/05/2011
 */
package paim.wingchun.testes;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;


public class FrameKey {

    public static void main(String args[]) {
        final JFrame frame = new JFrame("Frame Key");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Action actionListener = new AbstractAction() {

            /**  */
            private static final long serialVersionUID = 1L;

            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JDialog dialog = new EscapeDialog(frame, "Hey");
                JButton button = new JButton("Okay");
                ActionListener innerActionListener = new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        System.out.println("Dialog Button Selected");
                    }
                };
                button.addActionListener(innerActionListener);
                
                dialog.setBackground(Color.WHITE);
                dialog.setTitle("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                dialog.setModal(true);             
                JPanel p = new JPanel();
                p.setBackground(Color.WHITE);
                dialog.getContentPane().add(p, BorderLayout.NORTH);
                dialog.getContentPane().add(button, BorderLayout.SOUTH);
                dialog.setSize(200, 200);
                dialog.show();
            }
        };

        JPanel content = (JPanel) frame.getContentPane();
        KeyStroke stroke = KeyStroke.getKeyStroke("M");

        InputMap inputMap = content.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(stroke, "OPEN");
        content.getActionMap().put("OPEN", actionListener);

        frame.setSize(300, 300);
        frame.setVisible(true);
    }
}


class EscapeDialog extends JDialog {

    /**  */
    private static final long serialVersionUID = 1L;

    public EscapeDialog() {
        this((Frame) null, false);
    }

    public EscapeDialog(Frame owner) {
        this(owner, false);
    }

    public EscapeDialog(Frame owner, boolean modal) {
        this(owner, null, modal);
    }

    public EscapeDialog(Frame owner, String title) {
        this(owner, title, false);
    }

    public EscapeDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
    }

    public EscapeDialog(Dialog owner) {
        this(owner, false);
    }

    public EscapeDialog(Dialog owner, boolean modal) {
        this(owner, null, modal);
    }

    public EscapeDialog(Dialog owner, String title) {
        this(owner, title, false);
    }

    public EscapeDialog(Dialog owner, String title, boolean modal) {
        super(owner, title, modal);
    }

    @Override
    protected JRootPane createRootPane() {
        JRootPane rootPane = new JRootPane();
        KeyStroke stroke = KeyStroke.getKeyStroke("ESCAPE");
        Action actionListener = new AbstractAction() {

            /**  */
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
            }
        };
        InputMap inputMap = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(stroke, "ESCAPE");
        rootPane.getActionMap().put("ESCAPE", actionListener);

        return rootPane;
    }
}
