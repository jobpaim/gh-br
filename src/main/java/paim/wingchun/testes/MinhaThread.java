/**
 * @author paim 10/05/2010
 */
package paim.wingchun.testes;

/**
 * @author paim 10/05/2010
 */
public class MinhaThread extends Thread {

    // área de dados da Thread
    private int contador;

    private int limite;

    // inicializador da MinhaThread
    public MinhaThread(int limite) {
        this.limite = limite;
        this.contador = 0;
    }

    // área de código da Thread
    @Override
    public void run() {
        while ( contador <= limite ) {
            System.out.println(super.getName() + "\t" + contador);
            contador++;
            // trecho de código para demonstração somente
            try {
                Thread.sleep(1000); // coloca a "thread" para "dormir" 1 segundo
            }
            catch ( InterruptedException e ) {
                e.printStackTrace(System.err);
            }
        }
    }

}
