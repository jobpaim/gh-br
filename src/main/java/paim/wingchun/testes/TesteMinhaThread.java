/**
 * @author paim 10/05/2010
 */
package paim.wingchun.testes;

/**
 * @author paim 10/05/2010
 */
public class TesteMinhaThread extends Thread {

    public static void main(String[] args) {
        MinhaThread mt1 = new MinhaThread(200);
        mt1.setName("MinhaThread1");
        MinhaThread mt2 = new MinhaThread(300);
        mt2.setName("MinhaThread2");
        MinhaThread mt3 = new MinhaThread(200);
        mt3.setName("MinhaThread3");
        MinhaThread mt4 = new MinhaThread(400);
        mt4.setName("MinhaThread4");
        mt2.start();
        mt1.start();
        mt3.start();
        mt4.start();
    }

}
