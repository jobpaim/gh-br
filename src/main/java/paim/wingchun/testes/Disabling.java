/**
 * @author paim 23/08/2011
 */
package paim.wingchun.testes;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Disabling implements ActionListener {

    JFileChooser fileChooser;

    JLabel label;

    Disabling() {
        fileChooser = new JFileChooser(".");
        boolean hide = true; // hide parent
        // false; // disable textField
        disableTextComponent(fileChooser, hide);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int retVal = fileChooser.showOpenDialog(null);
        // System.out.println("retVal = " + retVal);
        if ( retVal == JFileChooser.APPROVE_OPTION ) {
            File file = fileChooser.getSelectedFile();
            if ( file != null ) {
                label.setText(file.getPath());
            }
        }
    }

    private void disableTextComponent(Container parent, boolean hide) {
        Component[] c = parent.getComponents();
        for ( int j = 0; j < c.length; j++ ) {
            // System.out.println(unpack(c[j]));
            if ( unpack(c[j]).equals("MetalFileChooserUI$3") ) {
                // System.out.println("MetalFileChooserUI$3 = " + c[j]);
                // System.out.println("MetalFileChooserUI$3 parent = " +
                // unpack(c[j].getParent()));
                if ( hide )
                    c[j].getParent().setVisible(false);
                else
                    // disable
                    c[j].setEnabled(false);
            }
            if ( ((Container) c[j]).getComponentCount() > 0 ) {
                disableTextComponent((Container) c[j], hide);
            }
        }
    }

    private String unpack(Component c) {
        String s = c.getClass().getName();
        int dot = s.lastIndexOf(".");
        if ( dot != -1 )
            s = s.substring(dot + 1);
        return s;
    }

    private JPanel getContent() {
        label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        JButton button = new JButton("open dialog");
        button.addActionListener(this);
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.weighty = 1.0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        panel.add(label, gbc);
        gbc.anchor = GridBagConstraints.SOUTH;
        panel.add(button, gbc);
        return panel;
    }

    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setContentPane(new Disabling().getContent());
        f.setSize(360, 140);
        f.setLocation(200, 200);
        f.setVisible(true);
    }
}