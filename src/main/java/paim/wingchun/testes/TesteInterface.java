package paim.wingchun.testes;

import java.util.EventListener;

import javax.swing.event.EventListenerList;

public class TesteInterface {

    EventListenerList listeners = new EventListenerList();

    public interface i1 extends EventListener {

        public void a();
    }

    public interface i2 extends EventListener {

        public void b();
    }

    public void adicionaListener(i1 listener) {
        listeners.add(i1.class, listener);
    }

    public void adicionaListener(i2 listener) {
        listeners.add(i2.class, listener);
    }

    public void adicionaListener(EventListener listener) {
        listeners.add(EventListener.class, listener);
    }

    public TesteInterface() {}

    public static void main(String[] args) {
        TesteInterface t = new TesteInterface();

        i1 i1i1 = new i1() {

            @Override
            public void a() {
                System.out.println("i1");

            }
        };

        i2 i2i2 = new i2() {

            @Override
            public void b() {
                System.out.println("i2");

            }
        };

        t.adicionaListener(i1i1);
        t.adicionaListener(i2i2);
        t.adicionaListener((EventListener) i2i2);
        t.adicionaListener((EventListener) i1i1);
        System.out.println("eeee");

    }
}
