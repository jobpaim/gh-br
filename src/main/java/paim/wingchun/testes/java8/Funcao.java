package paim.wingchun.testes.java8;


import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

/** @author paim 14/02/2014
 * @since */
public class Funcao {

    public Funcao() {}

    static List<Objetao> jedis = Arrays.asList(new Objetao(1L, "Luke"), new Objetao(2L, "Obiwan"), new Objetao(3L,
                    "Quigon"));

    static void aka() {
        List<String> jedis = Arrays.asList("Luke", "Obiwan", "Quigon");
        jedis.forEach(jedi -> {
            System.out.println(jedi);
        });
    }

    static void eka() {

        jedis.forEach(jedi -> {
            System.out.println(jedi.getId());
        });

    }

    public static Function<Objetao, Long> id() {

        Function<Objetao, Long> function = new Function<Objetao, Long>() {

            @Nullable
            @Override
            public Long apply(@Nullable Objetao input) {
                if ( input == null )
                    return null;
                return input.getId();
            }
        };
        return function;
    }

    static Collection<Long> ika_guava() {
        return Collections2.transform(jedis, id());
    }

    static Collection<Long> ika_java8() {
        List<Long> ursa = jedis.stream().map(jedi -> jedi.getId()).collect(Collectors.toList());
        return ursa;
    }

    public static Predicate<Objetao> grandes() {
        Predicate<Objetao> predicate = new Predicate<Objetao>() {

            @Override
            public boolean apply(@Nullable Objetao input) {
                return input.getId() >= 2;
            }

        };
        return predicate;
    }

    static Collection<Objetao> oka_guava() {
        Collection<Objetao> ursa = Collections2.filter(jedis, grandes());
        return ursa;
    }

    static Collection<Objetao> oka_java8() {
        List<Objetao> ursa = jedis.stream().filter(o -> o.getId() >= 2).collect(Collectors.toList());
        return ursa;
    }

    public static void main(String[] args) {

        Collection<Long> d = ika_guava();
        Collection<Long> e = ika_java8();

        Collection<Objetao> a = oka_guava();
        Collection<Objetao> b = oka_java8();
        System.out.println("çkjlh");
    }

}
