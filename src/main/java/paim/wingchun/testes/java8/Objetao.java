package paim.wingchun.testes.java8;


/** @author paim 14/02/2014
 * @since */
public class Objetao {

    private Long id;

    private String s;

    public Objetao() {}

    public Objetao(Long id, String s) {
        super();
        this.id = id;
        this.s = s;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getS() {
        return this.s;
    }

    public void setS(String s) {
        this.s = s;
    }

}
