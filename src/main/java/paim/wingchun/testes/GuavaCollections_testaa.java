package paim.wingchun.testes;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import paim.wingchun.testes.GuavaCollections_testaa.Levelel;

/** @author paim 23/09/2013
 * @since */
public class GuavaCollections_testaa {

    enum Levelel {
        /** nao deve ser validado */
        NENHUM(0), AVISO(1), ALERTA(2), ERRO(4), ERRO$IMPEDITIVO(5), ERRO$PRIORITARIO(6);

        private int valor;

        private Levelel(int valor) {
            this.valor = valor;
        }

        public Integer getValor() {
            return valor;
        }

        public boolean isImpeditivo() {
            return (valor >= ERRO$IMPEDITIVO.getValor() ? true : false);
        }
    }

    public GuavaCollections_testaa() {

    }

    static void testapredicate() {
        List<Puka> pukas = new ArrayList<>();
        pukas.add(new Puka(Levelel.NENHUM));
        pukas.add(new Puka(Levelel.AVISO));
        pukas.add(new Puka(Levelel.AVISO));
        pukas.add(new Puka(Levelel.ALERTA));
        pukas.add(new Puka(Levelel.ALERTA));
        pukas.add(new Puka(Levelel.ERRO));
        pukas.add(new Puka(Levelel.ERRO));
        pukas.add(new Puka(Levelel.ERRO));
        pukas.add(new Puka(Levelel.ERRO$IMPEDITIVO));
        pukas.add(new Puka(Levelel.ERRO$IMPEDITIVO));
        pukas.add(new Puka(Levelel.ERRO$IMPEDITIVO));
        pukas.add(new Puka(Levelel.ERRO$IMPEDITIVO));
        pukas.add(new Puka(Levelel.ERRO$IMPEDITIVO));
        pukas.add(new Puka(Levelel.ERRO$PRIORITARIO));
        pukas.add(new Puka(Levelel.ERRO$PRIORITARIO));
        Puka uka = new Puka();
        pukas.add(uka);
        pukas.add(null);

        Collection<Puka> result = Collections2.filter(pukas, new Predicate_level_impeditivo());

        System.out.println("ok");

    }

    public static void main(String[] args) {
        testapredicate();
    }

}


class Puka {

    Levelel levelelo;

    public Puka() {}

    public Puka(Levelel levelelo) {
        this.levelelo = levelelo;
    }

    public Levelel getLevelelo() {
        return this.levelelo;
    }

    public void setLevelelo(Levelel levelelo) {
        this.levelelo = levelelo;
    }

    @Override
    public String toString() {
        return levelelo.name();
    }

}


class Predicate_level_impeditivo implements Predicate<Puka> {

    @Override
    public boolean apply(@Nullable Puka input) {
        return input.getLevelelo().getValor() >= Levelel.ERRO$IMPEDITIVO.getValor();
        // return false;
    }
}
