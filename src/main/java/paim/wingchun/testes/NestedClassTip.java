package paim.wingchun.testes;


public class NestedClassTip {
    private String name = "instancename";
    private static String staticName = "staticname";

    @SuppressWarnings("unused")
    public static void main(String args[]){
        NestedClassTip nt= new NestedClassTip();

        NestedClassTip.NestedOne nco = nt.new NestedOne();

        NestedClassTip.NestedTwo nct = new NestedClassTip.NestedTwo();
    }

    class NestedOne {
        NestedOne(){
            System.out.println(name);
            System.out.println(staticName);
        }
    }
    
    static class NestedTwo {
        NestedTwo(){
            System.out.println(staticName);
        }
    }
} 