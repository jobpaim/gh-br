/**
 * @author paim
 * 07/06/2011
 *
 */
package paim.wingchun.testes;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
@SuppressWarnings({ "deprecation", "serial" })
public class TesteJTable extends JFrame {

   private javax.swing.JPanel jContentPane = null;
   private JTable jTable = null;
   private JScrollPane jScrollPane = null;
   private JButton jButtonAddColumnToView = null;
   private JCheckBox jCheckBoxAutoCreate = null;
   private JButton jButtonAddColumnToModel = null;

   private int count = 0;

   private JButton jButtonNewModel = null;

   public TesteJTable() {
       super();
       initialize();
   }

   private JCheckBox getJCheckBoxAutoCreate() {
       if (jCheckBoxAutoCreate == null) {
           jCheckBoxAutoCreate = new JCheckBox();
           jCheckBoxAutoCreate.setBounds(27, 237, 268, 26);
           jCheckBoxAutoCreate.setText("AutoCreateColumnsFromModel");
           jCheckBoxAutoCreate.addActionListener(new ActionListener() {

               @Override
            public void actionPerformed(ActionEvent e) {

                   boolean b = getJCheckBoxAutoCreate().isSelected();
                   getJTable().setAutoCreateColumnsFromModel(b);

               }
           });

           // init
           jCheckBoxAutoCreate.setSelected(
               getJTable().getAutoCreateColumnsFromModel());
       }
       return jCheckBoxAutoCreate;
   }

   private JButton getJButtonAddColumnToModel() {
       if (jButtonAddColumnToModel == null) {
           jButtonAddColumnToModel = new JButton();
           jButtonAddColumnToModel.setBounds(29, 307, 264, 27);
           jButtonAddColumnToModel.setText("Add a column to the model");
           jButtonAddColumnToModel.addActionListener(new ActionListener() {

               @Override
            public void actionPerformed(ActionEvent e) {

                   // add a column to the model
                   DefaultTableModel model = (DefaultTableModel)getJTable().getModel();
                   model.addColumn("" + count++);

                   System.err.println("model: column count = "
                       + getJTable().getModel().getColumnCount());
                   System.err.println("view: column count = "
                       + getJTable().getColumnCount());

               }
           });
       }
       return jButtonAddColumnToModel;
   }

   private JButton getJButtonNewModel() {
       if (jButtonNewModel == null) {
           jButtonNewModel = new JButton();
           jButtonNewModel.setBounds(29, 346, 265, 27);
           jButtonNewModel.setText("Set new empty model to the view");
           jButtonNewModel.addActionListener(new ActionListener() {

               @Override
            public void actionPerformed(ActionEvent e) {

                   getJTable().setModel(new DefaultTableModel());

               }
           });
       }
       return jButtonNewModel;
   }

   public static void main(String[] args) {
       new TesteJTable().show();
   }

   private void initialize() {
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       this.setSize(351, 425);
       this.setContentPane(getJContentPane());
       this.setTitle("JFrame");
   }

   private JPanel getJContentPane() {
       if (jContentPane == null) {
           jContentPane = new javax.swing.JPanel();
           jContentPane.setLayout(null);
           jContentPane.add(getJScrollPane(), null);
           jContentPane.add(getJButtonAddColumnToView(), null);
           jContentPane.add(getJCheckBoxAutoCreate(), null);
           jContentPane.add(getJButtonAddColumnToModel(), null);
           jContentPane.add(getJButtonNewModel(), null);
       }
       return jContentPane;
   }

   private JTable getJTable() {
       if (jTable == null) {
           jTable = new JTable();
       }
       return jTable;
   }

   private JScrollPane getJScrollPane() {
       if (jScrollPane == null) {
           jScrollPane = new JScrollPane();
           jScrollPane.setBounds(25, 27, 269, 195);
           jScrollPane.setViewportView(getJTable());
       }
       return jScrollPane;
   }

   private JButton getJButtonAddColumnToView() {
       if (jButtonAddColumnToView == null) {
           jButtonAddColumnToView = new JButton();
           jButtonAddColumnToView.setBounds(29, 268, 262, 27);
           jButtonAddColumnToView.setText("Add a column to the view");
           jButtonAddColumnToView.addActionListener(new ActionListener() {

               @Override
            public void actionPerformed(ActionEvent e) {

                   // add a column to the view
                   TableColumn tc = new TableColumn();
                   getJTable().addColumn(tc);

                   System.err.println("model: column count = "
                       + getJTable().getModel().getColumnCount());
                   System.err.println("view: column count = "
                       + getJTable().getColumnCount());
               }
           });
       }
       return jButtonAddColumnToView;
   }
} 