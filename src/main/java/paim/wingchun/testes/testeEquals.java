/**
 * @author paim 14/12/2011
 */
package paim.wingchun.testes;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author paim 14/12/2011
 */
public class testeEquals {
    public static enum TipoDiaSemana {
        DOMINGO("Domingo", "Dom"),
        SEGUNDA("Segunda-Feira", "Seg"),
        TERCA("Terça-Feria", "Ter"),
        QUARTA("Quarta-Feira", "Qua"),
        QUINTA("Quinta-Feira", "Qui"),
        SEXTA("Sexta-Feira", "Sex"),
        SABADO("Sábado", "Sab");

        private String nome;

        private String sigla;

        private TipoDiaSemana(String nome, String sigla) {
            this.nome = nome;
            this.sigla = sigla;
        }

        public String getNome() {
            return nome;
        }

        public String getSigla() {
            return sigla;
        }

        public static List<TipoDiaSemana> getLista() {
            return Arrays.asList(TipoDiaSemana.values());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Date inicio = Calendar.getInstance().getTime();
        Thread.sleep(1000);
        Date fim = Calendar.getInstance().getTime();
        boolean b = Objects.equals(inicio, fim);

        Long l1 = 1L;
        Long l2 = 2L;
        boolean b2 = Objects.equals(l1, l2);

        TipoDiaSemana d1 = TipoDiaSemana.DOMINGO;
        TipoDiaSemana d2 = TipoDiaSemana.QUARTA;
        boolean b3 = Objects.equals(d1, d2);

        System.out.println(inicio);
        System.out.println(b);
        System.out.println(fim);
        System.out.println(l1);
        System.out.println(b2);
        System.out.println(l2);
        System.out.println(d1);
        System.out.println(b3);
        System.out.println(d2);
    }
}
