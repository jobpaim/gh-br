/**
 * @author paim 19/05/2011
 */
package paim.wingchun.testes;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import paim.wingchun.view.componentes.WC_Label;

/**
 * @author paim 19/05/2011
 */
public class testejlabel {

    private static void createAndShowUI() {
        JFrame frame = new JFrame("JLabel");
        JLabel label = new JLabel();
        label.setText("ttttttttttttt");
        label.setIcon(new ImageIcon(testejlabel.class.getResource("/imagens/tango/16x16/actions/media-record.png")));
//        label.setHorizontalAlignment(JLabel.LEFT);
        label.setHorizontalTextPosition(SwingConstants.LEFT);
        label.setDisabledIcon(null);
        label.setIcon(null);
        label.setIcon(new ImageIcon(testejlabel.class.getResource("/imagens/ico_vazio.gif")));
        
        WC_Label wC_Label = new WC_Label();
        
        
        
        frame.getContentPane().setLayout(new java.awt.FlowLayout());
//        frame.getContentPane().add(label);
        frame.getContentPane().add(wC_Label);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                createAndShowUI();
            }
        });
    }
}
