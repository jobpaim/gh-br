/**
 * @author temujin
 * Jun 1, 2013
 *
 */
package paim.wingchun.testes;


/**
 * @author temujin
 * Jun 1, 2013
 *
 */
public class ClassDemo2 {

static class A {
   public static void show() {
      System.out.println("Class A show() function");
   }
}

static class B extends A {
   public static void show() {
      System.out.println("Class B show() function");
   }
}


   public static void main(String[] args) {

     ClassDemo cls = new ClassDemo();
        Class<? extends ClassDemo> c = cls.getClass();
     System.out.println(c);

     Object obj = new A();
     B b1 = new B();
     b1.show();

     // casts object
     Object a = A.class.cast(b1);
     System.out.println(obj.getClass());
     System.out.println(b1.getClass());
     System.out.println(a.getClass());
   }

}
