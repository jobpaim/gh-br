/**
 * @author temujin Oct 28, 2011
 */
package paim.wingchun.testes;

import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.List;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.pojos.Pojo;

/**
 * @author temujin Oct 28, 2011
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class Testeparametr {

    List lista = new ArrayList<>();

    List<?> lista1 = new ArrayList<>();

    List<Pojo> lista2 = new ArrayList<>();

    List<? extends Pojo> lista3 = new ArrayList<>();

    List<? super Pojo> lista4 = new ArrayList<>();
    
    akakak<?> lista5 = new akakak();
    
    akakak<Integer> lista6 = new akakak();
    
    

    public static void main(String[] args) throws NoSuchFieldException {

        String usar = "lista1";

        Field field = Reflections.getField(Testeparametr.class, usar);

        Type type = field.getGenericType();

        /* se for um tipado */
        if ( ParameterizedType.class.isAssignableFrom(type.getClass()) ) {
            System.out.println("ParameterizedType");
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type primeiro = parameterizedType.getActualTypeArguments()[0];

            if ( WildcardType.class.isAssignableFrom(primeiro.getClass()) ) {
                /* eh uma mascara, acho que deve retornar o nome do atributo */
                System.out.println("WildcardType");
                
                WildcardType wildcardType = (WildcardType)primeiro;
                wildcardType.getLowerBounds();//super
                wildcardType.getUpperBounds(); //extends
                

            }
            else {
                System.out.println("OKOKOK");
                /* retornar a classe tipada */
                Class<?> classe = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                System.out.println(classe.getCanonicalName());
            }
        }
        else if ( GenericArrayType.class.isAssignableFrom(type.getClass()) ) {
            System.out.println("GenericArrayType");

        }
        else if ( TypeVariable.class.isAssignableFrom(type.getClass()) ) {
            System.out.println("TypeVariable");
        }
        else {
            System.out.println("Type");
            System.out.println("retornar nome atributo");

        }

    }

    
}
class akakak<E> {
    
}
