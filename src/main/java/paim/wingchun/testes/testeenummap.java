/**
 * @author paim 23/11/2011
 */
package paim.wingchun.testes;

import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author paim 23/11/2011
 */
public class testeenummap {

    enum Size {
        S, M, L, XL, XXL, XXXL;
    }

    public static void main(String[] args) {
        EnumMap<Size, String> sizeMap = new EnumMap<Size, String>(Size.class);

        Map<Size, String> akaka = new LinkedHashMap<>();

        sizeMap.put(Size.M, "M");
        sizeMap.put(Size.XXXL, "XXXL");
        sizeMap.put(Size.L, "L");
        sizeMap.put(Size.S, "S");
        sizeMap.put(Size.XXL, "XXL");
        sizeMap.put(Size.XL, "XL");

        akaka.put(Size.M, "M");
        akaka.put(Size.XXXL, "XXXL");
        akaka.put(Size.L, "L");
        akaka.put(Size.S, "S");
        akaka.put(Size.XXL, "XXL");
        akaka.put(Size.XL, "XL");

        for (Size keyss : sizeMap.keySet()) {
            System.out.println(keyss);
        }
        for (Size keyss : akaka.keySet()) {
            System.out.println(keyss);
        }

        //        for ( Size size : Size.values() ) {
        //            System.out.println(size + ":" + sizeMap.get(size));
        //        }
    }

}
