/**
 * @author paim 09/11/2011
 */
package paim.wingchun.testes.observadores;

import java.util.Observable;
import java.util.Observer;

/**
 * @author smota
 */
public class classeProcesso extends Observable implements Runnable {

    /**
     * Construtor que recebe um objeto que irá observa-lo
     * 
     * @param observador
     *            Objeto que irá acompanhar as mudanças deste objeto
     */
    public classeProcesso(Observer observador) {
        // Adiciona o objeto observador a lista de observadores
        addObserver(observador);
        // ...outras inicializações
    }

    /**
     * Ponto de entrada da Thread.
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        int i;
        for ( i = 0; i <= classeGUI.TAM_PROCESSO; i++ ) {
            // Notifica o processamento a cada 10 iterações
            if ( (i % 10 == 0) ) {
                notifyObservers(new Integer(i));
                setChanged();
            }
        }

        // Notifica fim do processo
        notifyObservers(new Boolean(true));
        setChanged();
    }

}