/**
 * @author paim 09/11/2011
 */
package paim.wingchun.testes.observadores;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class classeGUIruim extends JFrame {

    // Nro de iterações para o loop de simulação do processo
    public static int TAM_PROCESSO = 100000000;

    // Variaveis da interface
    private JProgressBar barradeprogresso;

    private JButton botaoOK;

    private JTextField texto;

    private JLabel label;

    /**
     * Construtor padrão, instancia e monta objetos da tela
     */
    protected classeGUIruim() {
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        botaoOK = new JButton("Processar");
        botaoOK.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                for ( int i = 0; i < classeGUIruim.TAM_PROCESSO; i++ ) {
                    // Este é um processo muito muito longo que congelaria nossa tela, oh!
                }
            }
        });

        texto = new JTextField();

        label = new JLabel("Programa Exemplo");
        label.setHorizontalAlignment(JTextField.CENTER);

        barradeprogresso = new JProgressBar();
        contentPane.add(label, BorderLayout.NORTH);
        contentPane.add(texto, BorderLayout.CENTER);
        contentPane.add(botaoOK, BorderLayout.WEST);
        contentPane.add(barradeprogresso, BorderLayout.SOUTH);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        pack();
    }

    /**
     * Instancia e mostra a tela do usuário
     * 
     * @param args
     *            Parâmetros da linha de execução
     */
    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        new classeGUIruim().show();
    }
}