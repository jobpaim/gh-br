package paim.wingchun.testes;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import com.jgoodies.looks.plastic.theme.Silver;
@SuppressWarnings({ "rawtypes", "unchecked" })

public class UIManagerList {

    private JTextField textoFiltro;

    private JTable t;

    private JPanel p;

    private JPanel panelFiltro;

    private JButton btLimpaFiltro;


    private JScrollPane jTabela;

    public static void main(String[] args) {
        new UIManagerList();
    }

    public UIManagerList() {
        JFrame f = new JFrame("UIManager properties default values");
        f.setContentPane(getPanel());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();        
        f.setVisible(true);

    }

    private JPanel getPanel() {
        if ( p == null ) {
            p = new JPanel();
            p.setLayout(new BorderLayout());
          p.setPreferredSize(new Dimension(600, 600));
            p.add(getpanelFiltro(), BorderLayout.NORTH);
            p.add(getjTabela());
            
        }
        return p;

    }
    
    private JScrollPane getjTabela() {
        if ( jTabela == null ) {
            jTabela = new JScrollPane();
            jTabela.setViewportView(getTabela());
        }
        return jTabela;
    }
    
    
    private JTable getTabela() {
        if ( t == null ) {
            
            PlasticLookAndFeel.setCurrentTheme(new Silver());
            try {
                UIManager.setLookAndFeel(new PlasticXPLookAndFeel());
            }
            catch ( UnsupportedLookAndFeelException e1 ) {
                e1.printStackTrace();
            }
            UIDefaults defaults = UIManager.getDefaults();
            
            
            System.out.println(defaults.size() + " properties deffined !");
            String[] colName = { "Key", "Value" };
            String[][] rowData = new String[defaults.size()][2];
            int i = 0;
            for ( Enumeration<?> e = defaults.keys(); e.hasMoreElements(); i++ ) {
                Object key = e.nextElement();
                rowData[i][0] = key.toString();
                rowData[i][1] = "" + defaults.get(key);
                System.out.println(rowData[i][0] + " ,, " + rowData[i][1]);
            }
            t = new JTable(rowData, colName);
            /*adiciona um sorter*/
            t.setRowSorter(new TableRowSorter<TableModel>(t.getModel()));
        }
        return t;
    }

    
   
    protected void filtrar() {
        String texto = getTextoFiltro().getText();
        if ( texto == null || texto.length() == 0 )
            limparFiltro();
        else {
            try {
                RowFilter<?, ?> rf = RowFilter.regexFilter(texto, Pattern.LITERAL
                        | Pattern.UNICODE_CASE , 0);

                ((TableRowSorter) getTabela().getRowSorter()).setRowFilter(rf);
            }
            catch ( PatternSyntaxException e ) {
                System.err.println("expressao mal formada para filtro");
            }
        }
    }

    protected void limparFiltro() {
        this.getTextoFiltro().setText("");
        ((TableRowSorter<?>) getTabela().getRowSorter()).setRowFilter(null);
    }

    protected JPanel getpanelFiltro() {
        if ( panelFiltro == null ) {
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
            gridBagConstraints.gridx = 2;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.insets = new Insets(0, 0, 0, 0);

            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.anchor = GridBagConstraints.NORTHWEST;
            gridBagConstraints1.gridx = 0;
            gridBagConstraints1.gridy = 0;
            gridBagConstraints1.insets = new Insets(0, 10, 0, 0);

            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.anchor = GridBagConstraints.NORTHWEST;
            gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints2.weightx = 1D;
            gridBagConstraints2.gridx = 1;
            gridBagConstraints2.gridy = 0;
            gridBagConstraints2.insets = new Insets(0, 10, 0, 0);

            panelFiltro = new JPanel();
            panelFiltro.setLayout(new GridBagLayout());
            panelFiltro.add(new JLabel("Filtro:"), gridBagConstraints1);
            panelFiltro.add(getTextoFiltro(), gridBagConstraints2);
            panelFiltro.add(getBtLimpaFiltro(), gridBagConstraints);
        }
        return panelFiltro;
    }

    protected JTextField getTextoFiltro() {
        if ( textoFiltro == null ) {
            textoFiltro = new JTextField();
            textoFiltro.addKeyListener(new KeyListener() {

                @Override
                public void keyTyped(KeyEvent e) {
                    SwingUtilities.invokeLater(new Runnable() {

                        @Override
                        public void run() {
                            filtrar();
                        }
                    });
                }

                @Override
                public void keyReleased(KeyEvent e) {}

                @Override
                public void keyPressed(KeyEvent e) {}
            });
            textoFiltro.setToolTipText("digite uma expressão para filtrar");
        }
        return textoFiltro;
    }

    protected JButton getBtLimpaFiltro() {
        if ( btLimpaFiltro == null ) {
            btLimpaFiltro = new JButton();
            btLimpaFiltro.setText("");
            btLimpaFiltro.setIcon(new ImageIcon(getClass().getResource("/imagens/ico_limpar.gif")));
            btLimpaFiltro.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    limparFiltro();
                }
            });
            Dimension d = new Dimension(20, 20);
            btLimpaFiltro.setMinimumSize(d);
            btLimpaFiltro.setMaximumSize(d);
            btLimpaFiltro.setPreferredSize(d);
            btLimpaFiltro.setToolTipText("limpa o filtro atual");
        }
        return btLimpaFiltro;
    }

}