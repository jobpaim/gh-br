/**
 * @author paim 02/05/2011
 */
package paim.wingchun.testes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author paim 02/05/2011
 */
public class testeObjeto {

    private void recebe(Object o) {
        if (List.class.isAssignableFrom(o.getClass())) {
            System.out.println("lista");
        }
        if (String.class.isAssignableFrom(o.getClass())) {

            System.out.println("string");
        }


        System.out.println("metodo");
    }

    public static void main(String[] args) {
        testeObjeto t = new testeObjeto();
        List<String> o1 = new ArrayList<>();
        String o2 = "caca";
        t.recebe(o1);
        t.recebe(o2);
    }
}
