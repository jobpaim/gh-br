/**
 * @author paim 17/05/2011
 */
package paim.wingchun.testes;

import java.awt.BorderLayout;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;

@SuppressWarnings("serial")
public class TesteSpinnerTime extends JFrame {

    public TesteSpinnerTime() {
        initializeUI();
        this.pack();
    }

    private void initializeUI() {
        setSize(100, 60);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        //
        // The following spinner model will have current date as its
        // value and using hour of day as the calendar field. The start
        // and end comparable has a null values which mean it doesn't
        // have minimum or maximum value.
        //
        SpinnerDateModel modehora = new SpinnerDateModel(new Date(), null, null, Calendar.HOUR);
        // SpinnerDateModel modemin = new SpinnerDateModel(new Date(), null, null, Calendar.MINUTE);

        JSpinner spinnerhora = new JSpinner(modehora);
        // JSpinner spinnermin = new JSpinner(modemin);
//        Dimension d = new Dimension(35, 20);
//        spinnerhora.setPreferredSize(d);
//        spinnerhora.setMinimumSize(d);
//        spinnerhora.setMaximumSize(d);
        // spinnermin.setPreferredSize(d);
        // spinnermin.setMinimumSize(d);
        // spinnermin.setMaximumSize(d);

        spinnerhora.addMouseWheelListener(new MouseWheelListener() {

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {

                // spinnerhora.getModel().setValue)

            }
        });

        //
        // Reformat the display of our spinner to show only the hour
        // and minute information part.
        //
        // JFormattedTextField textFieldhora = ((JSpinner.DefaultEditor) spinnerhora.getEditor()).getTextField();
        // JFormattedTextField textFieldmin = ((JSpinner.DefaultEditor) spinnermin.getEditor()).getTextField();
        JFormattedTextField textFieldhora = ((JSpinner.DefaultEditor) spinnerhora.getEditor()).getTextField();
        // JFormattedTextField textFieldmin = ((JSpinner.DefaultEditor) spinnermin.getEditor()).getTextField();

        DefaultFormatterFactory dfhoraf = (DefaultFormatterFactory) textFieldhora.getFormatterFactory();
        // DefaultFormatterFactory dfminf = (DefaultFormatterFactory) textFieldmin.getFormatterFactory();

        DateFormatter formatterhora = (DateFormatter) dfhoraf.getDefaultFormatter();
        // DateFormatter formattermin = (DateFormatter) dfminf.getDefaultFormatter();
        // formatter.setFormat(new SimpleDateFormat("hh:mm a"));
        formatterhora.setFormat(new SimpleDateFormat("HH:mm"));
        // formattermin.setFormat(new SimpleDateFormat("mm"));

        getContentPane().add(spinnerhora, BorderLayout.WEST);
        // getContentPane().add(spinnermin, BorderLayout.EAST);

        // textFieldhora.setEditable(false);
        // textFieldmin.setEditable(false);

        // textFieldhora.setText(null);
        // textFieldmin.setText(null);
        // TODO Auto-generated method stub

        // String date_string = modehora.getValue().toString();
        // System.out.println(date_string);
        // java.util.Date date=modehora.getDate();
        // System.out.print(date);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                new TesteSpinnerTime().setVisible(true);
            }
        });

    }
}