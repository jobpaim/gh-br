package paim.wingchun.testes;

class ExampleThread extends Thread {

    private volatile int testValue;

    public ExampleThread(String str) {
        super(str);
    }

    @Override
    public void run() {
        for ( int i = 0; i < 3; i++ ) {
            try {
                System.out.println(getName() + " : " + i);
                if ( getName().equals("Thread 1 ") ) {
                    testValue = 10;
                    System.out.println("\tTest Value : " + testValue);
                }
                if ( getName().equals("Thread 2 ") ) {
                    System.out.println("\tTest Value : " + testValue);
                }
                Thread.sleep(1000);
            }
            catch ( InterruptedException exception ) {
                exception.printStackTrace();
            }
        }
    }
}


/* http://www.javabeat.net/tips/169-volatile-keyword-in-java.html */
public class VolatileExample {

    public static void main(String args[]) {
        new ExampleThread("Thread 1 ").start();
        new ExampleThread("Thread 2 ").start();
    }
}