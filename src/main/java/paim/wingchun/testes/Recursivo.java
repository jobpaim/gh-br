package paim.wingchun.testes;


/** @author paim 19/09/2013
 * @since */
public class Recursivo {

    public Recursivo() {}

    static void recursivometodo(int iok) {


        for ( int i = 0; i < 3; i++ ) {
            System.out.println(iok + "     " + i);
            if ( i == iok ) {
                System.out.println("OK em " + iok + "     " + i);
                return;
            }
        }

        recursivometodo(--iok);

        throw new RuntimeException();

    }

    public static void main(String[] args) {
        recursivometodo(5);
    }

}
