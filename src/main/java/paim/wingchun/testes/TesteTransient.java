package paim.wingchun.testes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;

public class TesteTransient implements Serializable {

    private static final long serialVersionUID = -4878102112461568426L;

    public String a;

    public transient String b;

    public transient Socket s = new Socket();

    @Override
    public String toString() {
        return "a=" + a + "; b=" + b;
    }

    public static void main(String[] args) throws Exception {

        TesteTransient t1 = new TesteTransient();
        t1.a = "fiquei";
        t1.b = "sumi";
        System.out.println(t1);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(buffer);
        out.writeObject(t1);

        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buffer.toByteArray()));
        TesteTransient t2 = (TesteTransient) in.readObject();

        System.out.println(t2);
    }
}