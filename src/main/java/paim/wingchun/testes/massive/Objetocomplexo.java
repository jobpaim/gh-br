package paim.wingchun.testes.massive;


/**
 * @author paim 29/08/2013
 * @since
 */
public class Objetocomplexo {

    private int int1;

    private int int2;

    private int int3;

    private int int4;

    private int int5;

    private int int6;

    private int int7;

    private int int8;

    private int int9;

    private long long1;

    private long long2;

    private long long3;

    private long long4;

    private long long5;

    private long long6;

    private long long7;

    private long long8;

    private long long9;

    private String string1;

    private String string2;

    private String string3;

    private String string4;

    private String string5;

    private String string6;

    private String string7;

    private String string8;

    private String string9;

    private Object objetct1;

    private Object objetct2;

    private Object objetct3;

    private Object objetct4;

    private Object objetct5;

    private Object objetct6;

    private Object objetct7;

    private Object objetct8;

    private Object objetct9;

    private Object objetct11;

    private Object objetct12;

    private Object objetct13;

    private Object objetct14;

    private Object objetct15;

    private Object objetct16;

    private Object objetct17;

    private Object objetct18;

    private Object objetct19;

    public Objetocomplexo() {
        int1 = 1;
        int2 = 2;
        int3 = 3;
        int4 = 4;
        int5 = 5;
        int6 = 6;
        int7 = 7;
        int8 = 8;
        int9 = 9;

        long1 = 1L;
        long2 = 2L;
        long3 = 3L;
        long4 = 4L;
        long5 = 5L;
        long6 = 6L;
        long7 = 7L;
        long8 = 8L;
        long9 = 9L;

        string1 = "string1";
        string2 = "string2";
        string3 = "string3";
        string4 = "string4";
        string5 = "string5";
        string6 = "string6";
        string7 = "string7";
        string8 = "string8";
        string9 = "string9";

        objetct1 = new Object();
        objetct2 = new Object();
        objetct3 = new Object();
        objetct4 = new Object();
        objetct5 = new Object();
        objetct6 = new Object();
        objetct7 = new Object();
        objetct8 = new Object();
        objetct9 = new Object();

        objetct11 = new Object();
        objetct12 = new Object();
        objetct13 = new Object();
        objetct14 = new Object();
        objetct15 = new Object();
        objetct16 = new Object();
        objetct17 = new Object();
        objetct18 = new Object();
        objetct19 = new Object();

    }

    public int getInt1() {
        return this.int1;
    }

    public void setInt1(int int1) {
        this.int1 = int1;
    }

    public int getInt2() {
        return this.int2;
    }

    public void setInt2(int int2) {
        this.int2 = int2;
    }

    public int getInt3() {
        return this.int3;
    }

    public void setInt3(int int3) {
        this.int3 = int3;
    }

    public int getInt4() {
        return this.int4;
    }

    public void setInt4(int int4) {
        this.int4 = int4;
    }

    public int getInt5() {
        return this.int5;
    }

    public void setInt5(int int5) {
        this.int5 = int5;
    }

    public int getInt6() {
        return this.int6;
    }

    public void setInt6(int int6) {
        this.int6 = int6;
    }

    public int getInt7() {
        return this.int7;
    }

    public void setInt7(int int7) {
        this.int7 = int7;
    }

    public int getInt8() {
        return this.int8;
    }

    public void setInt8(int int8) {
        this.int8 = int8;
    }

    public int getInt9() {
        return this.int9;
    }

    public void setInt9(int int9) {
        this.int9 = int9;
    }

    public long getLong1() {
        return this.long1;
    }

    public void setLong1(long long1) {
        this.long1 = long1;
    }

    public long getLong2() {
        return this.long2;
    }

    public void setLong2(long long2) {
        this.long2 = long2;
    }

    public long getLong3() {
        return this.long3;
    }

    public void setLong3(long long3) {
        this.long3 = long3;
    }

    public long getLong4() {
        return this.long4;
    }

    public void setLong4(long long4) {
        this.long4 = long4;
    }

    public long getLong5() {
        return this.long5;
    }

    public void setLong5(long long5) {
        this.long5 = long5;
    }

    public long getLong6() {
        return this.long6;
    }

    public void setLong6(long long6) {
        this.long6 = long6;
    }

    public long getLong7() {
        return this.long7;
    }

    public void setLong7(long long7) {
        this.long7 = long7;
    }

    public long getLong8() {
        return this.long8;
    }

    public void setLong8(long long8) {
        this.long8 = long8;
    }

    public long getLong9() {
        return this.long9;
    }

    public void setLong9(long long9) {
        this.long9 = long9;
    }

    public String getString1() {
        return this.string1;
    }

    public void setString1(String string1) {
        this.string1 = string1;
    }

    public String getString2() {
        return this.string2;
    }

    public void setString2(String string2) {
        this.string2 = string2;
    }

    public String getString3() {
        return this.string3;
    }

    public void setString3(String string3) {
        this.string3 = string3;
    }

    public String getString4() {
        return this.string4;
    }

    public void setString4(String string4) {
        this.string4 = string4;
    }

    public String getString5() {
        return this.string5;
    }

    public void setString5(String string5) {
        this.string5 = string5;
    }

    public String getString6() {
        return this.string6;
    }

    public void setString6(String string6) {
        this.string6 = string6;
    }

    public String getString7() {
        return this.string7;
    }

    public void setString7(String string7) {
        this.string7 = string7;
    }

    public String getString8() {
        return this.string8;
    }

    public void setString8(String string8) {
        this.string8 = string8;
    }

    public String getString9() {
        return this.string9;
    }

    public void setString9(String string9) {
        this.string9 = string9;
    }

    public Object getObjetct1() {
        return this.objetct1;
    }

    public void setObjetct1(Object objetct1) {
        this.objetct1 = objetct1;
    }

    public Object getObjetct2() {
        return this.objetct2;
    }

    public void setObjetct2(Object objetct2) {
        this.objetct2 = objetct2;
    }

    public Object getObjetct3() {
        return this.objetct3;
    }

    public void setObjetct3(Object objetct3) {
        this.objetct3 = objetct3;
    }

    public Object getObjetct4() {
        return this.objetct4;
    }

    public void setObjetct4(Object objetct4) {
        this.objetct4 = objetct4;
    }

    public Object getObjetct5() {
        return this.objetct5;
    }

    public void setObjetct5(Object objetct5) {
        this.objetct5 = objetct5;
    }

    public Object getObjetct6() {
        return this.objetct6;
    }

    public void setObjetct6(Object objetct6) {
        this.objetct6 = objetct6;
    }

    public Object getObjetct7() {
        return this.objetct7;
    }

    public void setObjetct7(Object objetct7) {
        this.objetct7 = objetct7;
    }

    public Object getObjetct8() {
        return this.objetct8;
    }

    public void setObjetct8(Object objetct8) {
        this.objetct8 = objetct8;
    }

    public Object getObjetct9() {
        return this.objetct9;
    }

    public void setObjetct9(Object objetct9) {
        this.objetct9 = objetct9;
    }

    public Object getObjetct11() {
        return this.objetct11;
    }

    public void setObjetct11(Object objetct11) {
        this.objetct11 = objetct11;
    }

    public Object getObjetct12() {
        return this.objetct12;
    }

    public void setObjetct12(Object objetct12) {
        this.objetct12 = objetct12;
    }

    public Object getObjetct13() {
        return this.objetct13;
    }

    public void setObjetct13(Object objetct13) {
        this.objetct13 = objetct13;
    }

    public Object getObjetct14() {
        return this.objetct14;
    }

    public void setObjetct14(Object objetct14) {
        this.objetct14 = objetct14;
    }

    public Object getObjetct15() {
        return this.objetct15;
    }

    public void setObjetct15(Object objetct15) {
        this.objetct15 = objetct15;
    }

    public Object getObjetct16() {
        return this.objetct16;
    }

    public void setObjetct16(Object objetct16) {
        this.objetct16 = objetct16;
    }

    public Object getObjetct17() {
        return this.objetct17;
    }

    public void setObjetct17(Object objetct17) {
        this.objetct17 = objetct17;
    }

    public Object getObjetct18() {
        return this.objetct18;
    }

    public void setObjetct18(Object objetct18) {
        this.objetct18 = objetct18;
    }

    public Object getObjetct19() {
        return this.objetct19;
    }

    public void setObjetct19(Object objetct19) {
        this.objetct19 = objetct19;
    }

    // ##################################################

    public Long longa1() {
        return new Long(123);
    }

    public Long longa2() {
        return new Long(123);
    }

    public Long longa3() {
        return new Long(123);
    }

    public Long longa4() {
        return new Long(123);
    }

    public Long longa5() {
        return new Long(123);
    }

    public Long longa6() {
        return new Long(123);
    }

    public Long longa7() {
        return new Long(123);
    }

    public Long longa8() {
        return new Long(123);
    }

    public Long longa9() {
        return new Long(123);
    }

    public Long longa10() {
        return new Long(123);
    }

    public String string0() {
        return new String("lkjjljljlj");
    }

    public String string1() {
        return new String("lkjjljljlj");
    }

    public String string2() {
        return new String("lkjjljljlj");
    }

    public String string3() {
        return new String("lkjjljljlj");
    }

    public String string4() {
        return new String("lkjjljljlj");
    }

    public String string5() {
        return new String("lkjjljljlj");
    }

    public String string6() {
        return new String("lkjjljljlj");
    }

    public String string7() {
        return new String("lkjjljljlj");
    }

    public String string8() {
        return new String("lkjjljljlj");
    }

    public String string9() {
        return new String("lkjjljljlj");
    }

    public String string10() {
        return new String("lkjjljljlj");
    }

    public String string11() {
        return new String("lkjjljljlj");
    }

    public String string12() {
        return new String("lkjjljljlj");
    }

    public String string13() {
        return new String("lkjjljljlj");
    }

    public String string14() {
        return new String("lkjjljljlj");
    }

    public String string15() {
        return new String("lkjjljljlj");
    }

    public String string16() {
        return new String("lkjjljljlj");
    }

    public String string17() {
        return new String("lkjjljljlj");
    }

    public String string18() {
        return new String("lkjjljljlj");
    }

    public String string19() {
        return new String("lkjjljljlj");
    }

    public String string20() {
        return new String("lkjjljljlj");
    }

    public String string21() {
        return new String("lkjjljljlj");
    }

    public String string22() {
        return new String("lkjjljljlj");
    }

    public String string23() {
        return new String("lkjjljljlj");
    }

    public String string24() {
        return new String("lkjjljljlj");
    }

    public String string25() {
        return new String("lkjjljljlj");
    }

    public String string26() {
        return new String("lkjjljljlj");
    }

    public void uka(long n, Object[] oo, String... asa) {

        for ( int i = 0; i < n; i++ ) {
            System.out.println(oo[i].toString());
            System.out.println(asa[i]);
        }

    }

    public void ukaoka(long n, Object[] oo, String... asa) {

        for ( int i = 0; i < n; i++ ) {
            System.out.println(oo[i].toString());
            for ( int j = 0; j < i; j++ ) {
                System.out.println(asa[j]);
            }
        }
    }
}
