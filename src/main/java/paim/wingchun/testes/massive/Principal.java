package paim.wingchun.testes.massive;


import java.util.ArrayList;
import java.util.List;

/**
 * @author paim 29/08/2013
 * @since
 */
public class Principal {

    public Principal() {

    }

    List<Objetosimples> gera_simples(int l) {
        List<Objetosimples> lista = new ArrayList<>();
        for ( int i = 0; i < l; i++ ) {
            lista.add(new Objetosimples());
        }
        return lista;
    }

    List<Objetocomplexo> gera_complexos(int l) {
        List<Objetocomplexo> lista = new ArrayList<>();
        for ( int i = 0; i < l; i++ ) {
            lista.add(new Objetocomplexo());
        }
        return lista;
    }

    List<Objetocomplexoherdado> gera_herdado(int l) {
        List<Objetocomplexoherdado> lista = new ArrayList<>();
        for ( int i = 0; i < l; i++ ) {
            lista.add(new Objetocomplexoherdado());
        }
        return lista;
    }

    static void printInfoMemory() {
        int fator = 1024 * 1024;
        long total, free, diferenca;
        total = Runtime.getRuntime().totalMemory() / fator;
        free = Runtime.getRuntime().freeMemory() / fator;
        diferenca = (total - free);

        System.out.println("#######   ");
        System.out.println("### TOTAL MEMORY = " + total);
        System.out.println("### FREE MEMORY = " + free);
        System.out.println("### dif MEMORY = " + diferenca);
    }

    static void limpa() throws InterruptedException {
        Runtime.getRuntime().gc();
        Thread.sleep(5000);
        Runtime.getRuntime().gc();
    }
    /* preciso saber uso de memoria */
    public static void main(String[] args) throws InterruptedException {

        System.out.println("inicio");
        printInfoMemory();
        Principal p = new Principal();

        System.out.println("----------------------------simples");
        limpa();
        p.gera_simples(1000000);
        printInfoMemory();

        System.out.println("----------------------------complexos");
        limpa();
        p.gera_complexos(1000000);
        printInfoMemory();

        System.out.println("----------------------------herdado");
        limpa();
        p.gera_herdado(1000000);
        printInfoMemory();

        System.out.println("fim");
        limpa();
        printInfoMemory();

    }
}
