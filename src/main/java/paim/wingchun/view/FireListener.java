/**
 * @author temujin Mar 20, 2012
 */
package paim.wingchun.view;


import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventListener;

import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import paim.wingchun.controller.WC_Event;

/**
 * @author temujin Mar 20, 2012
 */
public class FireListener implements ActionListener, KeyListener, ListSelectionListener, MouseListener,
                TreeSelectionListener {

    private Enum<?> evento;

    private  Fireable<? extends EventListener>  fireable;

    private Object[] args;

    private Component component;

    private transient WC_Event evt;

    public FireListener(Fireable<? extends EventListener>  fireable, Enum<?> evento, Component component, Object... args) {
        this.evento = evento;
        this.fireable = fireable;
        this.component = component;
        this.args = args;
    }

    public FireListener(Fireable<? extends EventListener> fireable, Enum<?> evento, Component component) {
        this(fireable, evento, component, (Object[]) null);
    }

    public FireListener(Fireable<? extends EventListener> fireable, Enum<?> evento) {
        this.evento = evento;
        this.fireable = fireable;
    }

    public FireListener(Fireable<? extends EventListener> fireable, Enum<?> evento, Object... args) {
        this(fireable, evento, null, args);
    }

    @Deprecated
    public FireListener(Fireable<? extends EventListener> fireable, WC_Event evt) {
        this.fireable = fireable;
        this.evt = evt;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.fireable.fire(new WC_Event(evento, e, component, args));
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        this.fireable.fire(new WC_Event(evento, e, component, args));
    }

    @Override
    public void keyTyped(final KeyEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                FireListener.this.fireable.fire(new WC_Event(FireListener.this.evento, e,
                                FireListener.this.component, FireListener.this.args));
            }
        });
    }

    @Override
    public void keyPressed(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void valueChanged(ListSelectionEvent e) {
        this.fireable.fire(new WC_Event(this.evento, e, component, args));
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if ( e.getClickCount() == 2 )
            this.fireable.fire(new WC_Event(this.evento, e, component, args));
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

}
