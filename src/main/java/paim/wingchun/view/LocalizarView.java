package paim.wingchun.view;


import paim.wingchun.model.pojos.Pojo;

public class LocalizarView<P extends Pojo> extends VisualizarView<P> {

    private static final long serialVersionUID = 1L;

    public LocalizarView(Class<P> pClass) {
        super(pClass);
    }

    @Override
    public String getHelpId() {
        return "Localizar";
    }

    @Override
    protected String titleName() {
        return "Localizar";
    }

}
