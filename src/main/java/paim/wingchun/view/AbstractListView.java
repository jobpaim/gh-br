package paim.wingchun.view;


import java.util.List;

import javax.swing.JToolBar;

import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;

/** @author paim 29/03/2011 */
public abstract class AbstractListView<P extends Pojo> extends AbstractView<P> {

    private static final long serialVersionUID = 1L;

    protected AbstractListView(Class<P> pClass) {
        super(pClass);
    }

    // @Override
    // protected void initialize() {
    // Dimension d = new Dimension(500, 250);
    // this.setSize(d);
    // this.setMinimumSize(d);
    // this.setLocation(new Point(0, 0));
    // this.setResizable(true);
    // this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    // this.setTitle("não setou nome dessa bagaça");
    // this.setContentPane(getjInternalFrame());
    //
    // }

    @Override
    public void setEditando(boolean editando) {
        super.setEditando(editando);
        getjPanelBotoes().setEditando(editando);
    }

    @Override
    public void vCleanErros() {}

    @Override
    protected void v_setErros(List<Erro> erros) {}

    /** por padrao, nehuma toolbar eh usada
     *
     * @author temujin Jun 5, 2011
     * @return JToolBar */
    @Override
    protected JToolBar getToolBar() {
        return null;
    }
}
