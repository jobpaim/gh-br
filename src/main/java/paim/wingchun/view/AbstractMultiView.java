/**
 * @author paim 21/01/2011
 */
package paim.wingchun.view;

import java.util.List;

import javax.swing.JToolBar;

import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.componentes.toolbar.JExtraTools;
import paim.wingchun.view.componentes.toolbar.JNavegador;
import paim.wingchun.view.componentes.toolbar.JTools;

/**
 * @author paim 21/01/2011
 */
@SuppressWarnings("serial")
@Deprecated
public abstract class AbstractMultiView<P extends Pojo> extends AbstractView<P> {

    protected AbstractMultiView(Class<P> pClass) {
        super(pClass);
    }

    /* #################### */

    @Override
    protected Object v_getObjeto() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }



    @Override
    public void setEditando(boolean editando) {
        super.setEditando(editando);
    }

    @Override
    public void vCleanErros() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void v_setErros(List<Erro> erros) {
        // TODO Auto-generated method stub
    }


    public JTools JNavegadorTools2() {
        return (JTools) getComponente(getpClass().getSimpleName() + "."
                + JTools.class.getSimpleName());
    }

    public JNavegador getNavegador2() {
        return (JNavegador) getComponente(getpClass().getSimpleName() + "." + JNavegador.class.getSimpleName());
    }


    private JNavegador jNavegador;

    public JNavegador getNavegador() {
        if ( jNavegador == null ) {
            jNavegador = new JNavegador(this.getTitle());
            jNavegador.add(getNavegadorTools());
        }
        return jNavegador;
    }

    private JTools jTools;

    public JTools getNavegadorTools() {
        if ( jTools == null ) {
            jTools = new JTools();
        }
        return jTools;
    }
    private JExtraTools jExtraTools;
    public JExtraTools getToolsExtra() {
        if ( jExtraTools == null ) {
            jExtraTools = new JExtraTools();
        }
        return jExtraTools;
    }

    @Override
    protected JToolBar getToolBar() {
        return getNavegador();
    }
}
