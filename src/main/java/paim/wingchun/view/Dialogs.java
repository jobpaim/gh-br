package paim.wingchun.view;

import java.awt.Component;
import java.util.Collection;

import javax.swing.Icon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import paim.wingchun.app.WC_App;
import paim.wingchun.app.WC_Strings;
import paim.wingchun.model.modelos.ErroModel.Level;
import paim.wingchun.model.pojos.Erro;

public class Dialogs {

    public static void menssagem(String msg, String titulo) {
        menssagem((Component) WC_App.getInstancia().getVisaoApp(), msg, titulo);
    }

    public static void menssagem(Component parent, String msg, String titulo) {
        JOptionPane.showMessageDialog(parent, msg, titulo, JOptionPane.INFORMATION_MESSAGE);
    }

    public static void erro(String msg, String titulo) {
        erro((Component) WC_App.getInstancia().getVisaoApp(), msg, titulo);
    }

    public static void erro(Component parent, String msg, String titulo) {
        JOptionPane.showMessageDialog(parent, msg, titulo, JOptionPane.ERROR_MESSAGE);
    }

    public static boolean pergunta(Component parent, String titulo) {
        String msg = "";
        if ( parent instanceof JInternalFrame )
            msg = ((JInternalFrame) parent).getTitle();
        return pergunta(parent, msg, titulo);
    }

    public static boolean pergunta(Component parent, String msg, String titulo) {
        switch ( JOptionPane.showConfirmDialog(parent, msg, titulo, JOptionPane.YES_NO_OPTION) ) {
            case JOptionPane.YES_OPTION:
                return true;
            case JOptionPane.NO_OPTION:
            default:
                return false;
        }
    }

    /**
     * ESC sai fora
     *
     * @author paim 24/05/2011
     * @param parent
     * @param erro
     * @param icon
     *            void
     */
    public static void showInternalErro(Component parent, Erro erro) {
        /* prepara tipo mensagem */
        int tipo;
        if ( erro.getLevel().getValor() > Level.ERRO.getValor() )
            tipo = JOptionPane.ERROR_MESSAGE;
        else if ( erro.getLevel() == Level.ERRO )
            tipo = JOptionPane.WARNING_MESSAGE;
        else if ( erro.getLevel() == Level.AVISO )
            tipo = JOptionPane.QUESTION_MESSAGE;
        else
            tipo = JOptionPane.INFORMATION_MESSAGE;
        /* mostra mensagem */
        JOptionPane.showInternalOptionDialog(parent, erro.getMensagem(), erro.getNome(), JOptionPane.DEFAULT_OPTION,
                tipo, null, new Object[] {}, null);

    }

    /**
     * ESC sai fora
     *
     * @author paim 24/05/2011
     * @param parent
     * @param erro
     * @param icon
     *            void
     */
    public static void showInternalErro(JInternalFrame parent, Erro erro, Icon icon) {
        /* prepara tipo mensagem */
        int tipo;
        if ( erro.getLevel().getValor() > Level.ERRO.getValor() )
            tipo = JOptionPane.ERROR_MESSAGE;
        else if ( erro.getLevel() == Level.ERRO )
            tipo = JOptionPane.WARNING_MESSAGE;
        else if ( erro.getLevel() == Level.AVISO )
            tipo = JOptionPane.QUESTION_MESSAGE;
        else
            tipo = JOptionPane.INFORMATION_MESSAGE;
        /* mostra mensagem */
        JOptionPane.showInternalOptionDialog(parent, erro.getMensagem(), erro.getNome(), JOptionPane.DEFAULT_OPTION,
                tipo, icon, new Object[] {}, null);

    }

    public static boolean showConfirmErros(Component parent, Collection<Erro> impeditivos) {
        String header = "Existe(m) erro(s) que impede(m) a persistencia dos dados.";
        String footer = "Descartar as alterações?";
        return showConfirmErros(parent, header, footer, impeditivos);
    }

    public static boolean showConfirmErros(Component parent, String header, String footer, Collection<Erro> impeditivos) {
        String title = "";
        if ( parent instanceof JInternalFrame )
            title = ((JInternalFrame) parent).getTitle();

        StringBuilder message = new StringBuilder();
        message.append(header);
        message.append("\n\n");
        message.append(WC_Strings.formataErros(impeditivos));
        message.append("\n");
        message.append(footer);

        switch ( JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION) ) {
            case JOptionPane.YES_OPTION:
                return true;
            case JOptionPane.NO_OPTION:
                return false;
        }
        return true;
    }

    public static void showMessageErros(Component parent, Collection<Erro> impeditivos) {
        String title = "";
        if ( parent instanceof JInternalFrame )
            title = ((JInternalFrame) parent).getTitle();

        StringBuilder message = new StringBuilder();
        message.append("Existem erros que impedem a exclusão");
        message.append("\n\n");
        message.append(WC_Strings.formataErros(impeditivos));
        message.append("\n");
        erro(parent, message.toString(), title);
    }

    public static boolean perguntaMessageErros(Component parent, Collection<Erro> impeditivos) {
        String title = "";
        if ( parent instanceof JInternalFrame )
            title = ((JInternalFrame) parent).getTitle();

        StringBuilder message = new StringBuilder();
        message.append("Existem erros que impedem a exclusão");
        message.append("\n\n");
        message.append(WC_Strings.formataErros(impeditivos));
        message.append("\n");
        return pergunta(parent, message.toString(), title);
    }

}
