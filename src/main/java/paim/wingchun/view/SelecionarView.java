package paim.wingchun.view;


import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.componentes.table.WC_JTable;

/** @author temujin Jun 5, 2011 */
public class SelecionarView<P extends Pojo> extends AbstractTableListView<P> {

    private static final long serialVersionUID = 1L;

    private int selectionMode;

    private boolean hasSequencial;

    private boolean hasSeletor;

    private boolean hasConstrutor;

    private JCheckBox jCheckBox;

    private JPanel jPanel;

    private WC_JTable<P> table;

    public SelecionarView(Class<P> pClass) {
        super(pClass);
    }

    @Deprecated
    public SelecionarView(Class<P> pClass, int selectionMode, boolean hasSequencial, boolean hasSeletor,
                    boolean hasConstrutor) {
        super(pClass);
        this.selectionMode = selectionMode;
        this.hasSequencial = hasSequencial;
        this.hasSeletor = hasSeletor;
        this.hasConstrutor = hasConstrutor;
    }

    @Override
    public String getHelpId() {
        return "Selecionar";
    }

    @Override
    protected void initialize() {
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        setSequencial(true);
        setSeletor(true);
        setConstrutor(false);

        super.initialize();
        this.setTitle("Selecionar " + Reflections.getRotuloS(getpClass()));
    }

    @Override
    public WC_JTable<P> getTable() {
        if ( table == null ) {
            table = new WC_JTable<P>(this.getpClass(), selectionMode, hasSequencial, hasSeletor, hasConstrutor);
        }
        return table;
    }

    @Override
    protected Object v_getObjeto() throws Exception {
        /* como nao ha alteracao de objeto, retorna a propria referencia */
        return getObjetoReferencia();
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        /* joga objetos na tabela */
        getTable().setObjetos((List<?>) objeto);
        return objeto;
    }

    public JCheckBox getJCheckBox() {
        if ( jCheckBox == null ) {
            jCheckBox = new JCheckBox("select/unselect All", false);
        }
        return jCheckBox;
    }

    @Override
    protected JPanel getJPanelUsuario() {
        if ( jPanel == null ) {
            jPanel = super.getJPanelUsuario();
            if ( isMultiSelecao() )
                jPanel.add(getJCheckBox(), BorderLayout.PAGE_START);
        }
        return jPanel;
    }

    final public boolean isMultiSelecao() {
        if ( ListSelectionModel.SINGLE_SELECTION == selectionMode && !hasSeletor )
            return false;
        return true;
    }

    protected final void setSeletor(boolean b) {
        this.hasSeletor = b;
    }

    protected final void setConstrutor(boolean b) {
        this.hasConstrutor = b;
    }

    protected final void setSequencial(boolean b) {
        this.hasSequencial = b;
    }

    protected final void setSelectionMode(int i) {
        this.selectionMode = i;
    }

}
