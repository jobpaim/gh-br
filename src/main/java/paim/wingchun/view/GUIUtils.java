package paim.wingchun.view;


import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.net.URL;
import java.util.HashMap;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.border.TitledBorder;

import static paim.wingchun.app.WC_Properties.getProperty;

/**
 * @author paim 18/01/2011
 */
public class GUIUtils {

    public static final String EMPTY_ICON = "/imagens/ico_vazio.gif";

    public static final Color PAINEL$BORDA$COR = new Color(10, 36, 106);

    public static final Font PAINEL$BORDA$FONTE = new Font("Dialog", Font.BOLD, 12);

    public static TitledBorder getBordaPadrao(String nome) {
        TitledBorder titledBorder = BorderFactory.createTitledBorder(null, nome, TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, PAINEL$BORDA$FONTE, PAINEL$BORDA$COR);
        return titledBorder;
    }

    /**
     * tenta encontrar a url do icone especificado, se nao traz o vazio
     * 
     * @author paim 10/05/2011
     * @param IconKey
     * @return URL do icone
     */
    public static URL getURLIcon(String IconKey) {
        String nome = getProperty(IconKey, EMPTY_ICON);
        URL url = GUIUtils.class.getResource(nome);
        return url;
    }

    public static ImageIcon getImageIcon(String IconKey) {
        URL url = getURLIcon(IconKey);
        if ( url == null )
            return new ImageIcon(EMPTY_ICON);
        return new ImageIcon(url);
    }

    public static AbstractView<?> getVisao(JComponent jComponent) {
        Component component = jComponent;
        do {
            if ( component instanceof AbstractView )
                return (AbstractView<?>) component;
            component = (Component) component.getParent();
        }
        while ( component != null );

        return null;
    }

    public static Cursor getCursorVisao(JComponent jcomponente) {
        AbstractView<?> visao = getVisao(jcomponente);
        if ( visao != null )
            return visao.getCursor();
        return null;
    }

    /** Pilha utilizada para armazenar os cursores utilizados */
    private static HashMap<Component, Stack<Cursor>> cursores = new HashMap<Component, Stack<Cursor>>();

    /** Altera o cursor do componente para o cursor informado e empilha a alteração para poder ser desfeita */
    public static void setCursor(Component componente, Cursor cursor) {

        Stack<Cursor> stackCursores = cursores.get(componente);
        if ( stackCursores == null ) {
            stackCursores = new Stack<Cursor>();
            cursores.put(componente, stackCursores);
        }
        stackCursores.push(componente.getCursor());
        componente.setCursor(cursor);
    }

    /** Retorna o cursor do componente para o anteriormente utilizado */
    public static void restauraCursor(Component componente) {
        /* por default, restaura para o cursor Padrao */
        Cursor cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
        Stack<Cursor> stackCursores = cursores.get(componente);
        if ( stackCursores != null )
            if ( stackCursores.size() > 0 )
                cursor = stackCursores.pop();
        componente.setCursor(cursor);
    }

    /**
     * Retorna o componente visual correspondente ao nome informado. O componente visual deve ter tido sua propriedade setName()
     * setada em um momento anterior. Utiliza recursao para percorrer todos os sub-componentes.
     * 
     * @param root
     *            - componente conteiner raiz, onde começara a busca.
     * @param nomeComponente
     */
    public static JComponent getComponenteRecursivo(JComponent root, String nomeComponente) {

        Component[] componentes = ((Container) root).getComponents();
        JComponent r = null;

        for ( Component c : componentes ) {
            if ( c instanceof JComponent ) {
                if ( ((JComponent) c).getComponentCount() > 0 ) {
                    r = getComponenteRecursivo((JComponent) c, nomeComponente);
                }

                if ( c.getName() != null ) {
                    if ( c.getName().equals(nomeComponente) )
                        return (JComponent) c;
                }
                if ( r != null )
                    return r;
            }
        }

        return null;
    }
}
