package paim.wingchun.view;


import java.util.List;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.componentes.panel.JPanelButtons;
import paim.wingchun.view.componentes.table.WC_JTable;

/** @author paim 13/02/2014
 * @since */
public class VisualizarView<P extends Pojo> extends AbstractTableListView<P> {

    private WC_JTable<P> table;

    private JPanelButtons panelBotoes;

    private static final long serialVersionUID = 1L;

    public VisualizarView(Class<P> pClass) {
        super(pClass);
    }

    @Override
    public String getHelpId() {
        return "Visualizar";
    }

    protected String titleName() {
        return "Visualizar";
    }

    @Override
    protected void initialize() {
        super.initialize();
        this.setTitle(titleName() + " " + Reflections.getRotuloS(getpClass()));
    }

    @Override
    public WC_JTable<P> getTable() {
        if ( table == null ) {
            table = new WC_JTable<P>(this.getpClass());
            // table = new WC_JTable<P>();
        }
        return table;
    }

    @Override
    protected Object v_getObjeto() throws Exception {
        List<Pojo> lista = getTable().getObjetos();
        return lista;
    }

    @Override
    protected Object v_setObjeto(Object objeto) throws Exception {
        getTable().setObjetos((List<?>) objeto);
        return objeto;
    }

    @Override
    public JPanelButtons getjPanelBotoes() {
        if ( panelBotoes == null )
            panelBotoes = new JPanelButtons(JPanelButtons.ALL ^ JPanelButtons.BT_CANCELAR);

        return panelBotoes;
    }
}
