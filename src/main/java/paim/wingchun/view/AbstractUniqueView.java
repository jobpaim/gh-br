package paim.wingchun.view;


import java.util.List;

import javax.swing.JToolBar;

import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;

@SuppressWarnings("serial")
public abstract class AbstractUniqueView<P extends Pojo> extends AbstractView<P> {

    protected AbstractUniqueView() {
        super();
    }

    @Override
    public String getHelpId() {
        throw new RuntimeException();
    }

    @Override
    protected void initialize() {
        // Dimension d = new Dimension(800, 500);
        // this.setSize(d);
        // this.setMinimumSize(d);
        // this.setLocation(new Point(0, 0));
        // this.setResizable(true);
        // this.setTitle("noname");
        // this.setContentPane(getjInternalFrame());
        // // this.setFrameIcon(getIconJInternalFrame());
        super.initialize();
        this.pack();
    }

    @Override
    public void vCleanErros() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void v_setErros(List<Erro> erros) {
        // TODO Auto-generated method stub

    }

    @Override
    protected Object v_getObjeto() throws Exception {
        /* o objeto de referencia ja recebeu as alteracoes via controlador */
        return getObjetoReferencia();
    }

    /** por padrao, nehuma toolbar eh usada
     *
     * @author temujin */
    @Override
    protected JToolBar getToolBar() {
        return null;
    }
}
