package paim.wingchun.view.componentes;

import javax.swing.ImageIcon;

import paim.wingchun.model.pojos.Erro;
import paim.wingchun.swing.JLabelIcon;
import paim.wingchun.view.GUIUtils;

/**
 * @author paim 19/05/2011
 */
public class WC_Label extends JLabelIcon {

    private static final long serialVersionUID = 1L;

    public static final ImageIcon ico$vazio = GUIUtils.getImageIcon("icon.empty");

    public static final ImageIcon ico$aviso = GUIUtils.getImageIcon("icon.erro.aviso");

    public static final ImageIcon ico$alerta = GUIUtils.getImageIcon("icon.erro.alerta");

    public static final ImageIcon ico$erro = GUIUtils.getImageIcon("icon.erro.erro");

    public static final ImageIcon ico$erro$impeditivo = GUIUtils.getImageIcon("icon.erro.impeditivo");

    public static final ImageIcon ico$erro$prioritario = GUIUtils.getImageIcon("icon.erro.prioritario");

    private Erro erro;

    public WC_Label() {
        super();
        /* icone vazio padrao, coloco o icone apenas para reservar o espaco */
        setIcon(ico$vazio);
    }

    public Erro getErro() {
        return this.erro;
    }
    
    public void setErro(Erro erro) {
        this.erro = erro;

        switch ( erro.getLevel() ) {
            case AVISO:
                setIcon(ico$aviso);
            break;
            case ERRO:
                setIcon(ico$erro);
            break;
            case ERRO$IMPEDITIVO:
                setIcon(ico$erro$impeditivo);
            break;
            case ERRO$PRIORITARIO:
                setIcon(ico$erro$prioritario);
            break;
            case NENHUM:
            default:
                setIcon(ico$vazio);
            break;
        }
    }

    public void cleanErro() {
        this.erro = null;
        setIcon(ico$vazio);
    }

}
