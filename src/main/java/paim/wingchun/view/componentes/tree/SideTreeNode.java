/**
 * @author temujin Sep 16, 2011
 */
package paim.wingchun.view.componentes.tree;

import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.pojos.Pojo;

/**
 * @author temujin Sep 16, 2011
 */
@Deprecated
public class SideTreeNode extends DefaultMutableTreeNode {

    private static final long serialVersionUID = 1L;

    protected Pojo objeto;

    protected Class<? extends Pojo> classe;

    // private SideTreeNode() {
    // super();
    // }

    // private SideTreeNode(Object userObject, boolean allowsChildren) {
    // super(userObject, allowsChildren);
    // // TODO Auto-generated constructor stub
    // }
    //
    // private SideTreeNode(Object userObject) {
    // super(userObject);
    // }

    public SideTreeNode(Class<? extends Pojo> classe) {
        super(Reflections.getRotuloS(classe));
        this.classe = classe;
    }

    public SideTreeNode(String userObject, Pojo pojo) {
        super(userObject);
        this.objeto = pojo;
        this.classe = pojo.getClass();
    }

    public Class<? extends Pojo> getClasse() {
        return this.classe;
    }

    public Pojo getObjeto() {
        return this.objeto;
    }

    public boolean isNodeObjeto() {
        if ( objeto != null )
            return true;
        return false;
    }

    /** swap o nodo de objeto para classe e
     * @author paim 07/12/2011
     * @throws Exception
     * @return POJO, o objeto do nodo antes do swap
     */
    public Pojo swapNodoClasse() throws Exception {
        if ( !isNodeObjeto() )
            throw new Exception("Nodo nï¿½o eh objeto");
        Pojo o = objeto;
        objeto = null;
        return o;
    }

    /**swap o nodo de classe para objeto
     * @author paim 07/12/2011
     * @param objeto
     * @throws Exception
     * void
     */
    public void swapNodoObjeto(Pojo objeto) throws Exception {
        if ( isNodeObjeto() )
            throw new Exception("Nodo nï¿½o eh classe");

        this.objeto = objeto;
    }

    /**
     * adiciona nodo a este nodo
     * 
     * @author temujin Sep 11, 2011
     * @param pojo
     *            void
     * @see DefaultMutableTreeNode#add(MutableTreeNode)
     */
    public void add(Pojo pojo) {
        SideTreeNode node = new SideTreeNode(pojo.toString(), pojo);
        this.add(node);
    }

    /**
     * adiciona nodos a este nodo.
     * 
     * @author temujin Sep 11, 2011
     * @param lista
     *            void
     * @see DefaultMutableTreeNode#add(MutableTreeNode)
     */
    public void addAll(List<? extends Pojo> lista) {
        if ( lista.isEmpty() )
            return;

        SideTreeNode node;
        for ( Pojo pojo : lista ) {
            node = new SideTreeNode(pojo.toString(), pojo);
            this.add(node);
        }

    }

    @Deprecated
    public boolean hasChildren() {
        return getChildCount() > 0;
    }

}
