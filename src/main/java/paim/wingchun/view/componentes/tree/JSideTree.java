/**
 * @author temujin Sep 18, 2011
 */
package paim.wingchun.view.componentes.tree;

import javax.swing.JPopupMenu;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreeModel;

import paim.wingchun.controller.WC_Event;

/**
 * @author temujin Sep 18, 2011
 */
public class JSideTree extends AbstractSideTree {

    private static final long serialVersionUID = 1L;

    public JSideTree() {}

    public JSideTree(TreeModel newModel) {
        super(newModel);
    }


    @Override
    protected JPopupMenu getPopupMenu(SideTreeNode node) {
        return null;
    }

    @Override
    public void fire(WC_Event e) {
        TreeListener[] listeners = listenerList.getListeners(TreeListener.class);

        for ( TreeListener listener : listeners ) {
            switch ( (Eventos) e.evento() ) {
                case ADD:
                    listener.add();
                break;

                default:
                break;
            }
        }
    }

    protected enum Eventos {
        ADD;
    }

    @Override
    protected void nodoSelecionado(TreeSelectionEvent e, SideTreeNode node) {
        // TODO Auto-generated method stub

    }
}


interface TreeListener extends SideTreeListener {

    public void add();
}
