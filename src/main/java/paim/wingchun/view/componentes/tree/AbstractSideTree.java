/**
 * @author temujin Aug 30, 2011
 */
package paim.wingchun.view.componentes.tree;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.event.EventListenerList;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import paim.wingchun.view.FireListener;
import paim.wingchun.view.Fireable;

import static paim.wingchun.app.WC_Properties.getProperty;
import static paim.wingchun.view.GUIUtils.getImageIcon;

/**
 * @author temujin Aug 30, 2011
 */
@Deprecated
public abstract class AbstractSideTree extends JTree implements MouseListener, TreeSelectionListener,
        Fireable<SideTreeListener> {

    private static final long serialVersionUID = 1L;

    public AbstractSideTree() {
        super();
    }

    public AbstractSideTree(Object[] value) {
        super(value);
    }

    public AbstractSideTree(Vector<?> value) {
        super(value);
    }

    public AbstractSideTree(Hashtable<?, ?> value) {
        super(value);
    }

    public AbstractSideTree(TreeNode root) {
        super(root);
    }

    public AbstractSideTree(TreeModel newModel) {
        super(newModel);
        addMouseListener(this);
        addTreeSelectionListener(this);
    }

    public AbstractSideTree(TreeNode root, boolean asksAllowsChildren) {
        super(root, asksAllowsChildren);
    }

   

    @Override
    public EventListenerList getListenersList() {
        return this.listenerList;
    }

    protected abstract JPopupMenu getPopupMenu(SideTreeNode node);

    protected JMenuItem makeMenuItem(Enum<?> evento, boolean enable) {
        String texto = getProperty("tooltip.tree." + evento.name().toLowerCase());
        ImageIcon imageIcon = getImageIcon("icon.tree." + evento.name().toLowerCase());
        JMenuItem menuItem = new JMenuItem(texto, imageIcon);
        menuItem.setToolTipText(texto);
        menuItem.setEnabled(enable);
        menuItem.addActionListener(new FireListener(this, evento));

        return menuItem;
    }

    private void menuShow(MouseEvent e) {
        /* se nao for botao direita cai fora */
        if ( e.getButton() != MouseEvent.BUTTON3 )
            return;

        /* pega a arvore */
        JTree jTree = (JTree) e.getComponent();
        /* o caminho para o nodo no click */
        TreePath treePath = jTree.getPathForLocation(e.getX(), e.getY());
        /* seleciona o nodo conforme o caminho */
        jTree.setSelectionPath(treePath);

        /* se nao for SideTreeNode cai fora */
        if ( !SideTreeNode.class.isAssignableFrom(jTree.getLastSelectedPathComponent().getClass()) )
            return;

        SideTreeNode nodoSelecionado = (SideTreeNode) jTree.getLastSelectedPathComponent();

        if ( e.isPopupTrigger() && nodoSelecionado != null ) {
            getPopupMenu(nodoSelecionado).show(jTree, e.getX(), e.getY());
        }
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        JTree jTree = (JTree) e.getSource();

        DefaultMutableTreeNode nodoSelecionado = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();

        /* if nothing is selected */
        if ( nodoSelecionado != null && nodoSelecionado instanceof SideTreeNode )
            nodoSelecionado(e, (SideTreeNode) nodoSelecionado);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // System.out.println("mouseClicked");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        menuShow(e);
        // System.out.println("mousePressed");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        menuShow(e);
        // System.out.println("mouseReleased");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        /* quando entra no painel da arvore */
        // System.out.println("mouseEntered");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        /* quando sai do painel da arvore */
        // System.out.println("mouseExited");
    }

    /**
     * @author temujin Feb 6, 2012
     * @param e2
     * @param e
     *            void
     */
    protected abstract void nodoSelecionado(TreeSelectionEvent e, SideTreeNode node);

}
