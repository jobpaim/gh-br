/**
 * @author paim
 * 04/11/2011
 *
 */
package paim.wingchun.view.componentes.table;

import java.util.EventListener;


/**
 * @author paim
 * 04/11/2011
 *
 */
public interface CarregarListener extends EventListener{

    public void carregar() ;

    public void selecionar();

}
