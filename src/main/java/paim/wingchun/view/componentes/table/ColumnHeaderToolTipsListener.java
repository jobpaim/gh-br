package paim.wingchun.view.componentes.table;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class ColumnHeaderToolTipsListener implements MouseMotionListener {

    /** Coluna atual para o qual o tooltip esta sendo mostrado */
    TableColumn colunaAtual;

    /** Mapeia as colunas da tabela com os tooltips */
    Map<TableColumn, String> tips = new HashMap<TableColumn, String>();

    /** Se o tooltip e nulo, remove qualquer texto de tooltip. */
    public void setToolTip(TableColumn coluna, String tooltip) {
        if ( tooltip == null )
            tips.remove(coluna);
        else
            tips.put(coluna, tooltip);
    }

    /** Listener de movimento do mouse */
    @Override
    public void mouseMoved(MouseEvent e) {
        TableColumn col = null;
        JTableHeader header = (JTableHeader) e.getSource();
        JTable table = header.getTable();
        if ( table != null ) {

            TableColumnModel colModel = table.getColumnModel();
            int vColIndex = colModel.getColumnIndexAtX(e.getX());

            /* Return if not clicked on any column header */
            if ( vColIndex >= 0 )
                col = colModel.getColumn(vColIndex);
        }

        if ( col != colunaAtual ) {
            header.setToolTipText((String) tips.get(col));
            colunaAtual = col;
        }
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent) */
    @Override
    public void mouseDragged(MouseEvent e) {}
}