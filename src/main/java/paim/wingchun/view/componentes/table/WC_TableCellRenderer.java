package paim.wingchun.view.componentes.table;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class WC_TableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    /**render que alinha a direita a coluna*/
    public WC_TableCellRenderer() {
        super();
        setHorizontalAlignment(SwingConstants.RIGHT);
    }
}