package paim.wingchun.view.componentes.table;

import java.util.Comparator;
import java.util.logging.Level;

import javax.swing.table.TableRowSorter;

import paim.wingchun.model.pojos.Pojo;

public class WC_TableRowSorter extends TableRowSorter<WC_TableModel> {

    public WC_TableRowSorter(WC_TableModel model) {
        super(model);

        /* colunao que nao serao sort */
        if ( model.haveSequencial() )
            setSortable(model.getSequencialColumn(), false);

        if ( model.haveSelector() ) {
            /* for ( Integer col : model.getSelectorColumns() ) */
            setSortable(model.getSelectorColumn(), false);
            setComparator(model.getSelectorColumn(), null);
        }

        if ( model.haveConstructor() )
            /* for ( Integer col : model.getConstrutorColumns() ) */
            setSortable(model.getConstructorColumn(), false);

        // setComparator(1, getComparatorExample());
        // setComparator(1, null);
        // setComparator(3, getComparatorExamplePOJO());
    }

    @Deprecated
    private Comparator<String> getComparatorExample() {
        return new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                try {
                    return o1.compareTo(o2);
                }
                catch ( Exception e ) {
                    System.out.println(Level.INFO.toString() + e.getClass().getCanonicalName() + "\t"
                            + e.getStackTrace()[0].toString());
                    return 0;
                }

            }

        };
    }

    @Deprecated
    private Comparator<? extends Pojo> getComparatorExamplePOJO() {
        return new Comparator<Pojo>() {

            @Override
            public int compare(Pojo o1, Pojo o2) {
                try {
                    return o1.compareTo(o2);
                }
                catch ( Exception e ) {
                    System.out.println(e.getClass().getCanonicalName() + "\t" + e.getStackTrace()[0].toString());
                    return 0;
                }
            }
        };
    }

}