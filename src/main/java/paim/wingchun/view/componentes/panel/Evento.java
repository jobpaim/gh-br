package paim.wingchun.view.componentes.panel;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/** @author paim 12/02/2014
 * @since */
@Target({ METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Evento {

    int evento();
}
