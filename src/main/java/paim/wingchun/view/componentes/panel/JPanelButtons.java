package paim.wingchun.view.componentes.panel;


import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import paim.wingchun.controller.WC_Event;
import paim.wingchun.view.Editable;
import paim.wingchun.view.FireListener;
import paim.wingchun.view.Fireable;
import paim.wingchun.view.componentes.botoes.JButtonAjudar;
import paim.wingchun.view.componentes.botoes.JButtonCancelar;
import paim.wingchun.view.componentes.botoes.JButtonConfirmar;
import paim.wingchun.view.componentes.botoes.JButtonFechar;

/** @author paim 28/03/2011 */

// FIXME ajustar estado quando com java8 e ajustar esse fire tambem
public class JPanelButtons extends JPanel implements Fireable<PanelButtonsListener>, Editable {

    private static final long serialVersionUID = 1L;

    protected enum Eventos {
        CONFIRMAR, CANCELAR, FECHAR, AJUDAR
    }

    private final static int NONE = 0x0;

    public final static int BT_CONFIRMAR = 0x1;

    public final static int BT_CANCELAR = 0x2;

    public final static int BT_FECHAR = 0x4;

    public final static int BT_AJUDA = 0x8;

    public final static int ALL = BT_CONFIRMAR | BT_CANCELAR | BT_FECHAR/* | BT_AJUDA */;

    @Deprecated
    private final static int ALLXX = BT_CONFIRMAR | BT_CANCELAR;

    @SuppressWarnings("unused")
    @Deprecated
    private final static int BT_ = ALL ^ BT_CANCELAR;

    @SuppressWarnings("unused")
    @Deprecated
    private final static int BT_2 = ALL & BT_AJUDA;

    @SuppressWarnings("unused")
    @Deprecated
    private final static int BT_3 = ALLXX & BT_AJUDA;

    private final int botoes;

    private JButton btAjudar;

    private JButton btConfirmar;

    private JButton btCancelar;

    private JButton btFechar;

    private boolean editando;

    public JPanelButtons() {
        this(ALL);
    }

    public JPanelButtons(int botoes) {
        super();
        this.botoes = botoes;
        initialize();
    }

    private void initialize() {
        this.setLayout(new FlowLayout(FlowLayout.RIGHT));

        /* se tem botao confirmar entao instancie-o */
        if ( BT_CONFIRMAR == (BT_CONFIRMAR & botoes) )
            this.add(getBtConfirmar());

        if ( BT_CANCELAR == (BT_CANCELAR & botoes) )
            this.add(getBtCancelar());

        if ( BT_FECHAR == (BT_FECHAR & botoes) )
            this.add(getBtFechar());

        if ( BT_AJUDA == (BT_AJUDA & botoes) )
            this.add(getBtAjudar());
    }

    @Override
    public void setEnabled(boolean enabled) {
        if ( NONE == botoes )
            return;

        if ( BT_CONFIRMAR == (BT_CONFIRMAR & botoes) )
            getBtConfirmar().setEnabled(enabled);

        if ( BT_CANCELAR == (BT_CANCELAR & botoes) )
            getBtCancelar().setEnabled(enabled);

        if ( BT_FECHAR == (BT_FECHAR & botoes) )
            getBtFechar().setEnabled(enabled);

        if ( BT_AJUDA == (BT_AJUDA & botoes) )
            getBtAjudar().setEnabled(enabled);
    }

    /** Altera estado dos botoes
     *
     * @author paim 11/05/2011
     * @param editando
     *            void */
    @Override
    public void setEditando(boolean editando) {

        /* se nao tem o botao confirmar, entao nao pode mudar estado edicao */
        if ( NONE == (BT_CONFIRMAR & botoes) )
            throw new IllegalArgumentException("não pode mudar o estado");

        this.editando = editando;
        if ( editando ) {
            getBtConfirmar().setEnabled(true);

            /* se nao tem o cancelar */
            if ( NONE == (BT_CANCELAR & botoes) )
                return;

            /* habilita e mostra o cancelar */
            getBtCancelar().setEnabled(true);
            getBtCancelar().setVisible(true);

            /* desabilita e esconde o fechar */
            getBtFechar().setEnabled(false);
            getBtFechar().setVisible(false);
        }
        else {
            getBtConfirmar().setEnabled(false);

            /* se nao tem o cancelar */
            if ( NONE == (BT_CANCELAR & botoes) )
                return;

            /* desabilita e esconde o cancelar */
            getBtCancelar().setEnabled(false);
            getBtCancelar().setVisible(false);

            /* habilita e mostra o fechar */
            getBtFechar().setEnabled(true);
            getBtFechar().setVisible(true);
        }
    }

    @Override
    public boolean isEditando() {
        return editando;
    }

    @Override
    public EventListenerList getListenersList() {
        return this.listenerList;
    }


    @Override
    public void fire(WC_Event e) {
       // PanelButtonsListener[] listeners = getListenersList().getListeners(PanelButtonsListener.class);
        for ( PanelButtonsListener listener : fireListeners() ) {
            switch ( (Eventos) e.evento() ) {
                case CONFIRMAR:
                    listener.confirmar();
                break;
                case CANCELAR:
                    listener.cancelar();
                break;
                case AJUDAR:
                    listener.ajudar();
                break;
                case FECHAR:
                    listener.fechar();
                break;
            }
        }
    }

    public JButton getBtFechar() {
        if ( btFechar == null ) {
            btFechar = new JButtonFechar();
            btFechar.addActionListener(new FireListener(this, Eventos.FECHAR));
        }
        return btFechar;
    }

    public JButton getBtConfirmar() {
        if ( btConfirmar == null ) {
            btConfirmar = new JButtonConfirmar();
            btConfirmar.addActionListener(new FireListener(this, Eventos.CONFIRMAR));
        }
        return btConfirmar;
    }

    public JButton getBtCancelar() {
        if ( btCancelar == null ) {
            btCancelar = new JButtonCancelar();
            btCancelar.addActionListener(new FireListener(this, Eventos.CANCELAR));

        }
        return btCancelar;
    }

    public JButton getBtAjudar() {
        if ( btAjudar == null ) {
            btAjudar = new JButtonAjudar();
            btAjudar.addActionListener(new FireListener(this, Eventos.AJUDAR));
        }
        return btAjudar;
    }

}
