package paim.wingchun.view.componentes.panel;


import java.util.EventListener;

/** @author paim 28/03/2011 */
public interface PanelButtonsListener extends EventListener {

    @Evento(evento = 0 /* Eventos.CONFIRMAR.ordinal() */)
    public void confirmar();

    public void cancelar();

    public void fechar();

    public void ajudar();
}
