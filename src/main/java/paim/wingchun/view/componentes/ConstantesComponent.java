/**
 * @author temujin Feb 25, 2012
 */
package paim.wingchun.view.componentes;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingConstants;

/**
 * @author temujin Feb 25, 2012
 */
public interface ConstantesComponent extends SwingConstants {

    final String WARNING_CHAR = "*";

    final Font LABEL$Font = new Font(Font.MONOSPACED, Font.PLAIN, 10);

    final Color LABEL$ForeColor = new Color(51, 51, 51);

    final Color LABEL$BackColor = new Color(238, 238, 238);


}
