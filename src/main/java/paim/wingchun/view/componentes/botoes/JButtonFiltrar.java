package paim.wingchun.view.componentes.botoes;


public class JButtonFiltrar extends WC_JButton {

    private static final long serialVersionUID = 1L;

    public JButtonFiltrar() {
        super(Tipo.FILTRAR);
        this.setLargura(90);
    }

}
