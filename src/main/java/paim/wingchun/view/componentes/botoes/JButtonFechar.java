package paim.wingchun.view.componentes.botoes;


public class JButtonFechar extends WC_JButton {

    private static final long serialVersionUID = 1L;

    public JButtonFechar() {
        super(Tipo.FECHAR);
        this.setLargura(100);
    }

}
