package paim.wingchun.view.componentes.botoes;

public class JButtonCancelar extends WC_JButton {

    private static final long serialVersionUID = 1L;

    public JButtonCancelar() {
        super(Tipo.CANCELAR);
        this.setLargura(105);
    }

}
