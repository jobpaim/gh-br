package paim.wingchun.view.componentes.botoes;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;

/**
 * @author paim 18/05/2011
 */
public class JButtonConstrutor extends JButton {

    private static final long serialVersionUID = 1L;

    public JButtonConstrutor() {
        super(". . .");
        Dimension d = new Dimension(20, 20);
        this.setPreferredSize(d);
        this.setMaximumSize(d);
        this.setMinimumSize(d);
        this.setBorder(BorderFactory.createRaisedBevelBorder());
    }
    // como adicionao construtor aqui???acho que usando interface construtor eh o correto ok
}
