package paim.wingchun.view.componentes.botoes;

public class JButtonAjudar extends WC_JButton {

    private static final long serialVersionUID = 1L;

    public JButtonAjudar() {
        super(Tipo.AJUDAR);
        this.setLargura(90);
    }
}
