package paim.wingchun.view.componentes.botoes;

public class JButtonConfirmar extends WC_JButton {

    private static final long serialVersionUID = 1L;

    public JButtonConfirmar() {
        super(Tipo.CONFIRMAR);
        this.setLargura(70);
    }
}
