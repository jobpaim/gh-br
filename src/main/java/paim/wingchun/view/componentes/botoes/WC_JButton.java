package paim.wingchun.view.componentes.botoes;


import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import static paim.wingchun.app.WC_Properties.getProperty;
import static paim.wingchun.view.GUIUtils.getImageIcon;

public class WC_JButton extends JButton {

    private static final long serialVersionUID = 1L;

    protected enum Tipo {
        CONFIRMAR, CANCELAR, FECHAR, AJUDAR, FILTRAR
    }

    public WC_JButton() {
        super();
        // this.setFont(new Font("Tahoma", Font.ITALIC, 10));
        // this.setFont(new Font("Arial", Font.PLAIN, 10));
        this.setFont(new Font(Font.MONOSPACED, Font.ITALIC, 10));

        this.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                if ( e.getKeyCode() == KeyEvent.VK_ENTER ) {
                    WC_JButton.this.doClick();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {}

            @Override
            public void keyPressed(KeyEvent e) {}
        });
    }

    public WC_JButton(Tipo tipo) {
        this();
        this.setTipo(tipo);
    }

    public void setLargura(int w) {
        Dimension d = new Dimension(w, 20);
        this.setMinimumSize(d);
        this.setMaximumSize(d);
        this.setPreferredSize(d);
    }

    public void setTipo(Tipo tipo) {
        ImageIcon imageIcon = getImageIcon("icon.button." + tipo.name().toLowerCase());
        this.setIcon(imageIcon);
        String texto = getProperty("tooltip.button." + tipo.name().toLowerCase());
        this.setText(texto);
        this.setToolTipText(texto);

    }
}
