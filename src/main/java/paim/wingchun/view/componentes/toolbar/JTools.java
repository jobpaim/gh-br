package paim.wingchun.view.componentes.toolbar;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.event.EventListenerList;

import paim.wingchun.controller.WC_Event;
import paim.wingchun.view.FireListener;
import paim.wingchun.view.Fireable;

/**
 * Barra de ferramentas.
 *
 * @author paim 20/01/2011
 */
/** @author paim 20/01/2011 */
public class JTools extends AbstractJToolBar implements Fireable<ToolsListener> {

    private static final long serialVersionUID = 1L;

    protected enum Eventos {
        INCLUIR, EXCLUIR
    }

    protected JButton jButtonIncluir;

    protected JButton jButtonExcluir;

    public JTools() {
        super(JToolBar.HORIZONTAL);
        initialize();
    }

    private void initialize() {
        add(getjButtonIncluir());
        add(getjButtonExcluir());

        setBorderPainted(false);
        setFloatable(false);
        setRollover(true);

        // TODO poderia colocar direto nos boteos?????

        FocoBotoes foco = new FocoBotoes();

        getjButtonIncluir().addFocusListener(foco);
        getjButtonIncluir().addActionListener(new FireListener(this, Eventos.INCLUIR));

        getjButtonExcluir().addFocusListener(foco);
        getjButtonExcluir().addActionListener(new FireListener(this, Eventos.EXCLUIR));
    }

    @Override
    public EventListenerList getListenersList() {
        return this.listenerList;
    }

    @Override
    public void fire(WC_Event e) {
        //ToolsListener[] listeners = getListenersList().getListeners(ToolsListener.class);
        for ( ToolsListener listener : fireListeners() ) {
            switch ( (Eventos) e.evento() ) {
                case INCLUIR:
                    listener.incluir();
                    break;
                case EXCLUIR:
                    listener.excluir();
                    break;

                default:
                    return;
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        getjButtonExcluir().setEnabled(enabled);
        getjButtonIncluir().setEnabled(enabled);
    }

    public JButton getjButtonIncluir() {
        if ( jButtonIncluir == null ) {
            jButtonIncluir = new JButtonP(Eventos.INCLUIR);
        }
        return jButtonIncluir;
    }

    public JButton getjButtonExcluir() {
        if ( jButtonExcluir == null ) {
            jButtonExcluir = new JButtonP(Eventos.EXCLUIR);
        }
        return this.jButtonExcluir;
    }

    /**
     * quando true, somente deixa habilitado o botão incluir
     *
     * @author paim 27/04/2011
     * @param empty
     *            void
     */
    @Override
    public void setEmpty(boolean empty) {
        super.setEmpty(empty);

        if ( empty )
            getjButtonIncluir().setEnabled(true);

    }
}
