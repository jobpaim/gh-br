/** @author paim 08/03/2012 */
package paim.wingchun.view.componentes.toolbar;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.event.EventListenerList;

import paim.wingchun.controller.WC_Event;
import paim.wingchun.swing.JDropDownButton;
import paim.wingchun.view.FireListener;
import paim.wingchun.view.Fireable;
import paim.wingchun.view.GUIUtils;

/** @author paim 08/03/2012 */
public class JExtraTools extends AbstractJToolBar implements Fireable<ExtraToolsListener> {

    private static final long serialVersionUID = 1L;

    protected enum Eventos {
        LOCALIZAR, FILTRAR, LIMPARFILTRO, IMPRIMIR, LISTARFILTRO
    }

    protected JButton jButtonBuscar;

    protected JButton jButtonFiltrar;

    protected JButton jButtonLimparFiltro;

    protected JButton jButtonImprimir;

    protected JButton jButtonListaFiltro;

    protected JDropDownButton jDropDownButtonFiltro;

    protected final boolean useDropDown;

    public JExtraTools() {
        this(false);
    }

    public JExtraTools(boolean isDropDown) {
        super(JToolBar.HORIZONTAL);
        this.useDropDown = isDropDown;
        initialize();
    }

    private void initialize() {
        if ( useDropDown ) {
            add(getjDropDownButtonFiltro());
            getjDropDownButtonFiltro().addComponent(getjButtonFiltrar());
            getjDropDownButtonFiltro().addComponent(getjButtonLimparFiltro());
            getjDropDownButtonFiltro().addComponent(getjButtonListaFiltro());
        }
        else {
            add(getjButtonFiltrar());
            add(getjButtonListaFiltro());
            add(getjButtonLimparFiltro());
        }

        add(getjButtonBuscar());
        add(getjButtonImprimir());

        setBorderPainted(false);
        setFloatable(false);
        setRollover(true);

        // TODO poderia colocar direto nos boteos?????

        FocoBotoes foco = new FocoBotoes();

        getjButtonImprimir().addFocusListener(foco);
        getjButtonImprimir().addActionListener(new FireListener(this, Eventos.IMPRIMIR));

        getjButtonBuscar().addFocusListener(foco);
        getjButtonBuscar().addActionListener(new FireListener(this, Eventos.LOCALIZAR));

        getjButtonFiltrar().addFocusListener(foco);
        getjButtonFiltrar().addActionListener(new FireListener(this, Eventos.FILTRAR));

        getjButtonLimparFiltro().addFocusListener(foco);
        getjButtonLimparFiltro().addActionListener(new FireListener(this, Eventos.LIMPARFILTRO));

        getjButtonListaFiltro().addFocusListener(foco);
        getjButtonListaFiltro().addActionListener(new FireListener(this, Eventos.LISTARFILTRO));

    }

    @Override
    public EventListenerList getListenersList() {
        return this.listenerList;
    }

    /** Dispara evento para que objeto referente a posicao seja carregado */
    @Override
    public void fire(WC_Event e) {
        //ExtraToolsListener[] listeners = getListenersList().getListeners(ExtraToolsListener.class);
        for ( ExtraToolsListener listener :fireListeners() ) {
            switch ( (Eventos) e.evento() ) {
                case FILTRAR:
                    listener.filtrar();
                    break;
                case LIMPARFILTRO:
                    listener.limparFiltro();
                    break;
                case LOCALIZAR:
                    listener.localizar();
                    break;
                case IMPRIMIR:
                    listener.imprimir();
                    break;
                case LISTARFILTRO:
                    listener.listaFiltro();
                    break;

                default:
                    return;
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        getjButtonBuscar().setEnabled(enabled);
        getjButtonFiltrar().setEnabled(enabled);
        getjButtonLimparFiltro().setEnabled(enabled);
        getjButtonImprimir().setEnabled(enabled);
        getjButtonListaFiltro().setEnabled(enabled);
    }

    public JButton getjButtonBuscar() {
        if ( jButtonBuscar == null ) {
            jButtonBuscar = new JButtonP(Eventos.LOCALIZAR);
        }
        return jButtonBuscar;
    }

    public JButton getjButtonFiltrar() {
        if ( jButtonFiltrar == null ) {
            jButtonFiltrar = new JButtonP(Eventos.FILTRAR);
        }
        return jButtonFiltrar;
    }

    public JButton getjButtonLimparFiltro() {
        if ( jButtonLimparFiltro == null ) {
            jButtonLimparFiltro = new JButtonP(Eventos.LIMPARFILTRO);
        }
        return jButtonLimparFiltro;
    }

    public JButton getjButtonImprimir() {
        if ( jButtonImprimir == null ) {
            jButtonImprimir = new JButtonP(Eventos.IMPRIMIR);
        }
        return jButtonImprimir;
    }

    public JButton getjButtonListaFiltro() {
        if ( jButtonListaFiltro == null ) {
            jButtonListaFiltro = new JButtonP(Eventos.LISTARFILTRO);
        }
        return jButtonListaFiltro;
    }

    public JDropDownButton getjDropDownButtonFiltro() {
        if ( jDropDownButtonFiltro == null ) {
            // TODO imageIcon
            jDropDownButtonFiltro = new JDropDownButton(GUIUtils.getImageIcon(GUIUtils.EMPTY_ICON));
            jDropDownButtonFiltro.setEnabled(true);

            Dimension d = new Dimension(24, 24);
            jDropDownButtonFiltro.setMaximumSize(d);
            jDropDownButtonFiltro.setMinimumSize(d);
            jDropDownButtonFiltro.setPreferredSize(d);
        }
        return jDropDownButtonFiltro;
    }
}
