package paim.wingchun.view.componentes.toolbar;

import java.util.EventListener;
import java.util.EventObject;


/**
 * @author paim
 * 18/01/2011
 *
 */
public interface NavegadorListener extends EventListener {

    public void primeiro(EventObject registro);

    public void anterior(EventObject registro);

    public void proximo(EventObject registro);

    public void ultimo(EventObject registro);

    public void carrega(EventObject registro);

    public boolean podeNavegar();
}