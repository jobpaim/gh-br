/** @author paim 08/03/2012 */
package paim.wingchun.view.componentes.toolbar;


import java.util.EventListener;

/** @author paim 08/03/2012 */
public interface ExtraToolsListener extends EventListener {

    public boolean localizar();

    public boolean filtrar();

    public boolean limparFiltro();

    public boolean imprimir();

    public boolean listaFiltro();
}
