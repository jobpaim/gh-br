package paim.wingchun.view.componentes.toolbar;


import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.plaf.BorderUIResource;

import paim.wingchun.view.Editable;

import static paim.wingchun.app.WC_Properties.getProperty;
import static paim.wingchun.view.GUIUtils.getImageIcon;

/** @author paim 21/01/2011 */
@SuppressWarnings("serial")
public abstract class AbstractJToolBar extends JToolBar implements Editable {

    private boolean editando;

    public AbstractJToolBar() {
        super();
    }

    public AbstractJToolBar(int orientation) {
        super(orientation);
    }

    public AbstractJToolBar(String name, int orientation) {
        super(name, orientation);
    }

    public AbstractJToolBar(String name) {
        super(name);
    }

    @Override
    public abstract void setEnabled(boolean enabled);

    /** Seta uma borda de linha preta no botão. */
    protected void setBorda(AbstractButton b, boolean show) {
        if ( show ) {
            b.setBorder(BorderUIResource.getBlackLineBorderUIResource());
            b.setBorderPainted(true);
        }
        else {
            b.setBorderPainted(false);
        }

        b.repaint();
        this.repaint();
    }

    @Override
    public void setEditando(boolean editando) {
        this.editando = editando;
        if ( editando )
            setEnabled(false);
        else
            setEnabled(true);
    }

    @Override
    public boolean isEditando() {
        return editando;
    }

    public void setEmpty(boolean isEmpty) {
        setEnabled(!isEmpty);
    }

    /** adiciona borda com foco */
    class FocoBotoes implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            JButton b = (JButton) e.getSource();
            setBorda(b, true);
        }

        @Override
        public void focusLost(FocusEvent e) {
            JButton b = (JButton) e.getSource();
            setBorda(b, false);
        }
    }

    /** JButton Personalizado */
    protected class JButtonP extends JButton {

        private static final long serialVersionUID = 1L;

        public JButtonP() {
            super();
            this.setEnabled(false);
            Dimension d = new Dimension(24, 24);
            this.setMaximumSize(d);
            this.setMinimumSize(d);
            this.setPreferredSize(d);
            this.setBorder(BorderFactory.createEmptyBorder());
        }

        public JButtonP(Enum<?> tipo) {
            this();
            ImageIcon imageIcon = getImageIcon("icon.toolbar." + tipo.name().toLowerCase());
            this.setIcon(imageIcon);
            String toolTip = getProperty("tooltip.toolbar." + tipo.name().toLowerCase(), "");
            this.setToolTipText(toolTip);
        }

        public JButtonP(ImageIcon imageIcon) {
            this();
            this.setIcon(imageIcon);
        }
    }

}
