package paim.wingchun.view.componentes.toolbar;


import java.util.EventListener;

/** @author paim 20/01/2011 */
public interface ToolsListener extends EventListener {

    public void excluir();

    public void incluir();

}
