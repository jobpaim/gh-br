/** @author temujin Feb 6, 2012 */
package paim.wingchun.view.componentes;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

/** @author temujin Feb 6, 2012 */
public class WC_TextField<T> extends AbstractComponent<T> {

    private static final long serialVersionUID = 1L;

    private JTextField textField;

    public WC_TextField() {
        this(null, false, TOP);
    }

    public WC_TextField(Class<T> clazz) {
        this(clazz, true, TOP);
    }

    public WC_TextField(Class<T> clazz, boolean hasConstrutor) {
        this(clazz, hasConstrutor, TOP);
    }

    public WC_TextField(Class<T> clazz, boolean hasConstrutor, int labelPosition) {
        super(clazz, hasConstrutor, labelPosition);
    }

    @Override
    protected void makeComponent() {
        // setBorder(BorderFactory.createLineBorder(Color.BLUE));
        switch ( labelPosition ) {
            case LEFT:
                initializeLeft();
                break;
            case TOP:
            default:
                initializeTop();
                break;
        }
    }

    /** @author temujin Feb 24, 2012 */
    private void initializeLeft() {
        setLayout(new FlowLayout());
        setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        add(getLabel());
        add(getWarning());
        add(getTextField());
        if ( hasConstrutor )
            add(getConstrutor());

        // setWarning(Color.RED);
    }

    /** @author temujin Feb 24, 2012 */
    private void initializeTop() {
        setLayout(new GridBagLayout());
        setMinimumSize(new Dimension(80, 35));
        setMaximumSize(new Dimension(Short.MAX_VALUE, 35));

        GridBagConstraints constraints1 = new GridBagConstraints();
        constraints1.gridx = 0;
        constraints1.gridy = 0;
        constraints1.gridwidth = 3;
        constraints1.anchor = GridBagConstraints.LINE_START;
        constraints1.insets = new Insets(2, 2, 1, 0);
        add(getLabel(), constraints1);

        GridBagConstraints constraints2 = new GridBagConstraints();
        constraints2.gridx = 0;
        constraints2.gridy = 1;
        constraints2.anchor = GridBagConstraints.PAGE_END;
        constraints2.fill = GridBagConstraints.HORIZONTAL;
        constraints2.weightx = 1D;
        constraints2.insets = new Insets(0, 2, 0, 2);
        add(getTextField(), constraints2);

        GridBagConstraints constraints3 = new GridBagConstraints();
        constraints3.gridx = 1;
        constraints3.gridy = 1;
        constraints3.anchor = GridBagConstraints.PAGE_END;
        constraints3.insets = new Insets(0, 0, 0, 0);

        add(getWarning(), constraints3);

        GridBagConstraints constraints4 = new GridBagConstraints();
        constraints4.gridx = 2;
        constraints4.gridy = 1;
        constraints4.anchor = GridBagConstraints.LAST_LINE_END;
        constraints4.insets = new Insets(0, 2, 0, 2);
        constraintsConstrutor = constraints4;
        if ( hasConstrutor )
            add(getConstrutor(), constraintsConstrutor);

    }

    private GridBagConstraints constraintsConstrutor;

    public JTextField getTextField() {
        if ( textField == null ) {
            textField = new JTextField();
            textField.setHorizontalAlignment(LEFT);
            // Dimension d = new Dimension(Short.MAX_VALUE, 20);
            // text.setMaximumSize(d);
            // text.setMinimumSize(d);
            // text.setPreferredSize(d);
            textField.setEnabled(false);
            textField.addFocusListener(new FocusListener() {

                @Override
                public void focusLost(FocusEvent e) {
                    textField.setForeground(Color.BLACK);
                }

                @Override
                public void focusGained(FocusEvent e) {
                    textField.setForeground(Color.BLUE);
                }
            });
        }
        return textField;
    }

    public WC_TextField<T> setText(String text) {
        getTextField().setText(text);
        return this;
    }

    @Override
    public WC_TextField<T> setEditable(boolean editable) {
        if (hasConstrutor)
            getConstrutor().setEnabled(editable);

        getTextField().setEditable(editable);

        return this;
    }



    /** @author temujin Mar 1, 2012 void */
    @Override
    public WC_TextField<T> addConstrutor() {
        if ( construtor != null )
            throw new RuntimeException("ja existe Construtor");

        switch ( labelPosition ) {
            case LEFT:
                add(getConstrutor());
                break;
            case TOP:
                add(getConstrutor(), constraintsConstrutor);
                break;

            default:
                break;
        }
        hasConstrutor = true;
        return this;
    }

    public void showErro() {

    }

}
