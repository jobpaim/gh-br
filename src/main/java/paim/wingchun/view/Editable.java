/**
 * @author paim 09/03/2012
 * 
 */
package paim.wingchun.view;

/**
 * @author paim 09/03/2012
 * 
 */
public interface Editable {

    //    private transient boolean _editando_setado;
    
//    private boolean editando;
    
    public boolean isEditando();

    public void setEditando(boolean editando);
    
//    public Editing getEditing();
    
//    public void changeEditando(boolean editando);
    
//    public boolean canChangeEditando(boolean editando) {
}
