/**
 * @author paim 04/11/2011
 */
package paim.wingchun.view;

import javax.swing.JButton;
import javax.swing.JTextField;

import paim.wingchun.view.componentes.table.WC_JTable;

/**
 * @author paim 04/11/2011
 */
public interface TableFiltravel {

    public JButton getButtonClear();

//    public JButton getButtonFiltrar();

    public JTextField getTextFieldFiltro();
    
    public  WC_JTable<?> getTable();

}
