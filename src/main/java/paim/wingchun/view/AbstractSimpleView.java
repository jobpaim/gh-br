/** @author paim 01/04/2011 */
package paim.wingchun.view;


import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import paim.wingchun.app.Reflections;
import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.componentes.WC_Label;
import paim.wingchun.view.componentes.toolbar.JExtraTools;
import paim.wingchun.view.componentes.toolbar.JNavegador;
import paim.wingchun.view.componentes.toolbar.JTools;

/** @author paim 01/04/2011 */
public abstract class AbstractSimpleView<P extends Pojo> extends AbstractView<P> {

    private static final long serialVersionUID = 1L;

    private boolean empty = true;

    private JNavegador jNavegador;

    private JTools jTools;

    private JExtraTools jExtraTools;

    private CardLayout cardLayout;

    private JPanel jPanelUsuario;

    private JPanel emptyPanel;

    private Map<String, WC_Label> mapLabel = new HashMap<String, WC_Label>();

    protected AbstractSimpleView() {
        super();
        setMapLabel();
    }

    public JNavegador getNavegador() {
        if ( jNavegador == null ) {
            jNavegador = new JNavegador();
            jNavegador.add(getNavegadorTools());
            jNavegador.add(getToolsExtra());
        }
        return jNavegador;
    }

    public JTools getNavegadorTools() {
        if ( jTools == null ) {
            jTools = new JTools();
        }
        return jTools;
    }

    public JExtraTools getToolsExtra() {
        if ( jExtraTools == null ) {
            jExtraTools = new JExtraTools();
        }
        return jExtraTools;
    }

    @Override
    protected void initialize() {
        // this.setSize(new Dimension(500, 200));
        // this.setResizable(true);
        // this.setTitle("noname");
        // this.setContentPane(getjInternalFrame());
        // this.setFrameIcon(getIconJInternalFrame());
        super.initialize();
        this.pack();
    }

    // protected ImageIcon getIconJInternalFrame() {
    // return GUIUtils.getImageIcon("icon.tabela." + getpClass().getSimpleName());
    // }

    @Override
    protected JToolBar getToolBar() {
        return getNavegador();
    }

    public CardLayout getCardLayout() {
        if ( cardLayout == null ) {
            cardLayout = new CardLayout();
            cardLayout.setHgap(8);
            cardLayout.setVgap(5);
        }
        return cardLayout;
    }

    protected abstract JPanel getJPanel();

    @Override
    protected JPanel getJPanelUsuario() {
        if ( jPanelUsuario == null ) {
            jPanelUsuario = new JPanel();
            jPanelUsuario.setLayout(getCardLayout());
            /* cardlayout precisa nome */
            getJPanel().setName(getpClass().getSimpleName() + "_cheio");

            jPanelUsuario.add(getJPanel(), getJPanel().getName());
            jPanelUsuario.add(getjEmptyPanel(), getjEmptyPanel().getName());
        }

        return jPanelUsuario;
    }

    protected JPanel getjEmptyPanel() {
        if ( emptyPanel == null ) {
            emptyPanel = new JPanel();
            emptyPanel.setName(getpClass().getSimpleName() + "_Vazio");
            emptyPanel.setLayout(new BorderLayout());

            emptyPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

            StringBuilder s = new StringBuilder();
            s.append("Tabela de ");
            s.append(Reflections.getRotulo(getpClass()));
            s.append(" estï¿½ vazia");

            JLabel rotulo = new JLabel(s.toString());
            rotulo.setForeground(Color.blue);
            rotulo.setVerticalAlignment(SwingConstants.CENTER);
            rotulo.setHorizontalAlignment(SwingConstants.CENTER);
            rotulo.setFont(new Font("Tahoma", Font.BOLD, 12));

            emptyPanel.add(rotulo, BorderLayout.CENTER);

        }
        return emptyPanel;
    }

    @Override
    protected Object v_getObjeto() throws Exception {
        /* o objeto de referencia ja recebeu as alteracoes via controlador */
        return getObjetoReferencia();
    }

    @Override
    public void setEditando(boolean editando) {
        super.setEditando(editando);
        if ( !isEmpty() ) {
            getNavegador().setEditando(editando);
            getNavegadorTools().setEditando(editando);
            getToolsExtra().setEditando(editando);
        }
        getjPanelBotoes().setEditando(editando);
    }

    @Override
    public final void vCleanErros() {
        cleanErros();
    }

    @Override
    public String getHelpId() {
        // TODO Auto-generated method stub
        return null;
    }

    /** coloca um painel vazio e desabilita navegador
     *
     * @author paim 23/05/2011
     * @param empty
     *            void */
    public void setEmpty(boolean empty) {
        this.empty = empty;

        if ( isEmpty() )
            getCardLayout().show(getJPanelUsuario(), getjEmptyPanel().getName());
        else
            getCardLayout().show(getJPanelUsuario(), getJPanel().getName());

        getToolsExtra().setEmpty(empty);
        getNavegadorTools().setEmpty(empty);
        getNavegador().setEmpty(empty);
    }

    public boolean isEmpty() {
        return empty;
    }

    /** retorna o map com os componentes WCJLabel */
    public final Map<String, WC_Label> getMapLabel() {
        return this.mapLabel;
    }

    protected abstract void setMapLabel();

    public void cleanErros() {
        for ( WC_Label label : getMapLabel().values() ) {
            label.cleanErro();
        }
    }

    @Override
    protected void v_setErros(List<Erro> erros) {

        if ( getMapLabel().isEmpty() )
            return;

        for ( Erro erro : erros ) {
            WC_Label label = getMapLabel().get(erro.getFieldName());
            label.setErro(erro);
        }
    }

}
