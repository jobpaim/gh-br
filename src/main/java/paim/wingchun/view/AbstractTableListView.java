package paim.wingchun.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.event.EventListenerList;

import paim.wingchun.controller.WC_Event;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.componentes.table.WC_JTable;

import static paim.wingchun.app.WC_Properties.getProperty;

/** @author temujin Jun 6, 2011 */
public abstract class AbstractTableListView<P extends Pojo> extends AbstractListView<P> implements
        Fireable<FiltrarListener>, TableFiltravel {

    private static final long serialVersionUID = 1L;

    protected enum Eventos {
        FILTRAR, LIMPAR_FILTRO
    }

    protected AbstractTableListView(Class<P> pClass) {
        super(pClass);
    }

    private JScrollPane scrollPane;

    private JLabel labelFiltro;

    private JPanel jpanel;

    private JToolBar toolBar;

    private JPanel panelToolBar;

    private JTextField textFieldFiltro;

    private JButton buttonClear;

    @Override
    public EventListenerList getListenersList() {
        return this.listenerList;
    }

    @Override
    public void fire(WC_Event e) {
        //FiltrarListener[] listeners = getListenersList().getListeners(FiltrarListener.class);
        for ( FiltrarListener listener : fireListeners() ) {
            switch ( (Eventos) e.evento() ) {
                case FILTRAR:
                    listener.filtrar();
                    break;
                case LIMPAR_FILTRO:
                    listener.limparFiltro();
                    break;

                default:
                    break;
            }
        }
    }

    @Override
    protected void initialize() {
        super.initialize();
        getTextFieldFiltro().addKeyListener(new FireListener(this, Eventos.FILTRAR));
        getButtonClear().addActionListener(new FireListener(this, Eventos.LIMPAR_FILTRO));
    }

    @Override
    protected JPanel getJPanelUsuario() {
        if ( jpanel == null ) {
            jpanel = new JPanel();
            jpanel.setLayout(new BorderLayout());
            jpanel.setBorder(GUIUtils.getBordaPadrao("Selecione"));
            jpanel.add(getScrollPane(), BorderLayout.CENTER);
        }
        return jpanel;
    }

    @Override
    protected JToolBar getToolBar() {
        if ( toolBar == null ) {
            toolBar = new JToolBar();
            toolBar.setFloatable(false);
            toolBar.setRollover(true);
            toolBar.setFocusable(false);
            toolBar.add(getpanelToolBar());
            Dimension d = new Dimension(this.getWidth() - 100, 28);
            toolBar.setPreferredSize(d);
        }
        return toolBar;
    }

    private JScrollPane getScrollPane() {
        if ( scrollPane == null ) {
            scrollPane = new JScrollPane();
            scrollPane.setViewportView(getTable());
        }
        return scrollPane;
    }

    private JPanel getpanelToolBar() {

        if ( panelToolBar == null ) {
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
            gridBagConstraints.gridx = 2;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.insets = new Insets(0, 0, 0, 0);

            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.anchor = GridBagConstraints.NORTHWEST;
            gridBagConstraints1.gridx = 0;
            gridBagConstraints1.gridy = 0;
            gridBagConstraints1.insets = new Insets(0, 10, 0, 0);

            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.anchor = GridBagConstraints.NORTHWEST;
            gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints2.weightx = 1D;
            gridBagConstraints2.gridx = 1;
            gridBagConstraints2.gridy = 0;
            gridBagConstraints2.insets = new Insets(0, 10, 0, 0);

            panelToolBar = new JPanel();
            panelToolBar.setLayout(new GridBagLayout());
            panelToolBar.add(getLabelFiltro(), gridBagConstraints1);
            panelToolBar.add(getTextFieldFiltro(), gridBagConstraints2);
            panelToolBar.add(getButtonClear(), gridBagConstraints);
        }
        return panelToolBar;
    }

    public JLabel getLabelFiltro() {
        if ( labelFiltro == null ) {
            labelFiltro = new JLabel();
            labelFiltro.setText(getProperty("filtrar.label"));
        }

        return labelFiltro;
    }

    @Override
    public JTextField getTextFieldFiltro() {
        if ( textFieldFiltro == null ) {
            textFieldFiltro = new JTextField();
            textFieldFiltro.setToolTipText(getProperty("tooltip.text.filtrar"));
        }

        return textFieldFiltro;
    }

    @Override
    public JButton getButtonClear() {
        if ( buttonClear == null ) {
            buttonClear = new JButton();
            buttonClear.setText("");
            buttonClear.setIcon(GUIUtils.getImageIcon("icon.toolbar.limparfiltro"));
            buttonClear.setToolTipText(getProperty("tooltip.toolbar.limparfiltro"));
            Dimension d = new Dimension(20, 20);
            buttonClear.setMinimumSize(d);
            buttonClear.setMaximumSize(d);
            buttonClear.setPreferredSize(d);

        }

        return buttonClear;
    }

    @Override
    public abstract WC_JTable<P> getTable();
}
