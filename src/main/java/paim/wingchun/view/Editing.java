/**
 * @author temujin Mar 18, 2012
 */
package paim.wingchun.view;


/**
 * @author temujin Mar 18, 2012
 */
@Deprecated
public class Editing implements Editable2 {




    private transient boolean b_editando_setado;

    private boolean editando;

    @SuppressWarnings("unused")
    private Editable2 editable;

    public Editing(Editable2 editable) {
        this.editable = editable;
    }

    @Override
    public boolean isEditando() {
        return editando;
    }

    @Override
    public void setEditando(boolean editando) {
        if ( !canChangeEditando(editando) )
            return;

        this.editando = editando;
        /* editable.changeEditando(editando); */
    }

    @Override
    public boolean canChangeEditando(boolean editando) {
        /* somente executa se realmente houver alteracao */
        // boolean alterado = editando ^ isEditando();
        // coloca num if generico

        if ( b_editando_setado && !(editando ^ isEditando()) )
            return false;
        else
            b_editando_setado = true;
        return true;

    }

    /* (non-Javadoc)
     * @see paim.wingchun.visao.Editable2#getEditing() */
    @Override
    public Editing getEditing() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see paim.wingchun.visao.Editable2#changeEditando(boolean) */
    @Override
    public void changeEditando(boolean editando) {
        // TODO Auto-generated method stub

    }

    public enum Estadoong /* implements Estado */{
        QW, OS
    };

    public Enum<?> Estado;

    public Enum<?> getEstado() {
        return Estadoong.QW;

    }

    public void changeEstado(Enum<?> estado) {

    }

    public <E extends Enum<?>> void chan(E xx) {

    }

    // // @Override
    // public abstract void changeEditando(boolean editando);

    public static void main(String[] args) {
       Editing u = new Editing(null);
       u.chan(null);
    }



    //    public Estados estado;


}


@Deprecated
interface Editable2 {

    // private transient boolean _editando_setado;

      boolean editando=false;

    public boolean isEditando();

    public void setEditando(boolean editando);

    public Editing getEditing();

    public void changeEditando(boolean editando);

    public boolean canChangeEditando(boolean editando);
}
