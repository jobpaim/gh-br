/** @author paim 03/11/2011 */
package paim.wingchun.view;


import java.util.EventListener;

/** @author paim 03/11/2011 */
public interface FiltrarListener extends EventListener {

    public void filtrar();

    public void limparFiltro();

}
