/** @author paim 09/12/2011 */
package paim.wingchun.model;


/** @author paim 09/12/2011 */
public class ModelException extends Exception {

    public ModelException() {}

    public ModelException(String message) {
        super(message);
    }

    public ModelException(Throwable cause) {
        super(cause);
    }

    public ModelException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModelException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
