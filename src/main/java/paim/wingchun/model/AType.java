package paim.wingchun.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

@Target({ TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface AType {

    /**
     * rotulo da {@link Class}<BR>
     * refere-se a palavara no singular
     * 
     * @author paim 25/10/2011
     * @return String
     */
    String rotulo() default "";

    /**
     * rotuloS da {@link Class} <BR>
     * refere-se ao rotulo no plural, para uso em listas
     * 
     * @author paim 25/10/2011
     * @return String
     */
    String rotuloS() default "";
}
