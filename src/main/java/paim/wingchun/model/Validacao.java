package paim.wingchun.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import paim.wingchun.app.WC_Utils;
import paim.wingchun.model.modelos.ErroModel;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

@Target({ METHOD, FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Validacao {

    /** se for nulo, validar como ...*/
    ErroModel.Level nulo() default ErroModel.Level.NENHUM;

    ErroModel.Level vazio() default ErroModel.Level.NENHUM;

    ErroModel.Level negativo() default ErroModel.Level.NENHUM;

    ErroModel.Level zero() default ErroModel.Level.NENHUM;

    ErroModel.Level campo() default ErroModel.Level.NENHUM;

    WC_Utils.Campo tipoCampo() default WC_Utils.Campo.NENHUM;

}