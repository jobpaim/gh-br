/** @author paim 09/12/2011 */
package paim.wingchun.model.DAO;


/** @author paim 09/12/2011 */
public class DAOException extends RuntimeException {

    public DAOException() {}

    public DAOException(String message) {
        super(message);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
