package paim.wingchun.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

import paim.wingchun.app.WC_Utils;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

@Target({ METHOD, FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface AField {

    /** rotulo do {@link Field}
     * @author paim 25/10/2011
     * @return String
     */
    String rotulo() default "";


    /** Indica que o atributo eh oculto para filtros, localizacao e reports....
     * @author paim 27/05/2011
     * @return boolean
     */
    boolean isHidden() default false;

    WC_Utils.Atributo tipoAtributo() default WC_Utils.Atributo.PADRAO;
}
