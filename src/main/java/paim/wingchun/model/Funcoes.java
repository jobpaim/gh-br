package paim.wingchun.model;


import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import paim.wingchun.model.modelos.ErroModel;
import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;

/** @author paim 24/09/2013
 * @since */
// XXX JAVA8 migrar para quando possivel
public class Funcoes {

    public static class POJO {

        /* nao usar mais esse metodo , usar sempre o transformer */
        @Deprecated
        public static List<Long> getIds(List<Pojo> lista) {
            return Lists.transform(lista, POJO.id());
        }

        /** Function para obter id ao ser passado pojo, usado em colecoes e listas
         *
         * @author temujin May 1, 2012 */

        /* @formatter:off */
        /* trocando */
        /* List<Long> ids = Lists.transform(pojos, id()); */
        /* por */
        /* List<Long> ids = pojos.stream().map(o -> o.getId()).collect(Collectors.toList()); */
        /* @formatter:on */
        @Deprecated
        public static Function<Pojo, Long> id() {
            Function<Pojo, Long> function = new Function<Pojo, Long>() {

                @Override
                public Long apply(Pojo pojo) {
                    return pojo.getId();
                }
            };
            return function;
        }
    }

    public static class ERRO {

        /** filtra a lista de erros com mesma classe de objeto pojo */
        public static Predicate<Erro> by_class(final Class<? extends Pojo> clazz) {
            Predicate<Erro> predicate = new Predicate<Erro>() {

                @Override
                public boolean apply(@Nullable Erro erro) {
                    return clazz.isAssignableFrom(erro.getPojo().getClass());
                }
            };
            return predicate;
        }

        public static Predicate<Erro> by_fieldName(final String fieldName) {
            Predicate<Erro> predicate = new Predicate<Erro>() {

                @Override
                public boolean apply(@Nullable Erro erro) {
                    return erro.getFieldName().equals(fieldName);
                }

            };
            return predicate;
        }

        public static Predicate<Erro> by_level(final ErroModel.Level level) {
            Predicate<Erro> predicate = new Predicate<Erro>() {

                @Override
                public boolean apply(@Nullable Erro erro) {
                    return erro.getLevel().ordinal() >= level.ordinal();
                }

            };
            return predicate;
        }

        /* @formatter:off */
        /*  List<Erro> impeditivos = (List<Erro>) Collections2.filter(erros, Funcoes.ERRO.impeditivos());*/
         /* usar lambda ... */
        /* List<Erro> impeditivos = erros.stream().filter(e -> e.getLevel().getValor() >= Level.ERRO$IMPEDITIVO.getValor()).collect(Collectors.toList());*/
        /* usando java8 lambda */
        /* @formatter:on */
        @Deprecated
        public static Predicate<Erro> impeditivos() {
            Predicate<Erro> predicate = new Predicate<Erro>() {

                @Override
                public boolean apply(@Nullable Erro erro) {
                    return erro.getLevel().ordinal() >= ErroModel.Level.ERRO$IMPEDITIVO.ordinal();
                }

            };
            return predicate;
        }

    }

}
