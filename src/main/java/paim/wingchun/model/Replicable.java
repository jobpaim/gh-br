/**
 * @author paim 08/12/2011
 */
package paim.wingchun.model;

import paim.wingchun.model.pojos.Pojo;

/**
 * interface para usuario replicar objetos
 * 
 * @author paim 08/12/2011
 */
public interface Replicable {

    /**
     * metodo gera uma replica desse POJO<BR>
     * este medo eh usado quando os objetos sao 
     * expandidos/clonados para as grades irmas.
     * nao deve ser usado como um metodo para clonar esse objeto
     * 
     * @author paim 08/12/2011
     * @return POJO
     */
    public Pojo replica();
}
