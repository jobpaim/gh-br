package paim.wingchun.model.modelos;


import paim.wingchun.model.pojos.Erro;

public class ErroModel extends Model<Erro> {

    public enum Level {
        /** nao deve ser validado */
        NENHUM(0), AVISO(1), ALERTA(2), ERRO(4), ERRO$IMPEDITIVO(5), ERRO$PRIORITARIO(6);

        private int valor;

        private Level(int valor) {
            this.valor = valor;
        }

        public Integer getValor() {
            return valor;
        }

        public boolean isImpeditivo() {
            return (valor >= ERRO$IMPEDITIVO.getValor() ? true : false);
        }
    }

    public enum Tipo {
        VALIDACAO("Validação"), EXCLUSAO("Exclusão"), PERSISTENCIA("Persistência"), SISTEMA("Sistema");

        private Tipo(String tipo) {
            nome = tipo;
        }

        private String nome;

        public String getNome() {
            return nome;
        }

        public void setNome(String valor) {
            this.nome = valor;
        }
    }

    /** Instancia */
    private static ErroModel modelo;

    private ErroModel() {}

    /** Singleton */
    public static ErroModel get() {
        if ( modelo == null )
            modelo = new ErroModel();
        return modelo;
    }


}
