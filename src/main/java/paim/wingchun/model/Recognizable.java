package paim.wingchun.model;

import paim.wingchun.model.pojos.Pojo;


public interface Recognizable<P extends Pojo> {

    public Class<P> getpClass();

}
