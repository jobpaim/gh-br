/**
 * @author paim 10/05/2010
 */
package paim.wingchun.tarefa;

public interface IEventoProgresso {

    public abstract void onProgress(Object sender, int min1, int max1, int progress1, int min2, int max2,
            int progress2, String descricaoProgresso);

}