/**
 * @author paim 11/05/2010
 */
package paim.wingchun.tarefa;

import javax.swing.JFrame;

/**
 * @author paim 11/05/2010
 */
public abstract class TarefaComBarras extends Tarefa {

    private CTarefa cTarefa = null;

//    private ITarefaBarra tarefaBarra;

    /** numero de barras de progresso */
    private int nBarras = 0;

    public TarefaComBarras(String nome) {
        this( nome, 0);
    }

    public TarefaComBarras(String nome, int nBarras) {
        super(nome);
        this.nBarras = nBarras;
    }

//    public TarefaComBarras(ITarefaBarra tarefaBarra, String nomeTarefa) {
//        this(tarefaBarra, nomeTarefa, 0);
//    }
//
//    public TarefaComBarras(ITarefaBarra tarefaBarra, String nomeTarefa, int nPBar) {
//        super(nomeTarefa);
//        this.nPBar = nPBar;
//        this.tarefaBarra = tarefaBarra;
//    }

    protected CTarefa getCTarefa() {
        return this.cTarefa;
    }

    /**Frame que contem a Visao */
    protected abstract JFrame getOwner();

    @Override
    protected void finalizacao() throws Exception {
        cTarefa.setRotulo("Fim coisas: ");
    }

    // protected CTarefa getCTarefa() {
    // return this.cTarefa;
    // }

    @Override
    protected boolean inicializacao() throws Exception {
        cTarefa = new CTarefa(getOwner(), "Aguarde...", 700, 300, true, true, false);
        cTarefa.setTarefa(this, true, false);
        cTarefa.setRotulo("Verificando coisas: ");
        cTarefa.mostra();

        for ( int i = 1; i <= nBarras; i++ ) {
            cTarefa.getBarras().add(cTarefa.new Barra(i));
        }
        return true;
    }

}
