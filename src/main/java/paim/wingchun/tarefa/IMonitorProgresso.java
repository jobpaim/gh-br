/**
 * @author paim 05/05/2010
 */
package paim.wingchun.tarefa;

import java.awt.Component;
import java.awt.event.ActionListener;

import paim.wingchun.tarefa.CTarefa.Barra;

public interface IMonitorProgresso {

    public Barra getBarra(int n);
    
    public Barra getBarra();
    
    public void addMenssagem(String mensagem);

    public void setRotulo(String msg);

    public void termina();

//    public void mostra();

    public Component getComponentPai();

    public void setTarefa(Tarefa tarefa, boolean canCancel, boolean canClose);

    /* Listener chamada quando janela de progresso for efetivamente fechada*/
    public void adicionaListenerFechar(ActionListener actionListener);
}
