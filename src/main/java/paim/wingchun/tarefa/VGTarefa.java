/**
 * @author paim
 * 05/05/2010
 *
 */
package paim.wingchun.tarefa;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import paim.wingchun.swing.JLabelBlink;


/**
 * @author paim
 * 05/05/2010
 *
 */

//TODO usar botoes do camel
public class VGTarefa extends JPanel {

    private static final long serialVersionUID = 1L;

    private JLabelBlink label;

    private JScrollPane scrollLista;

    private JList lista;

    private JPanel pBotoes;

    private JButton btCancelar;

    private JButton btFechar;

    private GridBagConstraints gridBagConstraintsLabel;

    private JPanel panelProgress;

    private List<JProgressBar> jProgressosBar = new ArrayList<>();

    private List<JLabel> jLabelsBar = new ArrayList<>();

    public VGTarefa() {
        super();
        initialize();
    }

    public void setVisiblePainelLista(boolean visible) {
        getScrollLista().setVisible(visible);
        if ( visible ) {
            gridBagConstraintsLabel.weighty = 0.0;
        }
        else {
            gridBagConstraintsLabel.weighty = 1.0;
        }
        this.remove(getLabel());
        this.add(getLabel(), gridBagConstraintsLabel);
    }

    public boolean isVisiblePainelLista() {
        return getScrollLista().isVisible();
    }

    private void initialize() {
        this.setSize(589, 318);
        this.setLayout(new GridBagLayout());

        GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
        gridBagConstraints3.gridx = 0;
        gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints3.gridy = 3;

        GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.fill = GridBagConstraints.BOTH;
        gridBagConstraints2.gridy = 1;
        gridBagConstraints2.weightx = 1.0;
        gridBagConstraints2.weighty = 3.0;
        gridBagConstraints2.insets = new Insets(0, 10, 0, 10);
        gridBagConstraints2.gridx = 0;

        gridBagConstraintsLabel = new GridBagConstraints();
        gridBagConstraintsLabel.gridx = 0;
        gridBagConstraintsLabel.fill = GridBagConstraints.BOTH;
        gridBagConstraintsLabel.insets = new Insets(2, 8, 10, 8);
        gridBagConstraintsLabel.weighty = 0.0;
        gridBagConstraintsLabel.gridy = 0;

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 10, 6, 10);
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.anchor = GridBagConstraints.SOUTH;
        gridBagConstraints.gridy = 2;

        this.add(getpanelProgress(), gridBagConstraints);
        // this.add(getJProgressBar(), gridBagConstraints);
        this.add(getLabel(), gridBagConstraintsLabel);
        this.add(getScrollLista(), gridBagConstraints2);
        this.add(getPBotoes(), gridBagConstraints3);
    }

    private JPanel getpanelProgress() {
        if ( panelProgress == null ) {
            panelProgress = new JPanel();
            panelProgress.setLayout(new GridBagLayout());
            // panelProgress.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
            addJProgressBar();
        }
        return panelProgress;
    }

    public boolean addJProgressBar() {
        if ( panelProgress != null ) {
            JProgressBar jProgressBar_ = new JProgressBar();
            jProgressBar_.setIndeterminate(false);
            jProgressBar_.setForeground(new Color(8, 36, 107));

            JLabel labelBarra = new JLabel();
            labelBarra.setHorizontalAlignment(JLabel.LEFT);
            labelBarra.setIcon(new ImageIcon(getClass().getResource("/imagens/print-preview.gif")));
            labelBarra.setVerticalAlignment(SwingConstants.TOP);
            labelBarra.setFont(new Font("Arial", Font.PLAIN, 10));

            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints.weightx = 1.0D;
            gridBagConstraints.weighty = 1.0D;
            gridBagConstraints.anchor = GridBagConstraints.SOUTH;

            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.gridx = 0;
            gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints1.weightx = 1.0D;
            gridBagConstraints1.weighty = 1.0D;
            gridBagConstraints1.anchor = GridBagConstraints.SOUTH;

            panelProgress.add(labelBarra, gridBagConstraints1);
            panelProgress.add(jProgressBar_, gridBagConstraints);

            jProgressosBar.add(jProgressBar_);
            jLabelsBar.add(labelBarra);
            return true;
        }
        return false;
    }

    public List<JProgressBar> getjProgressosBar() {
        return this.jProgressosBar;
    }

    public List<JLabel> getjLabelsBar() {
        return this.jLabelsBar;
    }

    public JLabelBlink getLabel() {
        if ( label == null ) {
            label = new JLabelBlink();
            label.setHorizontalAlignment(JLabel.LEFT);
            label.setIcon(new ImageIcon(getClass().getResource("/imagens/print-preview.gif")));
            label.setVerticalAlignment(SwingConstants.TOP);
            label.setFont(new Font("Arial", Font.PLAIN, 12));
            label.setBlinking(false);

        }
        return label;
    }

    private JScrollPane getScrollLista() {
        if ( scrollLista == null ) {
            scrollLista = new JScrollPane();
            scrollLista.setViewportView(getLista());
            scrollLista.setFocusable(false);
        }
        return scrollLista;
    }

    public JList getLista() {
        if ( lista == null ) {
            lista = new JList();
            lista.setFocusable(false);
        }
        return lista;
    }

    public JPanel getPBotoes() {
        if ( pBotoes == null ) {
            FlowLayout flowLayout = new FlowLayout();
            flowLayout.setAlignment(java.awt.FlowLayout.RIGHT);
            pBotoes = new JPanel();
            pBotoes.setLayout(flowLayout);
            pBotoes.add(getBtCancelar(), null);
            pBotoes.add(getBtFechar(), null);
        }
        return pBotoes;
    }

    public JButton getBtCancelar() {
        if ( btCancelar == null ) {
            btCancelar = new JButton();
            btCancelar.setText("Cancelar");
            btCancelar.setFont(new Font("Arial", Font.PLAIN, 11));
            btCancelar.setSize(new Dimension(120, 20));
            btCancelar.setIcon(new ImageIcon(getClass().getResource("/imagens/undo.gif")));
            /* default,  desabilitado e oculto  */
            btCancelar.setEnabled(false);
            btCancelar.setVisible(false);
        }
        return btCancelar;
    }

    public JButton getBtFechar() {
        if ( btFechar == null ) {
            btFechar = new JButton();
            btFechar.setText("Fechar");
            btFechar.setFont(new Font("Arial", Font.PLAIN, 11));
            btFechar.setSize(new Dimension(120, 20));
            btFechar.setIcon(new ImageIcon(getClass().getResource("/imagens/exit.gif")));
            /* default,  desabilitado */
            btFechar.setEnabled(false);
        }
        return btFechar;
    }

}