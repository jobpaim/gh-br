package paim.wingchun.controller;


import java.util.List;

import paim.wingchun.controller.eventos.DefaultFiltrarEvent;
import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.AbstractSimpleView;
import paim.wingchun.view.AbstractView;
import paim.wingchun.view.LocalizarView;
import paim.wingchun.view.componentes.table.CarregarListener;

public class Localizar<P extends Pojo> extends AbstractListController<P, LocalizarView<P>> implements CarregarListener {

    public Localizar() {
        super();
    }

    public <V2 extends AbstractView<P>, C extends AbstractController<P, V2> & HasConstrutor> Localizar(C parent) {
        super(parent);
    }

    @Override
    protected void apos_addListeners() {
        super.apos_addListeners();
        getVisao().addListener(new DefaultFiltrarEvent(getVisao()));

        getVisao().getTable().addListener(this);
    }

    @Override
    public void carregar() {
        trataDevolver();
    }

    @Override
    public void selecionar() {
        getVisao().setEditando(true);
    }

    @Override
    protected boolean antes_cancelar(Object objeto) {
        return false;
    }

    @Override
    protected boolean antes_confirmar(Object objeto, List<Erro> erros) {
        trataDevolver();
        /* nao confirma nada */
        return false;
    }

    protected void trataDevolver() {
        try {
            P objeto = getVisao().getTable().getSelectedObject();
            if ( objeto == null )
                return;
            devolver(objeto);
        }
        catch ( Exception e1 ) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    @Override
    protected void devolver(Object pojo) throws Exception {
        /* deixa o navegador setar o objeto na visao */
        ((AbstractSimpleView<?>) getPai().getVisao()).getNavegador().navega(((Pojo) pojo).getId());

        /* fecha localizador */
        getVisao().fechar();
    }

    @Override
    public boolean isModal() {
        return true;
    }
}
