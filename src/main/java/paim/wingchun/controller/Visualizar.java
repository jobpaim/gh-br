package paim.wingchun.controller;


import java.util.List;

import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.VisualizarView;

/** @author paim 14/02/2014
 * @since */
public class Visualizar<P extends Pojo> extends AbstractListController<P, VisualizarView<P>> {

    public Visualizar() {
        super();
    }

    @Override
    protected boolean antes_cancelar(Object objeto) {
        return false;
    }

    @Override
    protected boolean antes_confirmar(Object objeto, List<Erro> erros) {
        /* nao confirma nada */
        return false;
    }

    /* ignora o super */
    @Override
    protected void apos_setObjeto(Object objeto) {}

    @Override
    public boolean isModal() {
        return true;
    }
}
