package paim.wingchun.controller;

import java.util.List;

import javax.swing.JComponent;

public interface HasConstrutor {

    /**
     * @author temujin Jun 23, 2011
     * @param objeto
     *            deve ser sempre um {@link List}
     * @param component
     *            sempre um {@link JComponent} que deve receber o objeto
     */
    void recebeObjeto(Object objeto, JComponent component) throws Exception;
}
