package paim.wingchun.controller.eventos;


import javax.swing.JPanel;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import paim.wingchun.controller.AbstractController;

/** @author paim 27/05/2011 */
public class DefaultModalInternalFrameEvent implements InternalFrameListener {

    private final JPanel glass;

    private final AbstractController<?, ?> abstractController;

    public DefaultModalInternalFrameEvent(AbstractController<?, ?> controlador, JPanel glass) {
        this.glass = glass;
        this.abstractController = controlador;
    }

    @Override
    public void internalFrameOpened(InternalFrameEvent e) {
        /* proibe minimizar, so maximiza */
        abstractController.getVisao().setIconifiable(false);
        abstractController.getVisao().setMaximizable(true);
        /* bloqueia a janela pai */
        abstractController.getPai().getVisao().setIconifiable(false);
        abstractController.getPai().getVisao().setClosable(false);
        abstractController.getPai().getVisao().setMaximizable(false);

        // System.out.println("ModalInternaFrameEventoOpened");
    }

    @Override
    public void internalFrameIconified(InternalFrameEvent e) {}

    @Override
    public void internalFrameDeiconified(InternalFrameEvent e) {}

    @Override
    public void internalFrameDeactivated(InternalFrameEvent e) {}

    @Override
    public void internalFrameClosing(InternalFrameEvent e) {}

    @Override
    public void internalFrameClosed(InternalFrameEvent e) {
        /* libera novamente a janela ancestral */
        abstractController.getPai().getVisao().setIconifiable(true);
        abstractController.getPai().getVisao().setClosable(true);
        abstractController.getPai().getVisao().setMaximizable(true);

        glass.setVisible(false);
        glass.removeAll();

    }

    @Override
    public void internalFrameActivated(InternalFrameEvent e) {}
}
