package paim.wingchun.controller.eventos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

/**
 * @author paim Jun 5, 2011
 */
public abstract class DefaultConstrutorEvent implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                constroi();

            }
        });
    }

    public abstract void constroi();

}
