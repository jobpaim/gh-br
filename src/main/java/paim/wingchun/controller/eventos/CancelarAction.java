package paim.wingchun.controller.eventos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;

import javax.swing.SwingUtilities;

import paim.wingchun.controller.AbstractController;

public class CancelarAction implements ActionListener {

    WeakReference<AbstractController<?, ?>> controlador;

    public CancelarAction(AbstractController<?, ?> controlador) {
        this.controlador = new WeakReference<AbstractController<?, ?>>(controlador);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {

                controlador.get().cancelar();

                if ( controlador.get().getPai() != null && controlador.get().isCancelarFechaFilho() )
                    controlador.get().getVisao().fechar();

            }
        });
    }

}
