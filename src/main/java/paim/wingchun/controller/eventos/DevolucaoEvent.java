package paim.wingchun.controller.eventos;

import java.util.EventObject;

/**
 * @author temujin Jun 24, 2011
 */
public class DevolucaoEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    private DevolucaoEvent(Object source) {
        super(source);
    }

    public DevolucaoEvent(Object component, Class<?> receptor, Class<?> emissor, Class<?> produto) {
       this(component);
    }

}
