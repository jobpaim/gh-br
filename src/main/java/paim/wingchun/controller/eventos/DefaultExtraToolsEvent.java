/** @author paim 08/03/2012 */
package paim.wingchun.controller.eventos;


import java.awt.Cursor;
import java.lang.ref.WeakReference;

import paim.wingchun.controller.AbstractSimpleController;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.componentes.toolbar.ExtraToolsListener;

/** @author paim 08/03/2012 */
public class DefaultExtraToolsEvent<P extends Pojo> implements ExtraToolsListener {

    WeakReference<AbstractSimpleController<P, ?>> controlador;

    public DefaultExtraToolsEvent(AbstractSimpleController<P, ?> controlador) {
        this.controlador = new WeakReference<AbstractSimpleController<P, ?>>(controlador);
    }

    @Override
    public boolean localizar() {
        controlador.get().getVisao().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        controlador.get().localizar();
        controlador.get().getVisao().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        return true;
    }

    @Override
    public boolean filtrar() {
        controlador.get().getVisao().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        // TODO
        controlador.get().getVisao().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        return true;
    }

    @Override
    public boolean limparFiltro() {
        // TODO
        return true;
    }

    @Override
    public boolean imprimir() {
        controlador.get().getVisao().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        // TODO
        controlador.get().getVisao().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        return true;
    }

    @Override
    public boolean listaFiltro() {
        controlador.get().getVisao().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        // TODO
        controlador.get().getVisao().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        return true;
    }
}
