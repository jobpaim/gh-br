/**
 * @author paim 03/11/2011
 */
package paim.wingchun.controller.eventos;

import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.RowFilter;

import paim.wingchun.app.WC_Log;
import paim.wingchun.view.FiltrarListener;
import paim.wingchun.view.TableFiltravel;
import paim.wingchun.view.componentes.table.WC_RowFilter;
import paim.wingchun.view.componentes.table.WC_TableModel;
import paim.wingchun.view.componentes.table.WC_TableRowSorter;

/**
 * @author paim 03/11/2011
 */
public class DefaultFiltrarEvent implements FiltrarListener {

    TableFiltravel visao;

    public DefaultFiltrarEvent(TableFiltravel visao) {
        this.visao = visao;
    }

    @Override
    public void filtrar() {
        String texto = visao.getTextFieldFiltro().getText();
        if ( texto == null || texto.length() == 0 )
            limparFiltro();
        else {
            try {
                RowFilter<WC_TableModel, Integer> rf = WC_RowFilter.stringFilter(texto, Pattern.LITERAL
                        | Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE, 0, 1, 2);

                ((WC_TableRowSorter) visao.getTable().getRowSorter()).setRowFilter(rf);
            }
            catch ( PatternSyntaxException e ) {
                WC_Log.getInstancia().getlogger()
                        .log(Level.INFO, e.getClass().getCanonicalName() + "\t" + e.getStackTrace()[0].toString());
            }
        }

    }

    @Override
    public void limparFiltro() {
        visao.getTextFieldFiltro().setText(null);
        ((WC_TableRowSorter) visao.getTable().getRowSorter()).setRowFilter(null);
    }

}
