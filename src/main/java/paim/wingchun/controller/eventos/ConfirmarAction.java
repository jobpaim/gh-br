package paim.wingchun.controller.eventos;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;

import javax.swing.SwingUtilities;

import paim.wingchun.controller.AbstractController;
import paim.wingchun.view.AbstractView;

public class ConfirmarAction implements ActionListener {

    WeakReference<AbstractController<?, ?>> abstractController;

    public ConfirmarAction(AbstractController<?, ?> controlador) {
        this.abstractController = new WeakReference<AbstractController<?, ?>>(controlador);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {

                boolean confirmado = abstractController.get().confirmar().equals(AbstractView.Estado.Confirmado);

                if ( confirmado && abstractController.get().getPai() != null
                                && abstractController.get().isConfirmarFechaFilho() ) {
                    abstractController.get().getVisao().fechar();
                }
            }
        });
    }

}
