package paim.wingchun.controller.eventos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;

import javax.swing.SwingUtilities;

import paim.wingchun.view.AbstractView;

public class FecharAction implements ActionListener {

    WeakReference<AbstractView<?>> visao;

    public FecharAction(AbstractView<?> visao) {
        this.visao = new WeakReference<AbstractView<?>>(visao);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO
        // visao.get().setCursor(Cursor.WAIT_CURSOR);
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                // TODO
                // visao.get().setCursor(Cursor.DEFAULT_CURSOR);
                visao.get().fechar();
            }
        });
    }
}
