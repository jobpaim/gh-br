package paim.wingchun.controller.eventos;


import java.lang.ref.WeakReference;

import paim.wingchun.controller.AbstractSimpleController;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.componentes.toolbar.ToolsListener;

/** @author paim 21/01/2011 */
public class DefaultToolsEvent<P extends Pojo> implements ToolsListener {

    WeakReference<AbstractSimpleController<P, ?>> controlador;

    public DefaultToolsEvent(AbstractSimpleController<P, ?> controlador) {
        this.controlador = new WeakReference<AbstractSimpleController<P, ?>>(controlador);
    }

    @Override
    public void excluir() {
        try {
            controlador.get().excluir();
        }
        catch ( Exception e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void incluir() {
        try {
            controlador.get().incluir();
        }
        catch ( Exception e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
