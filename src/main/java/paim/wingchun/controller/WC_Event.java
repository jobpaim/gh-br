package paim.wingchun.controller;


import java.util.EventObject;

import javax.swing.JComponent;

/**
 * @author paim 24/05/2011
 */

/* Declare the event. It must extend EventObject. */
public final class WC_Event extends EventObject {

    private static final long serialVersionUID = 1L;

    private transient JComponent component;

    private transient Object objeto;

    private transient Object oldValue;

    private transient Object newValue;

    private transient Enum<?> evento;

    private transient Object[] args;

    private WC_Event(Object source) {
        super(source);
    }

    public WC_Event(Enum<?> evento, Object objeto, Object oldValue, Object newValue, EventObject e,
                    JComponent component, Object... args) {
        this(e.getSource());
        this.component = component;
        this.evento = evento;
        this.objeto = objeto;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.args = args;
    }

    public WC_Event(Enum<?> evento, Object objeto, Object oldValue, Object newValue, EventObject e,
                    JComponent component) {
        this(evento, objeto, oldValue, newValue, e, component, (Object[]) null);
    }

    public WC_Event(Enum<?> evento, EventObject e, JComponent component, Object... args) {
        this(evento, null, null, null, e, component, args);
    }

    public WC_Event(Enum<?> evento, EventObject e, Object... args) {
        this(evento, null, null, null, e, null, args);
    }

    public WC_Event(Enum<?> evento, EventObject e) {
        this(evento, e, (Object[]) null);
    }

    @Deprecated
    public WC_Event(Enum<?> evento) {
        this(evento, null);
        throw new RuntimeException();
    }

    public JComponent getComponent() {
        return this.component;
    }

    public void setComponent(JComponent component) {
        this.component = component;
    }

    public Object getObjeto() {
        return this.objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public Object getOldValue() {
        return this.oldValue;
    }

    public void setOldValue(Object oldValue) {
        this.oldValue = oldValue;
    }

    public Object getNewValue() {
        return this.newValue;
    }

    public void setNewValue(Object newValue) {
        this.newValue = newValue;
    }

    public Enum<?> evento() {
        return this.evento;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object... args) {
        this.args = args;
    }
}
