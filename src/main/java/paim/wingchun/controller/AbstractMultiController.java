/** @author paim 30/03/2011 */
package paim.wingchun.controller;


import java.util.ArrayList;
import java.util.List;

import paim.wingchun.model.modelos.Model;
import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.AbstractMultiView;

/** controler para mais de um tipo de objeto (classes diferentes)
 *
 * @author paim 30/03/2011 */
@Deprecated
public abstract class AbstractMultiController<P extends Pojo, V extends AbstractMultiView<P>> extends
                AbstractController<P, V> {

    public AbstractMultiController(Class<P> pClass, Class<V> vClass, Class<? extends Model<P>> mClass) {
        super(pClass, vClass, mClass);
    }

    // public AbstractMultiController(AbstractController<P, ? extends AbstractView<P>, M> controladorPai, Class<V>
    // vClass) {
    // super(controladorPai, vClass);
    // }
    //
    // public AbstractMultiController(HasConstrutor controladorPai, Session sessaoPai, Pojo pojoPai, JComponent
    // componentePai,
    // Class<P> pClass, Class<V> vClass, Class<M> mClass) {
    // super(controladorPai, sessaoPai, pojoPai, componentePai, pClass, vClass, mClass);
    // }

    /** #################################################################### */

    /* TODO se tem filhos vais usar ou nao???, se usar, tem que alterar o inicializa, e provavelmente nao eh exclusivo
     * desse controlador provavelmente o outro controlador sera bem pior */
    private List<? extends Model<P>> modelos = new ArrayList<>();

    protected <M extends Model<P>> List<M> cInstanciaModelo(List<M> modelos) throws Exception {
        return null;
    }

    public final <M extends Model<P>> List<M> instanciaModelos() throws Exception {
        modelos = antesInstanciaModelos(modelos);
        modelos = cInstanciaModelos(modelos);
        modelos = aposInstanciaModelos(modelos);

        return (List<M>) modelos;
    }

    protected <M extends Model<P>> List<M> antesInstanciaModelos(List<M> modelos) {
        return modelos;
    }

    protected abstract <M extends Model<P>> List<M> cInstanciaModelos(List<M> modelos) throws Exception;

    protected <M extends Model<P>> List<M> aposInstanciaModelos(List<M> modelos) {
        return modelos;
    }

    /** --------------------------------------------------------------------------- */

    /* (non-Javadoc)
     *
     * @see paim.wingchun.controle.Controlador#cIncluir(paim.wingchun.controle.Controlador.Inclusao,
     * paim.wingchun.modelo.POJO) */
    @Override
    protected boolean c_incluir(P objeto) {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     *
     * @see paim.wingchun.controle.Controlador#cExcluir(boolean, paim.wingchun.modelo.POJO, java.util.List) */
    @Override
    protected boolean c_excluir(Object objeto, List<Erro> errosExclusao) {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     *
     * @see paim.wingchun.controle.Controlador#cSetListeners() */
    @Override
    protected void addListeners() throws Exception {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     *
     * @see paim.wingchun.controle.Controlador#cPopulaVisao() */
    @Override
    protected Object cPopulaVisao() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

}
