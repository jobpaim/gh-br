package paim.wingchun.controller;

import java.util.EventListener;



/**
 * @author paim 24/05/2011
 */

/* Declare the listener class. It must extend EventListener. */
/* A class must implement this interface to get WCEvent. */
public interface ControllerListener extends EventListener {

    public void eventOccurred(WC_Event evt);

}