package paim.wingchun.controller;


public class excecao extends RuntimeException {

    public excecao() {
        super();
        this.printStackTrace();
    }

    public excecao(String message, Throwable cause) {
        super(message, cause);
        this.printStackTrace();
    }

    public excecao(String message) {
        super(message);
        this.printStackTrace();
    }

    public excecao(Throwable cause) {
        super(cause);
        this.printStackTrace();
    }

}
