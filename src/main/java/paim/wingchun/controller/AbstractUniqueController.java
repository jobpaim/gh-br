package paim.wingchun.controller;


import java.util.List;

import paim.wingchun.controller.eventos.DefaultPanelButtonsEvent;
import paim.wingchun.model.modelos.Model;
import paim.wingchun.model.pojos.Erro;
import paim.wingchun.model.pojos.Pojo;
import paim.wingchun.view.AbstractUniqueView;

/** para um objeto pojo
 *
 * @author paim 25/03/2011
 * @param <P>
 * @param <V> */
public abstract class AbstractUniqueController<P extends Pojo, V extends AbstractUniqueView<P>> extends
                AbstractController<P, V> {

    protected AbstractUniqueController() {
        super();
    }

    protected <M extends Model<P>> AbstractUniqueController(Class<P> pClass, Class<V> vClass, Class<M> mClass) {
        super(pClass, vClass, mClass);
    }

    @Override
    protected final V c_instanciaVisao() throws Exception {
        return super.c_instanciaVisao();
        /* metodo deve acabar aqui nessa classe */
    }

    @Override
    protected final <M extends Model<P>> M c_instanciaModelo() throws Exception {
        return super.c_instanciaModelo();
        /* metodo deve acabar aqui nessa classe */
    }

    @Override
    protected boolean c_incluir(P objeto) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected boolean c_excluir(Object objeto, List<Erro> errosExclusao) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected void addListeners() throws Exception {
        getVisao().getjPanelBotoes().addListener(new DefaultPanelButtonsEvent(this));

    }

    protected abstract Long getIdInicial();

    @Override
    protected Object cPopulaVisao() throws Exception {
        Long id = getIdInicial();

        if ( getPai() != null ) {
            // TODO posicionar no objeto do controlador pai......
            throw new Exception("TODO: implemente-me");
        }
        /* situacao normal caso nao tem controlador pai */

        if ( id == null ) {
            return null;
        }
        else {
            /* buscando objeto mostrado */
            Pojo pojo = getModelo().get(getSessao(), (Long) id);
            return pojo;

            // /* popula visao com um pojo */
            // setObjeto(pojo);

            // if ( pojo.getPendencia() > 0 )
            // refreshErrosSlow(pojo);

        }
    }

}
