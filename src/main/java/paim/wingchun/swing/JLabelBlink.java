/**
 * @author paim 11/05/2010
 */
package paim.wingchun.swing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.Timer;

/**
 * @author paim 11/05/2010
 */
public class JLabelBlink extends JLabel implements ActionListener {

    private static final long serialVersionUID = 1L;

    private static final int BLINKING_RATE = 500;

    private boolean blinking = true;

    Color colorBlinkBG;

    Color colorBlinkFG;

    private Color colorNormalBG;

    private Color colorNormalFG;

    Timer timer;

    /** true se estiver na cor blink, falso na normal */
    private boolean estadoBlink = true;

    public JLabelBlink() {
        this("");
    }

    public JLabelBlink(String text) {
        super(text);
        /*garanto existencia de cores trocadas aqui*/
        this.colorBlinkBG = getForeground();
        this.colorBlinkFG = getBackground();
    }

    public JLabelBlink(String text, boolean blinking) {
        this(text);
        setBlinking(blinking);
    }

    public Timer getTimer() {
        return this.timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public void setBlinking(boolean blinking) {
        this.blinking = blinking;
        if ( blinking ) {
            /*guarda as cores*/
            colorNormalFG = this.getForeground();
            colorNormalBG = this.getBackground();
            if ( timer == null ) {
                /* timer padrao*/
                this.timer = new Timer(BLINKING_RATE, this);
                this.timer.setInitialDelay(0);
                this.timer.start();
            }
        }
        else {
            if ( this.timer != null ) {
                this.timer.stop();
                this.timer = null;
                /*garante que vai voltar a cor anterior*/
                setForeground(colorNormalFG);
                setBackground(colorNormalBG);
                estadoBlink = false;
            }
        }
    }

    public boolean isBlinking() {
        return this.blinking;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ( blinking ) {
            swap();
        }
        else {
            /*certificando -se que volta ao estado normal blinking off*/
            if ( estadoBlink ) {
                setForeground(colorNormalFG);
                setBackground(colorNormalBG);
                estadoBlink = false;
            }
        }
    }

    protected void swap() {
        if ( estadoBlink ) {
            setForeground(colorNormalFG);
            setBackground(colorNormalBG);
        }
        else {
            setForeground(colorBlinkFG);
            setBackground(colorBlinkBG);
        }
        /*inverte o estado*/
        estadoBlink = !estadoBlink;
    }

    public Color getBackgroundBL() {
        return this.colorBlinkBG;
    }

    public void setBackgroundBL(Color backgroundBL) {
        this.colorBlinkBG = backgroundBL;
    }

    public Color getForegroundBL() {
        return this.colorBlinkFG;
    }

    public void setForegroundBL(Color foregroundBL) {
        this.colorBlinkFG = foregroundBL;
    }

}