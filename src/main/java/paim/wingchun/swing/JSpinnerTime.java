package paim.wingchun.swing;

import java.awt.Dimension;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

/**
 * @author paim 18/05/2011
 */

public class JSpinnerTime extends JSpinner {

    private static final long serialVersionUID = 1L;

    public JSpinnerTime() {
        super();
        /* campo zerado */
         GregorianCalendar calendar = new GregorianCalendar(0, 0, 0, 0, 0, 0);
         Date earliestDate = calendar.getTime();
         /*number of minutes in a day - 1*/
         calendar.add(Calendar.MINUTE, 1439);
         Date latestDate = calendar.getTime();
         
         SpinnerDateModel model = new SpinnerDateModel(earliestDate, earliestDate, latestDate, Calendar.HOUR_OF_DAY);
         setModel(model);
         
         String pattern = "HH:mm";
         
//         String pattern = ((SimpleDateFormat) DateFormat.getDateInstance()).toPattern(); 
                 
         setEditor(new JSpinner.DateEditor(this, pattern));
         
         /*FIXME e o problema desse e que nao funciona direito*/

         //TODO ajustar aqui para nao girar senao troca o dia e pensa que eh outro time ver impacto em textfield
//        JFormattedTextField textField = ((JSpinner.DefaultEditor) getEditor()).getTextField();
//        
//        DefaultFormatterFactory dff = (DefaultFormatterFactory) textField.getFormatterFactory();
//        DateFormatter formatter = (DateFormatter) dff.getDefaultFormatter();
//        /* formatacao */
//        formatter.setFormat(new SimpleDateFormat("HH:mm"));
        /*FIXME o problema desse e que quando instancia, ele sai com a data toda?*/
        
        //XXX quando eh a primeira tela, banco vazio, parece que nao coloca so o time, coloca toda data, entao
//        textField.setText("00:00");
        
        
        
     // Disable keyboard edits in the spinner
//        JFormattedTextField tf =
//            ((JSpinner.DefaultEditor)this.getEditor()).getTextField();
//        tf.setEditable(false);
        
        Dimension d = new Dimension(70, 20);
        setPreferredSize(d);
        setMinimumSize(d);
        setMaximumSize(d);

    }
}