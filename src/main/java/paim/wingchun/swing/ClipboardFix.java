package paim.wingchun.swing;

import java.awt.HeadlessException;
import java.awt.datatransfer.Clipboard;

import javax.swing.JLabel;

public class ClipboardFix {

    public static void fixClipBoard() {
        Clipboard clipboard = getSystemSelection();
        if ( clipboard != null ) {
            try {
                clipboard.setContents(clipboard.getContents(null), null);
            }
            catch ( Exception e ) {}
        }
    }

    private static Clipboard getSystemSelection() {
        try {
            return (new JLabel()).getToolkit().getSystemSelection();
        }
        catch ( HeadlessException he ) {
            // do nothing... there is no system clipboard
        }
        catch ( SecurityException se ) {
            // do nothing... there is no allowed system clipboard
        }
        return null;
    }
}
