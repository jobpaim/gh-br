package paim.wingchun.swing;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;
/**  Drop Down Button.
 *
 * @author paim
 * 19/01/2011
 *
 */
public class JDropDownButton extends JButton implements ActionListener {

    private JPopupMenu popup = new JPopupMenu();

    protected Map<String, JComponent> botoesToolbar = new HashMap<String, JComponent>();

    private ActionListener mainButtonListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            Component component = popup.getComponent(0);
            if ( component instanceof JMenuItem ) {
                JMenuItem item = (JMenuItem) component;
                item.doClick(0);
            }
            if ( component instanceof JButton ) {
                JButton botao = (JButton) component;
                botao.doClick();
            }
        }
    };

    public JDropDownButton(Icon icon) {
        super(icon);
        init();
    }

    /**
     * Adiciona a configurações dos botões do menu dropdown.
     * [0] - btId, nome identificador do botão
     * [1] - icone
     * [2] - tooltip
     * [3] - ActionListener
     * @param itens
     */
    public void addBotoes(List<Object[]> itens) throws Exception {
        Iterator<Object[]> i = itens.iterator();
        while ( i.hasNext() ) {
            Object[] parametros = i.next();
            JMenuItem botaoInterno = criaItemBotaoDropDownToolBar(parametros);
            botaoInterno.setName((String) parametros[0]);
            this.addComponent(botaoInterno);
            botoesToolbar.put((String) parametros[0], botaoInterno);
        }
    }

    private JMenuItem criaItemBotaoDropDownToolBar(Object[] parametros) {
        JMenuItem jMenuItemItem = new JMenuItem(new ImageIcon(getClass().getResource((String) parametros[1])));
        jMenuItemItem.setText((String) parametros[2]);
        jMenuItemItem.addActionListener((ActionListener) parametros[3]);
        return jMenuItemItem;
    }

    /** Configura o estado do botão.  */
    public void setBotaoHabilitado(String btId, boolean estado) {
        botoesToolbar.get(btId).setEnabled(estado);
    }

    protected Border getRolloverBorder() {
        return BorderFactory.createEmptyBorder(0, 0, 0, 0);
    }

    private void init() {
        setRequestFocusEnabled(false);
        setRolloverEnabled(true);
        setRunFirstItem(true);
        this.addActionListener(this);
    }

    public void removeComponent(Component component) {
        popup.remove(component);
    }

    public Component addComponent(final Component component) {
        if ( component instanceof JButton ) {
            JButton button = (JButton) component;
            button.setRolloverEnabled(true);

            button.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseEntered(MouseEvent e) {
                    ((JButton) e.getComponent()).getModel().setRollover(true);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    ((JButton) e.getComponent()).getModel().setRollover(false);
                }
            });

            button.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    popup.setVisible(false);
                }
            });
        }
        return popup.add(component);
    }

    /**
     * Indicates that the first item in the menu should be executed
     * when the main button is clicked
     * @param isRunFirstItem True for on, false for off
     */
    public void setRunFirstItem(boolean isRunFirstItem) {
        this.removeActionListener(this);
        if ( !isRunFirstItem ) {
            this.addActionListener(this);
        }
        else
            this.addActionListener(mainButtonListener);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        JPopupMenu popup = getPopupMenu();
        popup.show(this, 0, this.getHeight());
    }

    protected JPopupMenu getPopupMenu() {
        return popup;
    }

}
