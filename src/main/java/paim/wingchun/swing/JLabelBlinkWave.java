/**
 * @author paim 11/05/2010
 */
package paim.wingchun.swing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.Timer;

/**
 * @author paim 11/05/2010
 */
public class JLabelBlinkWave extends JLabel implements ActionListener {

    private static final long serialVersionUID = 1L;

    private static final int BLINKING_RATE = 500;

    private boolean blinking = true;

    Color colorBlinkBG;

    Color colorBlinkFG;

    private Color colorNormalBG;

    private Color colorNormalFG;

    Timer timer;

    /** true se estiver na cor blink, falso na normal */
    private boolean estadoBlink = true;

    public JLabelBlinkWave() {
        this("");
    }

    public JLabelBlinkWave(String text) {
        super(text);
        /*garanto existencia de cores trocadas aqui*/
        this.colorBlinkBG = getForeground();
        this.colorBlinkFG = getBackground();
    }

    public JLabelBlinkWave(String text, boolean blinking) {
        this(text);
        setBlinking(blinking);
    }

    public Timer getTimer() {
        return this.timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    
   
   
      
   
      
   
    public void setBlinking(boolean blinking) {
        this.blinking = blinking;
        if ( blinking ) {
            /*guarda as cores*/            
            colorNormalFG = this.getForeground();
            colorNormalBG = this.getBackground();
            initColors();
            setForeground(colors[0]);
            if ( timer == null ) {
                /* timer padrao*/
                this.timer = new Timer(DELAY, this);
                this.timer.setInitialDelay(0);
                this.timer.start();
            }
        }
        else {
            if ( this.timer != null ) {
                this.timer.stop();
                this.timer = null;
                /*garante que vai voltar a cor anterior*/
                setForeground(colorNormalFG);
                setBackground(colorNormalBG);
                estadoBlink = false;
            }
        }
    }

    public boolean isBlinking() {
        return this.blinking;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ( blinking ) {
            swap();
        }
        else {
            /*certificando -se que volta ao estado normal blinking off*/
            if ( estadoBlink ) {
                setForeground(colorNormalFG);
                setBackground(colorNormalBG);
                estadoBlink = false;
            }
        }
    }


    protected void swap() {
        setForeground(colors[counter]);
        counter++;
        counter %= colors.length;
    }

    public Color getBackgroundBL() {
        return this.colorBlinkBG;
    }

    public void setBackgroundBL(Color backgroundBL) {
        this.colorBlinkBG = backgroundBL;
    }

    public Color getForegroundBL() {
        return this.colorBlinkFG;
    }

    public void setForegroundBL(Color foregroundBL) {
        this.colorBlinkFG = foregroundBL;
    }

    private static final int STEPS = 64;
    private static final int GREEN_VALUE = 0;
    private static final int DELAY = 12;
    private Color[] colors = new Color[STEPS];
    private int counter = 0;

    
    private void initColors()
    {
      for (int i = 0; i < colors.length; i++)
      {
        int value = (int)(255 * (Math.cos(2 * Math.PI * (double)(i) / colors.length)+1)/2);
        colors[i] = new Color(value, GREEN_VALUE, 255- value);
      }
    }
    
 
    
}