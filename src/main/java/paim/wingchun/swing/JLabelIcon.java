package paim.wingchun.swing;

import java.awt.FlowLayout;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * JLabel com icon<BR>
 * componente com 2 jlabel, um para o text e outro para icone, mas apena o icone implementa listener
 * 
 * @author paim 20/05/2011
 */
public class JLabelIcon extends JComponent implements SwingConstants {

    private static final long serialVersionUID = 1L;

    private JLabel labelText;

    private JLabel labelIcon;

    public JLabelIcon() {
        super();
        setLayout(new FlowLayout());
        add(getLabelText());
        add(getLabelIcon());
    }

    public JLabelIcon(String text) {
        this(text, null);
    }

    public JLabelIcon(Icon icon) {
        this(null, icon);
    }

    public JLabelIcon(String text, Icon icon) {
        this();
        setText(text);
        setIcon(icon);
    }

    private JLabel getLabelText() {
        if ( labelText == null ) {
            labelText = new JLabel();
            labelText.setHorizontalAlignment(LEFT);
            labelText.setHorizontalTextPosition(LEFT);

        }
        return labelText;
    }

    private JLabel getLabelIcon() {
        if ( labelIcon == null ) {
            labelIcon = new JLabel();
            labelIcon.setHorizontalAlignment(RIGHT);
            labelIcon.setIcon(null);
        }
        return labelIcon;
    }

    public void setText(String text) {
        getLabelText().setText(text);

    }

    public Icon getIcon() {
        return getLabelIcon().getIcon();
    }

    /**
     * especifica qual o icone a ser usado
     * 
     * @author paim 19/05/2011
     * @param icon
     *            void
     */
    public void setIcon(Icon icon) {
        getLabelIcon().setIcon(icon);
    }

    public void setHorizontalAlignmentIcon(int alignment) {
        getLabelIcon().setHorizontalAlignment(alignment);
    }

    public void setHorizontalAlignmentText(int alignment) {
        getLabelText().setHorizontalAlignment(alignment);
    }

    @Override
    public void addMouseListener(MouseListener listener) {
        getLabelIcon().addMouseListener(listener);
    }

    @Override
    public void addFocusListener(FocusListener listener) {
        getLabelIcon().addFocusListener(listener);
    }

    @Override
    public void removeMouseListener(MouseListener listener) {
        getLabelIcon().removeMouseListener(listener);
    }

    @Override
    public void removeFocusListener(FocusListener listener) {
        getLabelIcon().removeFocusListener(listener);
    }

}
