/**
 * @author paim 28/03/2011
 */
package paim.wingchun.app;


/**
 * @author paim 28/03/2011
 */
public interface TemEstado {

    public enum Estado0 {};

    enum Estado {
        ALTERADO, INALTERADO
    }


    //    public Estados estado;

    public Estado getEstado();

    public void changeEstado(Estado estado);

    public <E extends Estado> void chan(E xx);


    // @Deprecated
    // private Estado estado;
    //
    // @Deprecated
    // public Estado getEstado() {
    // return estado;
    // }
    //
    // @Deprecated
    // private void changeEstado(Estado newEstado) {
    // this.estado = newEstado;
    // }
}
