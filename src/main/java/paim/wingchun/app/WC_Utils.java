/** @author paim 12/05/2011 */
package paim.wingchun.app;


import java.text.NumberFormat;
import java.util.List;
import java.util.stream.Collectors;

import paim.wingchun.model.pojos.Pojo;

/** @author paim 12/05/2011 */
public class WC_Utils {

    public static enum Campo {
        NENHUM("Nenhum"), CPF("CPF"), CNPJ("CNPJ"), CPF_CNPJ("CPF ou CNPJ"), CEP("CEP"), EMAIL("EMAIL");

        private String tipo;

        private Campo(String tipo) {
            this.tipo = tipo;
        }

        public String getTipo() {
            return tipo;
        }
    }

    public static enum Atributo {
        PADRAO
    }

    // public class Pojos {
    /** verifica se dois objetos sao iguais
     *
     * @author paim 14/12/2011
     * @param esse
     * @param outro
     * @return boolean */
    @Deprecated
    public static boolean iguaisOLD(Object esse, Object outro) {
        if ( esse == null ) {
            if ( outro != null )
                return false;
        }
        else if ( !esse.equals(outro) )
            return false;
        return true;
    }



    public static String strToHtml(String str) {
        // Coloca o tag <html> no inï¿½cio da mensagem, se nï¿½o existir
        if ( !str.toLowerCase().startsWith("<html>") )
            str = "<html>" + str;

        // Coloca o tag </html> no final da mensagem, se nï¿½o existir
        if ( !str.toLowerCase().endsWith("</html>") )
            str = str + "</html>";

        // Substitui todos os '\n' pelo tag <br> para forï¿½ar quebra de linha
        str = str.replaceAll("\\n", "<br>");

        return str;
    }

    public static String insereZerosAEsquerda(Object valor, int nroCasas) {
        String textoValor = valor.toString();

        int nroZeros = nroCasas - textoValor.length();

        if ( nroZeros > 0 ) {
            String zeros = "";

            for ( int i = 0; i < nroZeros; i++ )
                zeros += "0";

            return zeros.concat(textoValor);

        }
        else {
            return textoValor;
        }
    }

    public static String formatarValorDecimal(Number valorAtributo, int nroCasasDecimais) {
        if ( valorAtributo != null ) {
            NumberFormat formato = NumberFormat.getInstance();
            /* Fixa o numero de casas decimais */
            formato.setMinimumFractionDigits(nroCasasDecimais);
            formato.setMaximumFractionDigits(nroCasasDecimais);

            return formato.format(valorAtributo);
        }
        else
            return "";
    }

    /** true se 2 objetos sao iguais, incluido null usar objects
     *
     * @author paim 12/05/2011
     * @param esse
     * @param outro
     * @return boolean */
    @Deprecated
    public static boolean iguais(Object esse, Object outro) {
        if ( esse == null && outro == null )
            return true;

        if ( esse != null && outro != null && esse.equals(outro) )
            return true;

        return false;
    }

    public static List<Long> ids(List<? extends Pojo> pojos) {
        return pojos.stream().map(o -> o.getId()).collect(Collectors.toList());
    }
}
