package paim.wingchun.app;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;



/** Log eventos de Camel e app
 * @author paim
 * 09/05/2011
 *
 */
public class WC_Log{
    /** caso nao queira mensagem na console...., altere o field */
    private final boolean useConsole = true;

    private final String output =  WC_File.appHomePath()+ Constantes.App$Sigla + ".log";

    private static WC_Log instancia;

    private Logger logger = Logger.getLogger(WC_Properties.getProperty("app.nome"));

    private FileHandler fh = null;

    public WC_Log() throws SecurityException, IOException {
        fh = new FileHandler(output, 256*1024, 5, false);
        /*This block configure the logger with handler and formatter*/
        logger.addHandler(fh);
        logger.setLevel(Level.ALL);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
        instancia = this;
        logger.setUseParentHandlers(this.useConsole);
        logger.log(Level.INFO, "sistema de log inicializado");
    }

    public static WC_Log getInstancia() {
        return instancia;
    }

    public FileHandler getFileHandler() {
        return fh;
    }

    public Logger getlogger() {
        return logger;
    }

    public void done() {
        WC_Log.getInstancia().getlogger().log(Level.INFO, "Finalizando sistema de log");
        WC_Log.getInstancia().getFileHandler().flush();
        WC_Log.getInstancia().getFileHandler().close();
    }

    static void loga2(Level level, String msg) {
        /* log pode nao estar disponivel nesse momento */
        if ( instancia == null )
            System.out.println(level + ": " + msg);
        else
            instancia.getlogger().log(Level.SEVERE, msg);
    }

    public static void info(String msg) {
        loga(Level.INFO, msg);
    }

    public static void warning(String msg) {
        loga(Level.WARNING, msg);
    }

    public static void severe(String msg) {
        loga(Level.SEVERE, msg);
    }

    public static void severe(Throwable thrown) {
        loga(Level.SEVERE, thrown.getMessage(), thrown);
    }

    public static void severe(String msg, Throwable thrown) {
        loga(Level.SEVERE, msg, thrown);
    }


    public static void loga(Level level, String msg) {
        instancia.getlogger().log(level, msg);
    }

    public static void loga(Level level, String msg, Throwable thrown) {
        instancia.getlogger().log(level, msg, thrown);
    }
}
