package paim.wingchun.app;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;


public class WC_File {

    private static String appHomePath;

    /**
     * retorna o dir onde ficam todas informacoes sobre apliccao, como banco, log, preferencias? ....
     *
     * @author paim 05/05/2011
     * @return String
     */
    public static String appHomePath() {
        /* se a raiz já está setada, então retorne a */
        if ( appHomePath == null ) {
            appHomePath = System.getProperty("user.home") + File.separatorChar + "." + Constantes.App$Sigla
                    + File.separatorChar;
            File fd = new File(appHomePath);
            /* se nao existe entao criar */
            if ( !(fd.exists() && fd.isDirectory()) ) {
                System.out.println("criando app Home Path: [" + appHomePath + "]");
                fd.mkdir();
            }
        }
        return appHomePath;
    }

    public static String appPath() {
        URL url = WC_File.class.getResource("/");
        return url.getPath();
    }

    /**
     * retorna o dir onde foi instalada a aplicacao
     *
     * @author paim 05/05/2011
     * @return String
     */
    @Deprecated
    public static String getAppInstallPath() {
        URL url = File.class.getResource(WC_Properties.App$Properties);
        if ( url == null )
            return null;
        return url.getPath();
    }

    @Deprecated
    public static File getPathApp() {
        String path;
        URL url = WC_Properties.class.getResource("aplicacao.properties");

        if ( url == null )
            path = System.getProperty("user.dir");
        else {
            URI uri = null;
            try {
                uri = new URI(url.getFile());
                path = WC_File.extraiPath(uri.getPath());
            }
            catch ( URISyntaxException e ) {
                path = System.getProperty("user.dir");
            }
        }

        return new File(path);
    }

    public static String extraiPath(String nomeArquivo) {
        /* trata paths passados de url */
        if ( nomeArquivo.startsWith("file:") )
            nomeArquivo = nomeArquivo.substring(5);

        /* trata paths passados do jar */
        int posjar = nomeArquivo.indexOf('!');
        if ( posjar != -1 )
            nomeArquivo = nomeArquivo.substring(0, posjar);

        File file = new File(nomeArquivo);
        return nomeArquivo.substring(0, nomeArquivo.lastIndexOf(file.getName()));
    }
}
