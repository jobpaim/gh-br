/**
 * @author paim 23/11/2011
 */
package paim.wingchun.app;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * @author paim 23/11/2011
 */
public class Collections {

    @SafeVarargs
    public static <T> T[] arrayMerge(T[]... arrays) {
        /* Determine required size of new array */

        int count = 0;
        for ( T[] array : arrays ) {
            count += array.length;
        }

        /* create new array of required class */

        @SuppressWarnings("unchecked")
        T[] mergedArray = (T[]) Array.newInstance(arrays[0][0].getClass(), count);

        /* Merge each array into new array */

        int start = 0;
        for ( T[] array : arrays ) {
            System.arraycopy(array, 0, mergedArray, start, array.length);
            start += array.length;
        }
        return mergedArray;
    }

    /**
     *  baseado em {@link EnumSet#of(Enum, Enum...)}
     * @author paim 23/11/2011
     * @param first
     * @param rest
     * @return List<T>
     */
    @SafeVarargs
    public static <T> List<T> listof(T first, T... rest) {
        List<T> result = new ArrayList<>();
        result.add(first);
        for ( T e : rest )
            result.add(e);
        return result;
    }

}
