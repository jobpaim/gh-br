package paim.wingchun.app;

import java.text.Normalizer;
import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.MaskFormatter;

import paim.wingchun.model.pojos.Erro;

/**
 * alguns utilitarios
 *
 * @see org.apache.commons.lang.StringUtils
 * @author paim 13/05/2011
 */
public class WC_Strings {

    /** Remove todas tags html da string e converte os caracteres extendidos. */
    public static String htmlTrim(String html) {
        if ( html == null )
            return null;
        String texto = html.replaceAll("<[^>]*>", "").replaceAll("\r\n", "").trim();
        Pattern p = Pattern.compile("&#[0-9]*;");
        Matcher m = p.matcher(texto);
        while ( m.find() ) {
            String valor = m.group();
            valor = valor.substring(2, valor.length() - 1);
            texto = texto.replaceAll(m.group(), String.valueOf((char) Integer.parseInt(valor)));
        }
        return texto;
    }

    public static String removeAcentos(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[^\\p{ASCII}]", "");
        return s;
    }

    /**
     * String esta vazia?
     *
     * @author paim 13/05/2011
     * @param s
     * @return boolean
     */
    public static boolean isEmpty(String s) {
        if ( s.isEmpty() || htmlTrim(s).trim().equals("") )
            return true;
        return false;
    }

    public static String formataLista(List<String> lista, String titulo, String prefixo, String sufixo) {
        StringBuilder result;

        if ( titulo == null )
            result = new StringBuilder();
        else
            result = new StringBuilder(titulo);

        for ( Iterator<String> iterator = lista.iterator(); iterator.hasNext(); ) {
            String s = (String) iterator.next();
            if ( prefixo != null )
                result.append(prefixo);

            result.append(s);

            if ( iterator.hasNext() )
                result.append(sufixo);
        }
        return result.toString();
    }

    public static String formataLista(List<String> lista) {
        return formataLista(lista, null, null, ", ");
    }

    public static String formataLista(String titulo, List<String> lista) {
        return formataLista(lista, titulo, "\t", "\n");
    }

    public static StringBuilder formataErros(Collection<Erro> lista) {
        StringBuilder result = new StringBuilder();
        for ( Erro erro : lista ) {
            result.append("\t");
            result.append(erro.getRotuloField());
            result.append(": ");
            result.append(erro.getNome());
            result.append("\n");
        }
        return result;
    }

    public static String formataMap(String titulo, Map<Object, Object> map) {
        StringBuilder result = new StringBuilder(titulo);

        for ( Object k : map.keySet() ) {
            result.append("\t").append(k);
            result.append(" = ");
            result.append(map.get(k)).append("\n");
        }

        return result.toString();
    }

    public static String substParametros(String s, String[] parametros) {
        byte count = 1;
        for ( String param : parametros ) {

            String regex = Matcher.quoteReplacement("<$" + count + ">");
            String replacement = Matcher.quoteReplacement(param);

            s = s.replaceAll(regex, replacement);
            count++;
        }

        return s;
    }

    // FIXME esse metodo nao deveria estar nessa classe, pois depende do swing ok.
    public static String formatarString(String texto, String mascara) throws ParseException {
        MaskFormatter mf = new MaskFormatter(mascara);
        mf.setValueContainsLiteralCharacters(false);
        return mf.valueToString(texto);
    }

}
