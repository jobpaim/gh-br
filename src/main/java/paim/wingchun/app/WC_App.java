package paim.wingchun.app;


import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.SplashScreen;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.BindException;
import java.nio.channels.FileLock;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hsqldb.Server;
import org.hsqldb.persist.HsqlDatabaseProperties;
import org.hsqldb.server.ServerConstants;

import com.jgoodies.looks.Options;
import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import com.jgoodies.looks.plastic.theme.Silver;

import paim.wingchun.controller.AbstractController;
import paim.wingchun.view.AbstractView;
import paim.wingchun.view.Dialogs;
import paim.wingchun.view.GUIUtils;
import paim.wingchun.view.IVisaoApp;

import static paim.wingchun.app.WC_Properties.getProperty;

public abstract class WC_App<VisaoApp extends IVisaoApp> extends WindowAdapter {

    /** reconhece informacoes sobre aplicacao, ininicaliza sistema de LOG ou cai fora */
    static {
        try {
            WC_Properties.verificaPropertiesApp();

            /* simplismente inicializa constantes dependentes das propriedades da aplicacao */
            new Constantes();

            /* verificar dir home da aplicacao, */
            System.out.println("OK. app Home Path: [" + WC_File.appHomePath() + "]");

            /* inicia sistema de log */
            new WC_Log();
        }
        catch ( Throwable e ) {
            e.printStackTrace();
            throw new Error("Aplicacao nao pode ser inicializada");
        }
        if ( WC_Log.getInstancia() == null )
            throw new Error("Log não inicializou");
        WC_Log.getInstancia().getlogger().log(Level.INFO, "iniciando aplicação");
    }

    protected static WC_App<?> instancia;

    protected static FileLock fileLock;

    protected VisaoApp visaoApp;

    public static Session sessao;

    /* lista de controladores ativos */
    protected static Set<AbstractController<?, ?>> abstractControllers = new HashSet<>();

    protected AbstractView<?> hasFocus;

    private volatile boolean saindo = false;

    private transient SplashScreen splash;

    private transient Graphics2D splashGraphics2D;

    /** versao */
    public static final String App$Versao = Constantes._versao + "." + Constantes._subversao + "."
                    + Constantes._release;

    // private static HelpCamel help;

    protected void renderSplashFrame(String msg) {
        if ( splashGraphics2D != null ) {
            splashGraphics2D.setComposite(AlphaComposite.Clear);
            splashGraphics2D.fillRect(120, 140, 200, 40);
            splashGraphics2D.setPaintMode();
            splashGraphics2D.setColor(Color.BLACK);
            splashGraphics2D.drawString("Loading " + msg + "...", 120, 150);
            splash.update();
        }
    }

    protected void initializeSplash() {
        splash = SplashScreen.getSplashScreen();
        if ( splash == null )
            WC_Log.getInstancia().getlogger().log(Level.WARNING, "SplashScreen.getSplashScreen() returned null");
        else
            splashGraphics2D = splash.createGraphics();
    }

    /** @author paim 02/03/2012 void */
    protected void splashStop() {
        if ( splash != null )
            splash.close();
    }

    protected WC_App() {
        instancia = this;

        initializeSplash();

        renderSplashFrame("HSQLDB");
        if ( !initializeHSQLDB() ) {
            /* tonto cocoduche! */
            splashStop();
            System.exit(1);
        }
        renderSplashFrame("Hibernate");
        if ( !initializeHibernate() ) {
            /* tonto cocoduche! */
            splashStop();
            System.exit(1);
        }

        antesInstanciaVisao();
        renderSplashFrame("Visão");
        instanciaVisao();
        aposInstanciaVisao();

        if ( !initializeUIManager() )
            /* tonto cocoduche! */
            System.exit(1);
    }

    public static WC_App<?> getInstancia() {
        return instancia;
    }

//    public static synchronized void setSessao(Session sessao) {
//        WC_App.sessao = sessao;
//    }

//    public static synchronized Session getSessao() {
//        return sessao;
//    }

    protected boolean initializeUIManager() {
        boolean lafOK = false;

        WC_Log.getInstancia().getlogger().log(Level.INFO, "sistema   ----" + System.getProperty("os.name") + "----");
        /* http://mindprod.com/jgloss/properties.html#OSNAME */
        /* http://bugs.sun.com/view_bug.do?bug_id=6819886 */
        try {

            PlasticXPLookAndFeel laf = new PlasticXPLookAndFeel();
            PlasticLookAndFeel.setCurrentTheme(new Silver());
            PlasticLookAndFeel.setTabStyle(PlasticLookAndFeel.TAB_STYLE_METAL_VALUE);
            PlasticLookAndFeel.setHighContrastFocusColorsEnabled(true);
            PlasticLookAndFeel.set3DEnabled(true);


            /* tenta colocar esse LookAndFeel */
            UIManager.setLookAndFeel(laf);
            lafOK = true;
        }
        catch ( UnsupportedLookAndFeelException e ) {
            WC_Log.getInstancia().getlogger().log(Level.WARNING, e.getMessage(), e);

            try {
                /* tenta colocar o padrao para esse sistema */
                UIManager.setLookAndFeel(Options.getSystemLookAndFeelClassName());
                lafOK = true;
            }
            catch ( ClassNotFoundException | InstantiationException | IllegalAccessException
                            | UnsupportedLookAndFeelException e1 ) {
                WC_Log.getInstancia().getlogger().log(Level.SEVERE, e.getMessage(), e);
            }

        }
        return lafOK;
    }

    /** retorna true se pode instanciar aplicacao */
    protected static boolean antesInstanciarApp(String[] args) {
        if ( !initializeFileLock() )
            return false;
        return true;
    }

    /** procedimentos antes de sair da aplicacao */
    public void antesSairApp() {
        WC_App.getInstancia().finalizeFileLock();
        WC_App.getInstancia().finalizeHSQLDB();

    }

    public void sairApp() {
        setSaindo(true);

        WC_Log.getInstancia().getlogger().log(Level.INFO, "Shutting down...");

        if ( WC_App.getInstancia() == null )
            System.exit(1);

        // Controle.getInstancia().fecharDeclaracao(true);
        Thread t = new Thread("SairApp") {

            @Override
            public void run() {
                try {
                    WC_App.getInstancia().antesSairApp();

                    sleep(300);

                    if ( WC_App.getInstancia().getVisaoApp() != null )
                        ((JFrame) WC_App.getInstancia().getVisaoApp()).dispose();
                    // TODO colocar janelina progresso bla bla em thread aqui

                }
                catch ( InterruptedException e ) {
                    WC_Log.getInstancia().getlogger().log(Level.SEVERE, e.getMessage(), e);
                }
                finally {
                    /* finaliza LOG */
                    WC_Log.getInstancia().done();
                    /* sai mesmo */
                    System.exit(0);
                }
            }
        };
        t.start();

        // while ( t.isAlive() ) {
        // System.out.println("ta");
        // }

    }

    public synchronized void setSaindo(boolean saindo) {
        this.saindo = saindo;
    }

    public synchronized boolean isSaindo() {
        return this.saindo;
    }

    public static Set<AbstractController<?, ?>> getControladors() {
        return abstractControllers;
    }

    protected abstract Class<VisaoApp> getVClass();

    public void instanciaVisao() {
        try {
            visaoApp = getVClass().newInstance();
        }
        catch ( InstantiationException | IllegalAccessException e ) {
            WC_Log.getInstancia().getlogger().log(Level.SEVERE, e.getMessage(), e);
        }

        ((JFrame) visaoApp).setExtendedState(Frame.MAXIMIZED_BOTH);
        ((JFrame) visaoApp).addWindowListener(this);

        // TODO inicializar tooltip
        ToolTipManager.sharedInstance().setInitialDelay(500);
        ToolTipManager.sharedInstance().setReshowDelay(5000);
        ToolTipManager.sharedInstance().setDismissDelay(20000);
        ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);

        // FIXME colocar fim de splash aqui
        // if(splashScreen != null) {
        // splashScreen.stop();
        // splashScreen.dispose();
        // splashScreen = null;
        // }

        ((Window) visaoApp).setVisible(true);

        /* o listener vai resolver */
        ((JFrame) visaoApp).setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        // ((JFrame) visaoApp).setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        /** Encerra a aplicacao ao fechar a janela */
        // this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    protected void aposInstanciaVisao() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                StringBuilder titulo = new StringBuilder();
                titulo.append(Constantes.App$Sigla);
                titulo.append("  ");
                titulo.append(getProperty("app.nome"));
                titulo.append("  ");
                titulo.append(App$Versao);
                if ( !Constantes.ESTAGIO.equals(Estagio.OMEGA) ) {
                    titulo.append(" - ");
                    titulo.append(Constantes.ESTAGIO.toString());
                }

                ((Frame) WC_App.this.getVisaoApp()).setTitle(titulo.toString());

                ImageIcon imageIcon = GUIUtils.getImageIcon("app.icon");
                ((Frame) getVisaoApp()).setIconImage(imageIcon.getImage());
            }
        });

    }

    protected void antesInstanciaVisao() {}

    public AbstractView<?> getHasFocus() {
        return this.hasFocus;
    }

    public void setHasFocus(AbstractView<?> hasFocus) {
        this.hasFocus = hasFocus;
    }

    public VisaoApp getVisaoApp() {
        return this.visaoApp;
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.out.println("Janela aplicação " + Constantes.App$Sigla + " Fechada");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        WC_Log.getInstancia().getlogger().log(Level.INFO, "Fechando janela Aplicação " + Constantes.App$Sigla);
        sairApp();
    }

    private static boolean initializeFileLock() {
        try {
            RandomAccessFile raf = new RandomAccessFile(Constantes.App$LockFile, "rw");
            fileLock = raf.getChannel().tryLock();
            if ( fileLock == null ) {
                Dialogs.erro(null, "Aplicacao já está em execução", Constantes.App$Sigla + " " + App$Versao);
                System.exit(1);
                return false;
            }
            return true;
        }
        catch ( Throwable e ) {
            WC_Log.getInstancia().getlogger().log(Level.SEVERE, e.getMessage(), e);
            Dialogs.erro(null, WC_Properties.getProperty("app.nome"), "arquivo TryLock.txt ");
            System.exit(1);
            return false;
        }
    }

    private void finalizeFileLock() {
        if ( fileLock == null )
            return;

        try {
            fileLock.release();
            fileLock.channel().close();
            new File(Constantes.App$LockFile).delete();
        }
        catch ( IOException e ) {
            WC_Log.getInstancia().getlogger().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void finalizeHSQLDB() {
        try {
            /* Se o banco ainda nao fechou, entao tenta fechar! */
            if ( HyperSQLsisOnline() ) {
                WC_Log.getInstancia().getlogger().log(Level.INFO, "Shutdown HSQLDB");
                HyperSQLsshutdown();
            }
        }
        catch ( Exception e ) {
            WC_Log.getInstancia().getlogger().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private boolean initializeHSQLDB() { // TODO log
        try {
            startHSQLDB(getProperty("app.banco"),getProperty("app.banco.usuario"), getProperty("app,banco.senha") );
            /* se nao esta executando... */
            if ( !HyperSQLsisOnline() ) {
                /* FIXME vai chegar aqui se tiver OUTRA aplicacao com hsqldb rodando */
                servidor.getServerError();
                throw new Exception(servidor.getServerError());
            }
        }
        catch ( BindException e ) {
            Dialogs.menssagem("O sistema já está em execução neste computador."
                            + "\nFeche a instância atual antes de iniciar outra execuïção.", "Erro na inicialização");
            return false;
        }
        catch ( Throwable e ) {
            // FIXME splashcreen
            // if(splashScreen != null) {
            // splashScreen.stop();
            // splashScreen.dispose();
            // }
            // splashScreen = null;
            Dialogs.erro("Erro ao iniciar banco de dados local." + "\nDescrição do erro: " + e.getMessage(),
                            "Erro na inicialização");
            WC_Log.getInstancia().getlogger().log(Level.SEVERE, e.getMessage(), e);
            return false;
        }
        return true;
    }

    //TODO tornar final isso
    private SessionFactory sessionFactory;
    private boolean initializeHibernate() {
        try {
            /* A SessionFactory is set up once for an application!*/
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
            // configures settings from hibernate.cfg.xml
            try {
                sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            }
            catch ( Exception e ) {
                /* The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory so
                 * destroy it manually. */
                StandardServiceRegistryBuilder.destroy(registry);
            }
            /* obtem sessao hibernate. */
            sessao = sessionFactory.openSession();
            /* Testa se retorna conexao com banco */
            if ( !sessao.isConnected()  )
                return false;
        }
        catch ( Exception e ) {
            Dialogs.menssagem("Conex\u00e3o com o Banco de Dados falhou!\n" + e.getMessage(), "Informa\u00e7\u00f5es do HSQLDB");
            WC_Log.getInstancia().getlogger().log(Level.SEVERE, e.getMessage(), e);
            return false;
        }
        return true;
    }
    public SessionFactory sessionFactory() {
        return sessionFactory;
    }
    public Session openSession() {
        return sessionFactory.openSession();
    }
    /** Informa o tipo de formato utilizado pelo script do banco (0 : texto , 1 : binario, 3 : binario comprimido)) */
    public static final byte FORMATO_SCRIPT_BD = (Constantes.ESTAGIO == Estagio.ALPHA ? (byte) 0 : (byte) 3);
    static Server servidor;
    public static void startHSQLDB( String nomeBanco, String user, String senha)
                    throws Throwable {
        boolean modoServidor = true;
        if ( HyperSQLsisOnline() )
            return;

        if ( modoServidor ) {
            servidor = new Server();
            /* Desabilita o log do hsqldb */
            // servidor.setLogWriter(null);
            // servidor.setErrWriter(null);
            // servidor.setTrace(false);
            servidor.setSilent(true);
            servidor.setAddress("localhost");
            servidor.setPort(ServerConstants.SC_DEFAULT_HSQL_SERVER_PORT);
            servidor.setDatabaseName(0, nomeBanco);
            /* Seta o caminho do banco informando o formato do script DDL utilizado */
            servidor.setDatabasePath(0, WC_File.appHomePath() + nomeBanco + ";"
                            + HsqlDatabaseProperties.hsqldb_script_format + "=" + FORMATO_SCRIPT_BD);
            servidor.start();
        }
        else {
            Class.forName("org.hsqldb.jdbcDriver").newInstance();
            DriverManager.getConnection(url_file() + nomeBanco, user, senha);
        }
    }

    public static boolean HyperSQLsisOnline() {
      if ( servidor != null )
          return ServerConstants.SERVER_STATE_ONLINE == servidor.getState();

      return false;
  }

    public static void HyperSQLsshutdown() throws SQLException, InterruptedException {
        /* http://melloware.com/products/jukes/xref/com/melloware/jukes/db/Database.html */
        if ( servidor != null ) {
            HyperSQLsexecute("SHUTDOWN COMPACT");

            // FIXME o finalizar
            // connection.close();
            // st.close();
            // connection.commit();
            // session.close();
            // session.disconnect();
            // servidor.shutdown();
            servidor.stop();

            while ( servidor.getState() != ServerConstants.SERVER_STATE_SHUTDOWN ) {
                WC_Log.getInstancia().getlogger().log(Level.INFO, "Sleeping waiting for server to stop");
                Thread.sleep(100);
            }

        }
        servidor = null;
    }

    static String url() {
        return "jdbc:hsqldb:hsql://" + servidor.getAddress() + ":" + servidor.getPort() + "/"
                        + servidor.getDatabaseName(0, true);
    }

    static String url_file() {
        return "jdbc:hsqldb:file:" + WC_File.appHomePath();
    }

    public static Connection getConnection(String nomeBanco, String portaTCP, String user, String senha)
                    throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        String url;
        if ( HyperSQLsisOnline() )
            url = url();
        else
            url = url_file() + nomeBanco;

        Class.forName("org.hsqldb.jdbcDriver").newInstance();
        return DriverManager.getConnection(url, user, senha);
    }

    public static void HyperSQLsdefrag(Connection connection) throws SQLException {
        HyperSQLsexecute(connection, "CHECKPOINT DEFRAG");
    }

    public static void HyperSQLsexecute(String sql) throws SQLException {
        Session session = WC_App.getInstancia().openSession();
        //TODO novohibernate ajustar
//        Connection connection = session.connection();
        Connection connection = null;
        Statement statement = connection.createStatement();


        statement.execute(sql);
    }

    public static void HyperSQLsexecute(Connection connection, String sql) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(sql);
    }


//    Connection con;
//    public void initxyz() throws Exception{
//        try {
//            Class.forName("org.hsqldb.jdbc.JDBCDriver");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace(System.out);
//        }
//        try {
//            con=DriverManager.getConnection("jdbc:hsqldb:mydatabase","SA","");
//            con.createStatement().executeUpdate("create table contacts (name varchar(45),email varchar(45),phone varchar(45))");
//        } catch (SQLException e) {
//            e.printStackTrace(System.out);
//        }
//    }

}
