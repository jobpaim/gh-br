package paim.wingchun.app;

import java.util.GregorianCalendar;


@Deprecated
public final class Constantes {

    public static final long DATA_INVALIDA = new GregorianCalendar(1500, 01, 01).getTimeInMillis();

    public static final Byte _versao = Byte.valueOf(WC_Properties.getProperty("app.versao"));

    public static final Byte _subversao = Byte.valueOf(WC_Properties.getProperty("app.subversao"));

    public static final Byte _release = Byte.valueOf(WC_Properties.getProperty("build.number", "0"));

    public static final String App$Sigla = WC_Properties.getProperty("app.sigla");

    public static final Estagio ESTAGIO = Estagio.valueOf(Estagio.class, WC_Properties.getProperty("app.estagio"));

    public static final String App$LockFile = WC_File.appHomePath() + App$Sigla + ".tryLock";

    Constantes() {}
}
