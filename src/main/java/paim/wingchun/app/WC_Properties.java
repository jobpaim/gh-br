package paim.wingchun.app;


import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;

/* System.getProperty("user.dir"); */
/* System.getProperty("user.home"); */
/* System.getProperty("file.separator"); */
public class WC_Properties {

    private static final String WCApp$Properties = "/wingchun.properties";

    /** constante basica para localizar de toda estrutura da aplicacao */
    public static final String App$Properties = "/aplicacao.properties";

    private static Properties properties;

    private static Properties mensagens;

    /** identifica as properties do wc e da aplicacao, necessarias para o frame wc trabalhar
     *
     * @author paim 05/05/2011
     * @throws Exception
     *             void */
    public static void verificaPropertiesApp() throws Exception {
        if ( properties != null )
            throw new Exception("properties jah foi identificado");

        InputStream isWCApp = WC_Properties.class.getResourceAsStream(WCApp$Properties);
        InputStream isApp = WC_Properties.class.getResourceAsStream(App$Properties);

        if ( isWCApp == null ) {
            throw new Exception("PANE: Nao foi poss\u00EDvel localizar o arquivo: " + WCApp$Properties);
        }
        else if ( isApp == null ) {
            throw new Exception("PANE: Nao foi poss\u00EDvel localizar o arquivo: " + App$Properties);
        }

        properties = load(WCApp$Properties);
        properties.putAll(load(App$Properties));
        properties.putAll(load("/build.release"));

        mensagens = load("/Mensagens.properties");
    }

    /** properties com direito a log */
    public static Properties properties() {
        if ( properties != null )
            return properties;

        throw new Error("propriedades foram perdidas");
    }

    public static Properties mensagens() {
        if ( mensagens != null )
            return mensagens;

        throw new Error("mensagens foram perdidas");
    }

    /** carrega properties de um arquivo
     *
     * @author paim 04/05/2011
     * @param path
     * @param clazz
     * @return {@link Properties} Properties */
    /** carrega properties de um arquivo */
    public static Properties load(String resource) {
        Properties p = new Properties();
        try {
            InputStream in = WC_Properties.class.getResourceAsStream(resource);
            if ( in == null )
                WC_Log.loga2(Level.WARNING, "Nao foi poss\u00EDvel localizar o arquivo: " + resource);
            else {
                p.load(in);
                WC_Log.loga2(Level.INFO, "Propriedades carregadas do arquivo: " + resource);
            }
        }
        catch ( Exception e ) {
            WC_Log.loga2(Level.SEVERE, e.getMessage());
        }
        return p;
    }

    public static void save() {}

    /** se nao encontrar a chave, retorna string vazia
     *
     * @author paim 10/05/2011
     * @param key
     * @return String */
    public static String getProperty(String key) {
        return properties().getProperty(key, "");
    }

    /** @see java.util.Properties#getProperty(String key, String defaultValue) */
    public static String getProperty(String key, String defaultValue) {
        return properties().getProperty(key, defaultValue);
    }

    public static String getmsg(String key) {
        return mensagens().getProperty(key, "");
    }

    public static String getmsg(String key, String[] params) {
        String msg = getmsg(key);

        if ( params == null )
            return msg;

        for ( int i = 0; i < params.length; i++ )
            msg = msg.replaceAll("(@" + (i + 1) + ")", params[i]);

        return msg;
    }
}
