 select o from class_tab_1 tab_1
        join fetch tab_1.xxx tab_2
        join fetch tab_2.yyy tab_3
        where tab_1.zzz.id = :coisa
        order by tab_3.codigo, tab_3.id, tab_2.inicio, tab_2.fim


SELECT DISTINCT o FROM o IN CLASS clazz
 INNER JOIN  o.lista ls
 WHERE o.inicio <= :data
       AND (o.fim IS NULL OR o.fim >= :data )
       AND ls.xxx.id IN (:minhaLista)
 ORDER BY o.zzz

select distinct o1.xxx from o1 in class clazz1, o2 in class clazz2
where  o2.xx.yy.zz.id= :id_1
        and oe.xx.id= :id_2
        and o1.aa.bb.codigo= :codigo



SELECT DISTINCT o FROM o IN CLASS clazz
        INNER JOIN o.associativa ass
        WHERE ass.xx.id= :idx
        AND   ass.yy.id= :idy


select o.id from clazz o
inner join o.lista ls
where o.XXX.codigo = :codigo
      AND ls.AAA.id in (:ids)
      and not exists (select o2.id from clazz2 o2
                      where o2.111.id = o.id
                            and o2.AAA.id not in (:ids)
                     )
group by o.id having count(o) = :idsSize


SELECT DISTINCT o FROM clazz o
      inner join o.asslista1 ls1
      inner join o.asslista2 ls2
      WHERE ls1.ooo.id = o.id
            AND ls1.xxx.id in (:idsxxx)
            AND ls2.ooo.id = o.id
            AND ls2.yyy.id in (:idsyyy)

